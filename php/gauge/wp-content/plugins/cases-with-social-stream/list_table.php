<?php
/**
 * Created by PhpStorm.
 * User: anderson
 * Date: 11/9/14
 * Time: 4:21 PM
 */

if (!class_exists('WP_List_Table')) {
    require_once(ABSPATH . 'wp-admin/includes/class-wp-list-table.php');
}

class CaseListTable extends WP_List_Table {

    function __construct(){

        //Set parent defaults
        parent::__construct( array(
            'singular' => 'case',
            'plural'   => 'cases',
            'ajax'     => true
        ));
    }

    function column_default($item, $column_name) {
        switch ($column_name) {
            case 'description':
            case 'media_url':
            case 'link':
            case 'created_at':
                return $item[$column_name];
            break;
            case 'published':
                return $this->statuses($item);
            break;
            case 'actions':
                return $this->column_name($item);
                break;
            default:
                return print_r($item, true);
        }
    }

    function statuses($item) {
        $class = $item['published'] == 1 ? 'enabled' : 'disabled';
        $statuses = sprintf('<a class="case-status '  . $class . '" href="?page=%s&action=%s&id=%s&action_type=%s&status=%s"></a>',$_REQUEST['page'], 'edit' ,$item['id'], 'status', $item['published']);
        $action = array(
            'edit_status' => $statuses
        );

        return sprintf('%1$s', $this->row_actions($action, true));
    }

    function column_name($item) {
        $screen = get_current_screen();
        $edit = sprintf('<a href="?page=%s&action=%s&id=%s">Edit</a>',$_REQUEST['page'], 'edit' ,$item['id']);
        $delete = sprintf('<a class="case-delete" href="?page=%s&action=%s&id=%s">Delete</a>',$_REQUEST['page'],'delete',$item['id']);
        switch($screen->id) {
            case 'cases_page_twitter-stream':
            case 'cases_page_facebook-stream':
            case 'cases_page_instragram-stream':
                $action_type = 'status';
                $actions = array(
                    'delete' => $delete
                );
            break;
            default:
                //$action_type = 'edit';
                $actions = array    (
                    //'edit'   => $edit,
                    'delete' => $delete
                );
        }

        return sprintf(('%3$s'),
            /*$1%s*/ '<a href="?page=' . $_REQUEST['page'] . '&action=edit&id=' . $item['id'] . '"><b>' . $item['title'] . '</b></a>',
            /*$2%s*/ $item['id'],
            /*$3%s*/ $this->row_actions($actions, true)
        );

    }

    function column_cb($item) {
        return sprintf(
            '<input type="checkbox" name="%1$s[]" value="%2$s" />',
            /*$1%s*/ $this->_args['singular'],
            /*$2%s*/ $item['id']
        );
    }

    function get_columns(){
        $columns = array(
            'cb'          => '<input type="checkbox" />',
            'description' => 'Texto',
            'media_url'   => 'Link da midia',
            'link'        => 'Link do post',
            'published'   => 'Publicado',
            'created_at'  => 'Data',
            'actions'     => 'Ações'
        );
        return $columns;
    }

    function get_sortable_columns() {
        $sortable_columns = array(
            'id'          => array('id', false),
            'description' => array('description', false),
            'published'   => array('published'),
            'created_at'  => array('created_at', false)
        );
        return $sortable_columns;
    }

    function get_bulk_actions() {
        $actions = array(
            'delete' => 'Delete'
        );
        return $actions;
    }

    function process_bulk_action() {
        global $wpdb;

        if ('delete' === $this->current_action() ) {
            $case_id = $_GET['case'];
            if ($case_id) {
                for ($i = 0; $i < count($case_id); $i++) {
                    $query = $wpdb->prepare(
                        "
                            DELETE FROM ytew5_cases WHERE id = %d
                        ",
                        $case_id[$i]
                    );
                    $wpdb->query($query);
                }
            }
        }
    }

    function prepare_items() {
        global $wpdb;

        if ($_GET['page'] == 'twitter-stream')
            $type_case = 'tw';

        if ($_GET['page'] == 'facebook-stream')
            $type_case = 'fb';

        if ($_GET['page'] == 'instagram-stream')
            $type_case = 'in';

        $query = "SELECT * FROM ytew5_cases WHERE type_case='$type_case' ORDER BY id ASC";
        $per_page = 20;

        $columns = $this->get_columns();
        $hidden = array();
        $sortable = $this->get_sortable_columns();

        $this->_column_headers = array($columns, $hidden, $sortable);

        $this->get_bulk_actions();
        $this->process_bulk_action();
        $items = $wpdb->get_results($query);

        if ($items) {

            foreach($items as $item) {

                if ($item->media_type == 'photo' || $item->media_type == 'swf' || $item->media_type == 'image') {
                    if ($type_case == 'in') {
                        $img_src = $item->media_thumb;
                    } else {
                        $img_src = $item->media_url;
                    }
                    $item->media_url = '<a class="image-link" target="_blank" href="' . $item->media_url . '"><img src="' . $img_src . '" width="100"></a>';
                }

                if ($type_case == 'tw') {

                    $media_url = trim($item->media_url);

                    if (empty($media_url)) {
                        $item->link = 'https://twitter.com/gauge/status/' . $item->type_id;
                        $item->media_url = false;
                    }
                }


                if ($item->media_type == 'link' || $item->media_type == '' && $item->media_url != false)
                    //$item->media_url = '<a target="_blank" href="' . $item->link . '">' . $item->link . '</a>';

                if ($type_case == 'fb') {
                    $item->media_url = '<a class="image-link" target="_blank" href="' . $item->media_url . '"><img src="' . $item->media_url . '" width="100"></a>';
                    $item->description = $item->text;
                }

                $data[] = array(
                    'id'          => $item->id,
                    'description' => $item->description,
                    'media_url'   => $item->media_url,
                    'link'        => '<a target="_blank" href="' . $item->link . '">' . $item->link . '</a>',
                    'published'   => $item->published,
                    'created_at'  => $item->created_at
                );
            }
        } else {
            $data = array();
        }

        function usort_reorder($a,$b) {
            $orderby = (!empty($_REQUEST['orderby'])) ? $_REQUEST['orderby'] : 'id';
            $order   = (!empty($_REQUEST['order'])) ? $_REQUEST['order'] : 'desc';
            $result  = strcmp($a[$orderby], $b[$orderby]);
            return ($order==='desc') ? $result : - $result;
        }

        usort($data, 'usort_reorder');

        $current_page = $this->get_pagenum();

        $total_items = count($data);

        $data = array_slice($data,(($current_page-1) * $per_page),$per_page);

        $this->items = $data;

        $this->set_pagination_args( array(
            'total_items' => $total_items,
            'per_page'    => $per_page,
            'total_pages' => ceil($total_items / $per_page)
        ));
    }
}

$list_items = new CaseListTable();
$list_items->prepare_items();