<?php

if (!defined('CASES_WITH_SOCIAL_STREAM')) {
    header( 'Status: 403 Forbidden' );
    header( 'HTTP/1.1 403 Forbidden' );

    exit;
}

$path = ABSPATH . 'wp-content/plugins/cases-with-social-stream/';

if ($_SERVER['SERVER_ADDR'] == '192.168.0.120') {
    // Gauge Stream Local app
    $fbApp    = '416441218511021';
    $fbSecret = 'cbb6c3ac58beb65c2a7d8662ac38d6bd';
    $fbUrl    = 'http://192.168.0.120/gauge/gauge/dev/php/gauge/wp-admin/admin.php?page=facebook-stream';
} else {
    $fbApp    = '497658837042328';
    $fbSecret = '801724d013dcdd7def6e6b1a13ddcf90';
    $fbUrl    = 'http://www.gauge.com.br/wp-admin/admin.php?page=facebook-stream';
}

require_once($path . 'db_actions.php');

$access_token = $_POST['access_token'];
$data      = file_get_contents("https://graph.facebook.com/v2.2/357726489632/feed?fields=actions,link,full_picture,attachments,description,message&access_token=".$access_token . "&limit=10");
$posts     = json_decode($data);
$db_action = new CasesDbExtraActions();
$type_case = 'fb';
$type_of   = $db_action->selectTypeId($type_case);

$user = 'Gauge';

foreach($posts->data as $post) {

    if ($post->type != 'status' || $post->type == 'status' && $post->message != '') {
        if ($post->link == '') {
            $post->link = $post->actions[0]->link;
        }

        if ($post->message == '') {
            $post->message = $post->description;
        }

        $media_url = $post->attachments->data[0]->media->image->src;

        if ($media_url == '') {
            $media_url = $post->link;
        }

        $data = array(
            'type_id'     => $post->id,
            'type_case'   => $type_case,
            'object_id'   => $post->object_id,
            'user'        => $user,
            'title'       => 'Facebook',
            'text'        => $post->message,
            'description' => $post->description,
            'caption'     => $post->caption,
            'media_url'   => $media_url,
            'media_type'  => $post->type,
            'media_thumb' => '',
            'link'        => $post->link,
            'created_at'  => $post->created_time
        );

        if (array_search(array('type_id' => $post->id), $type_of) === false) {
            $db_action->insertCaseData($data);
        } else {

        }
    }
}
exit;