<?php
/**
 * Created by PhpStorm.
 * User: anderson
 * Date: 10/15/14
 * Time: 5:08 PM
 */

class CasesDbExtraActions {

    public $_wpdb;

    function __construct() {
        global $wpdb;
        $this->_wpdb = $wpdb;
    }

    public function selectTypeId($type_case) {

        return $this->_wpdb->get_results("SELECT type_id FROM ytew5_cases WHERE type_case='$type_case'", ARRAY_A);
    }

    public function insertCaseData($data) {

        if ($data['type_case'] == 'fb') {
            $created_at = $data['created_at'];
        } else {
            $created_at = date("Y-m-d h:i:s");
        }
        $query = $this->_wpdb->prepare(
            "
                INSERT INTO ytew5_cases
                (type_id, object_id, type_case, user, title, text, description, caption, media_url, media_type, media_thumb, link, created_at)
                VALUES (%s, %s, %s, %s, %s, %s, %s, %s, %s, %s, %s, %s, %s)
            ",
            $data['type_id'],
            $data['object_id'],
            $data['type_case'],
            $data['user'],
            $data['title'],
            $data['text'],
            $data['description'],
            $data['caption'],
            $data['media_url'],
            $data['media_type'],
            $data['media_thumb'],
            $data['link'],
            $created_at);
        if($this->_wpdb->query($query)) {

        } else {
            print_r($this->_wpdb->last_error);
        }
    }

    public function updateCase() {

    }

    public function updateCaseStatus($fields) {
        $status = $fields['status'];

        if ($status == 1) {
            $status = 0;
        } else {
            $status = 1;
        }

        $case_id = $fields['case_id'];

        $query = $this->_wpdb->prepare(
            "
                UPDATE ytew5_cases SET published = %d WHERE id = %d
            ",
            $status,
            $case_id
        );

        $this->_wpdb->query($query);
    }

    public function caseDelete($case_id) {
        $query = $this->_wpdb->prepare(
            "
                DELETE FROM ytew5_cases WHERE id = %d
            ",
            $case_id
        );
        var_dump($this->_wpdb->query($query));
        echo $case_id;
    }
}

$db_action = new CasesDbExtraActions();