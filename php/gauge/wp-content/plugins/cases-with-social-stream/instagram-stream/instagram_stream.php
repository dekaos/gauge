<?php
/**
 * Created by WebStorm.
 * User: anderson
 * Date: 12/17/14
 * Time: 10:17 AM
 */
if (!defined('ABSPATH')) {
    header( 'Status: 403 Forbidden' );
    header( 'HTTP/1.1 403 Forbidden' );

    exit;
}

$path = ABSPATH . 'wp-content/plugins/cases-with-social-stream/';

require_once($path . 'db_actions.php');
require_once('instagram-php-sdk/src/instagram.php');

use MetzWeb\Instagram\Instagram;

$instagram = new Instagram(array(
    'apiKey' => '46911dd363b24b5b8332f47a4e3e1653'
));

$media = $instagram->getUserMedia(249673769, 10);

$type_case = 'in';
$db_action = new CasesDbExtraActions();
$type_of   = $db_action->selectTypeId($type_case);
$user      = 'Gauge';

foreach ($media->data as $post) {
    $data = array(
        'type_id'     => $post->id,
        'type_case'   => $type_case,
        'oject_id'    => '',
        'user'        => $user,
        'title'       => 'Instagram',
        'text'        => '',
        'description' => $post->caption->text,
        'caption'     => '',
        'media_url'   => $post->images->standard_resolution->url,
        'media_type'  => $post->type,
        'media_thumb' => $post->images->thumbnail->url,
        'link'        => $post->link
    );

    if (array_search(array('type_id' => $post->id), $type_of) === false) {
        $db_action->insertCaseData($data);
    } else {

    }
}