<?php
/**
 * Created by PhpStorm.
 * User: anderson
 * Date: 10/20/14
 * Time: 5:38 AM
 */

$path = ABSPATH . 'wp-content/plugins/cases-with-social-stream/';

require_once('TwitterAPIExchange.php');
require_once($path . 'db_actions.php');

$settings = array(
    'oauth_access_token'        => "1095621434-zjs1FSoUdwtgW4NhmXBTnWw6vlQSUrKRaVSZhMn",
    'oauth_access_token_secret' => "EYnU01JhnJ8B9y7erzSSVTnvzClQRlK0RnrAageAKWyvm",
    'consumer_key'              => "2zkemCH99gFlJSYFHpmVrN8Xf",
    'consumer_secret'           => "d6qlyXLtH8OWSvOsrnoEvY37A3pXheXJUL44VM9DYq1MUyJQy8"
);

$url = 'https://api.twitter.com/1.1/statuses/user_timeline.json';

$requestMethod = 'GET';
$getfield = '?user_id=11012942&count=10';
$twitter  = new TwitterAPIExchange($settings);
$data     = $twitter
    ->setGetfield($getfield)
    ->buildOauth($url, $requestMethod)
    ->performRequest();

$tweets    = json_decode($data, true);
$type_case = 'tw';
$db_action = new CasesDbExtraActions();
$type_of   = $db_action->selectTypeId($type_case);

if ($tweets) {
    foreach($tweets as $tweet) {
        $data = array(
            'type_id'     => $tweet['id'],
            'object_id'   => '',
            'type_case'   => $type_case,
            'user'        => $tweet['user']['screen_name'],
            'title'       => 'Twitter',
            'text'        => '',
            'description' => $tweet['text'],
            'caption'     => '',
            'media_url'   => $tweet['entities']['media'][0]['media_url'],
            'media_type'  => $tweet['entities']['media'][0]['type'],
            'media_thumb' => '',
            'link'        => $tweet['entities']['media'][0]['url'],
        );

        // insert post only if it not exist in db
        if (array_search(array('type_id' => $tweet['id']), $type_of) === false) {
            $db_action->insertCaseData($data);
        } else {

        }
    }
}