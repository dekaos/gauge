<?php
/**
 * Created by PhpStorm.
 * User: anderson
 * Date: 10/20/14
 * Time: 4:34 AM
 * Plugin name: Cases with social stream
 * Description: Cases Gauge
 */

if (!defined('ABSPATH')) {
    header( 'Status: 403 Forbidden' );
    header( 'HTTP/1.1 403 Forbidden' );

    exit;
}

session_start();

function cases_with_social_stream () {
    if ( !current_user_can( 'manage_options' ) )  {
        wp_die( __( 'You do not have sufficient permissions to access this page.' ) );
    }

    require_once('all-cases.php');
}

$path = ABSPATH . 'wp-content/plugins/cases-with-social-stream/';

define('CASES_WITH_SOCIAL_STREAM', '1.0');

add_action( 'admin_menu', 'register_cases_with_social_stream' );
add_action( 'admin_enqueue_scripts', 'cases_js_enqueue' );
add_action( 'admin_enqueue_scripts', 'cases_style' );

add_action ('wp_ajax_get_tw_posts', 'get_tw_posts');
add_action ('wp_ajax_get_fb_posts', 'get_fb_posts');
add_action ('wp_ajax_get_in_posts', 'get_in_posts');

function admin_inline_js(){
    echo '<script>';
    echo "var root_admin_addr = " . "'" . $_SERVER["SERVER_ADDR"] . "';";
    if ($_SERVER['SERVER_ADDR'] == '192.168.0.120') {
        echo "var admin_redirect_url = 'http://192.168.0.120/gauge/gauge/dev/php/gauge/wp-admin/admin.php';";
    } else {
        echo "var admin_redirect_url = 'http://www.gauge.com.br//wp-admin/admin.php';";
    }
    echo '</script>';
}
add_action( 'admin_print_scripts', 'admin_inline_js' );

function cases_style() {
    wp_register_style( 'cases_style', plugin_dir_url( __FILE__ ) . 'assets/css/cases.css', false, false);
    wp_enqueue_style( 'cases_style' );
}

function cases_js_enqueue() {
    wp_enqueue_script( 'cases_js', plugin_dir_url( __FILE__ ) . 'assets/js/cases.js' );
}

function register_cases_with_social_stream () {
    add_menu_page( 'Trabalhos', 'Trabalhos', 10, 'cases', 'cases_with_social_stream', 'dashicons-smiley', 72);
    add_submenu_page('cases', 'Twitter', 'Twitter', 0, 'twitter-stream', 'twitter_stream');
    add_submenu_page('cases', 'Facebook', 'Facebook', 0, 'facebook-stream', 'facebook_stream');
    add_submenu_page('cases', 'Instagram', 'Instagram', 0, 'instagram-stream', 'instagram_stream');
}

function get_tw_posts () {
    require_once('twitter-stream/twitter_stream.php');
    exit;
}

function get_fb_posts () {
    require_once('facebook-stream/facebook_stream.php');
    exit;
}

function get_in_posts () {
    require_once('instagram-stream/instagram_stream.php');
    exit;
}

function twitter_stream () {
    require_once('display_posts.php');
}

function facebook_stream () {
    require_once('display_posts.php');
}

function instagram_stream () {
    require_once('display_posts.php');
}

function cases_conf () {

}

if (isset($_POST['action']) && isset($_POST['action_type'])) {

    require_once($path . 'db_actions.php');

    $action_type = $_POST['action_type'];
    $status      = $_POST['status'];
    $case_id     = $_POST['case_id'];

    if ($action_type == 'status') {
        $fields = array(
            'status'  => $status,
            'case_id' => $case_id
        );

        $db_action->updateCaseStatus($fields);
    }

    if ($action_type == 'delete') {
        $db_action->caseDelete($case_id);
    }
}