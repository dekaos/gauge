<?php
/**
 * Created by PhpStorm.
 * User: anderson
 * Date: 10/20/14
 * Time: 4:34 AM
 */

if (!defined('ABSPATH')) {
    header('Status: 403 Forbidden');
    header('HTTP/1.1 403 Forbidden');

    exit;
}

require_once($path . 'list_table.php');

$title = '';

if ($_GET['page'] == 'twitter-stream')
  $title = 'Twitter';

if ($_GET['page'] == 'facebook-stream')
    $title = 'Facebook';

if ($_GET['page'] == 'instagram-stream')
    $title = 'Instagram';

?>

<div class="wrap">
    <h2><?php echo $title; ?></h2>
    <form id="cases-filter" method="get">
        <?php $list_items->display() ?>
        <input type="hidden" name="page" value="<?php echo $_REQUEST['page'] ?>" />
    </form>
</div>