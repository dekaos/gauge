<?php
/**
 * Created by WebStorm.
 * User: anderson
 * Date: 12/5/14
 * Time: 5:44 PM
 */

// prevent directory listing

header( 'Status: 403 Forbidden' );
header( 'HTTP/1.1 403 Forbidden' );
exit;
