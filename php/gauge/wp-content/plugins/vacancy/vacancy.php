<?php
/**
 * Created by WebStorm.
 * User: anderson
 * Date: 12/5/14
 * Time: 5:45 PM
 * Plugin name: Banco de Talentos
 * Description: Banco de talentos Gauge
 */

if (!defined('ABSPATH')) {
    header( 'Status: 403 Forbidden' );
    header( 'HTTP/1.1 403 Forbidden' );
    exit;
}

define('VACANCY', '1.0');
define('VACANCY_PATH', ABSPATH . 'wp-content/plugins/vacancy/');

require_once(VACANCY_PATH . 'class/vacancy.php');
require_once(VACANCY_PATH . 'class/talent.php');

add_action('admin_menu', 'register_vacancy');
add_action('admin_enqueue_scripts', 'admin_vacancy_styles');
add_action('admin_enqueue_scripts', 'admin_vacancy_js');
add_action('wp_ajax_add_vacancy', 'add_vacancy');
add_action('wp_ajax_edit_vacancy', 'edit_vacancy');
add_action('wp_ajax_add_department', 'add_department');

add_action('wp_ajax_no_vacancy', 'no_vacancy');
add_action('wp_ajax_nopriv_no_vacancy', 'no_vacancy');

add_action('wp_ajax_have_vacancy', 'have_vacancy');
add_action('wp_ajax_nopriv_have_vacancy', 'have_vacancy');

add_action('wp_ajax_edit_talent', 'edit_talent');

function admin_vacancy_styles() {;
    wp_enqueue_style( 'admin_css_vacancy', '/wp-content/plugins/vacancy/assets/css/style.css', false, '1.0.0' );
}

function admin_vacancy_js() {
    wp_enqueue_script('jquery-form');
    wp_enqueue_script('admin_js_vacancy', '/wp-content/plugins/vacancy/assets/js/vacancy.js', array(), '1.0.0', true );
}

function register_vacancy () {

    add_menu_page( 'talent_pool', 'Trabalhe conosco', 'manage_options', 'vacancy_list', 'gauge_vacancy', 'dashicons-megaphone', 74);
    add_submenu_page('vacancy_list', 'Vagas', 'Vagas', 0, 'vacancy_list', 'vacancy_admin');
    add_submenu_page('vacancy_list', 'Cadastros', 'Cadastros', 0, 'talent_list', 'talent_admin');
    add_submenu_page('vacancy_list', 'Configurações', 'Configuraçẽos', 0, 'research-config', 'research_config');
}

function research_config () {
    if ($_POST['vacancy_email_settings']) {

        $vacancy_email_settings = sanitize_text_field($_POST['vacancy_email_settings']);

        update_option('vacancy_email_settings', $vacancy_email_settings);
    }
?>

    <style>
        #vacancy-email-settings label {
            padding-right: 10px;
            font-weight: bold;
            display: block;
            font-size: 16px;
        }

        #vacancy-email-settings input[type="email"] {
            height: 40px;
            line-height: 40px;
            width: 450px;
            display: block;
            margin-bottom: 10px
        }
    </style>

    <h2>Email de recebimento</h2>
    <form method="post" id="vacancy-email-settings">
        <label for="vacancy-email">Email</label>
        <input type="email" name="vacancy_email_settings" value="<?php echo get_option('vacancy_email_settings'); ?>" required>
        <input value="Atualizar" type="submit" class="button button-primary button-large">
    </form>
<?php
}

function gauge_vacancy() {

    if (!current_user_can( 'manage_options' )) {
        wp_die( __( 'You do not have sufficient permissions to access this page.' ) );
    }
}

function vacancy_admin () {

    if (isset($_GET['action']) && $_GET['action'] == 'new_vacancy' || $_GET['action'] == 'edit_vacancy') {

        $vacancy = new VacancyActions();

        $departments = $vacancy->getDepartments();
        $citys = $vacancy->getCitys();

        if($_GET['action'] == 'edit_vacancy') {

            $id = sanitize_text_field($_GET['id']);

            $data = $vacancy->getVacancy($id);
        }

        require_once(VACANCY_PATH . 'templates/vacancies/edit-vacancy.php');
    } else {
        require_once(VACANCY_PATH . 'templates/vacancies/list.php');
    }
}

function talent_admin () {
    if (isset($_GET['action']) && $_GET['action'] == 'new_talent' || $_GET['action'] == 'edit_talent') {

        $talent = new TalentActions();
        $vacancy = new VacancyActions();

        $departments = $vacancy->getDepartments();

        $states = $talent->getStates();

        $citys = $vacancy->getCitys();

        if($_GET['action'] == 'edit_talent') {

            $id = sanitize_text_field($_GET['id']);
            $vacancies = $talent->getVacancies();
            $data = $talent->getTalent($id);
            $city = $talent->getCity($data['city']);
        }

        require_once(VACANCY_PATH . 'templates/talents/edit-talent.php');
    } else {
        require_once(VACANCY_PATH . 'templates/talents/list.php');
    }
}
// admin
function add_vacancy() {
    return vacancy_data('new');
}

// admin
function edit_vacancy () {
    return vacancy_data('edit');
}

// admin
function add_department () {

    $title      = sanitize_text_field($_POST['department-title']);
    $created_at = date("Y-m-d h:i:s");

    $data = array(
        'title'      => $title,
        'created_at' => $created_at
    );

    $vacancy = new VacancyActions();

    $vacancy->newDepartment($data);

    exit;
}

function vacancy_data ($type) {
    $id          = sanitize_text_field($_POST['vacancy-id']);
    $action_type = sanitize_text_field($_POST['vacancy-action-type']);
    $statuses    = sanitize_text_field($_POST['vacancy-status']);
    $name        = sanitize_text_field($_POST['vacancy-name']);
    $location    = sanitize_text_field($_POST['vacancy-location']);
    $department  = sanitize_text_field($_POST['vacancy-department']);
    $description = $_POST['vacancy-description'];
    $amount      = sanitize_text_field($_POST['vacancy-amount']);
    $published   = sanitize_text_field($_POST['vacancy-published']);
    $created_at  = date("Y-m-d h:i:s");

    $data = array(
        'id'          => $id,
        'vacancy'     => $name,
        'action_type' => $action_type,
        'location'    => $location,
        'department'  => $department,
        'description' => $description,
        'amount'      => $amount,
        'published'   => $published,
        'statuses'    => $statuses,
        'created_at'  => $created_at
    );

    $vacancy = new VacancyActions();

    if ($type == 'new') {
        $vacancy->newVacancy($data);
    } else {
        $vacancy->updateVacancy($data);
    }

    exit;
}

function have_vacancy () {
    return process_form();
}

function no_vacancy () {
    return process_form();
}

// admin
function edit_talent () {
    return process_form();
}

function process_form () {
    $action       = sanitize_text_field($_POST['action']);
    $name         = sanitize_text_field($_POST['nv-name']);
    $email        = sanitize_text_field($_POST['nv-email']);
    $state        = sanitize_text_field($_POST['nv-state']);
    $city         = sanitize_text_field($_POST['nv-city']);
    $phone        = sanitize_text_field($_POST['nv-phone']);
    $interest     = sanitize_text_field($_POST['nv-interest']);
    $interest_1   = sanitize_text_field($_POST['nv-interest-1']);
    $linkedin     = sanitize_text_field($_POST['nv-linkedin']);
    $vacancy_type = sanitize_text_field($_POST['vacancy-type']);
    $curriculum   = sanitize_text_field($_POST['curriculum']);

    $error_message = __('This field is mandatory', 'roots');
    $error = '';

    $file = $_FILES['nv-upload'];

    if ($action == 'edit_talent' && !empty($file) || $action == 'have_vacancy' || $action == 'no_vacancy') {
        if ($file) {

            $file_info = finfo_file(finfo_open(FILEINFO_MIME_TYPE), $file['tmp_name']);

            if ($file_info != 'application/pdf')
                $error = json_encode(array('message' => __('Invalid file', 'roots'), 'message_type' => 'error', 'field_id' => 'nv-file')) . ',';

            if ($file['size'] > 5242880)
                $error .= json_encode(array('message' => __('Invalid file size', 'roots'), 'message_type' => 'error', 'field_id' => 'nv-file')) . ',';
        } else {
            $error .= json_encode(array('message' => $error_message, __('Please, select your resume (PDF)', 'roots') => 'error', 'field_id' => 'nv-file')) . ',';
        }
    }

    if(!$name)
        $error .= json_encode(array('message' => $error_message, 'message_type' => 'error', 'field_id' => 'nv-name')) . ',';

    if (!$email)
        $error .= json_encode(array('message' => $error_message, 'message_type' => 'error', 'field_id' => 'nv-email')) . ',';

    if ($email && !is_email($email))
        $error .= json_encode(array('message' => __('Invalid e-mail', 'roots'), 'message_type' => 'error', 'field_id' => 'nv-email')) . ',';

    if (!$state)
        $error .= json_encode(array('message' => $error_message, 'message_type' => 'error', 'field_id' => 'nv-state')) . ',';

    if (!$city)
        $error .= json_encode(array('message' => $error_message, 'message_type' => 'error', 'field_id' => 'nv-city')) . ',';

    if (!$phone)
        $error .= json_encode(array('message' => $error_message, 'message_type' => 'error', 'field_id' => 'nv-phone')) . ',';

    if (!$interest)
        $error .= json_encode(array('message' => $error_message, 'message_type' => 'error', 'field_id' => 'nv-interest')) . ',';

    if (!$linkedin)
        $error .= json_encode(array('message' => $error_message, 'message_type' => 'error', 'field_id' => 'nv-likedln')) . ',';

    if (!$error) {
        $vacancy   = new VacancyActions();
        $tmp_name  = $file['tmp_name'];
        $file_name = explode('.', $file['name']);
        $random    = rand(1, 999999999);
        $pdf_file  = round(microtime(true) * 1000) . '_' . $random . '.' . end($file_name);
        $directory = VACANCY_PATH .  'uploads/';
        $created_at = date("Y-m-d h:i:s");

        $pdf_url  = plugins_url() . '/vacancy/uploads/' . $pdf_file;

        $data = array(
            'action'     => $action,
            'name'       => $name,
            'email'      => $email,
            'state'      => $state,
            'city'       => $city,
            'phone'      => $phone,
            'interest'   => $interest,
            'linkedin'   => $linkedin,
            'pdf_url'    => $pdf_url,
            'pdf_file'   => $pdf_file,
            'created_at' => $created_at
        );

        $state_id           = $data['state'];
        $city_id            = $data['city'];
        $interest_id        = $data['interest'];
        $data['state_name'] = $vacancy->getState($state_id);
        $data['city_name' ] = $vacancy->getCity($city_id);

        if ($tmp_name) {
            move_uploaded_file($tmp_name, $directory . $pdf_file);
        }

        $db_error_message = '[' . json_encode(array('message' => __('Data error insert. Please try again.' , 'roots'), 'message_type' => 'error')) . ']';

        if ($action == 'no_vacancy' || $action == 'have_vacancy') {

            if ($vacancy->addNewTalent($data)) {

                if ($data['action'] == 'no_vacancy') {
                    $data['interest']  = $vacancy->getInterest($interest_id);
                    $data['form_name'] = 'no-vacancy-form';
                } else {
                    $data['interest']  = $vacancy->getVacancyTitle($interest_id);
                    $data['form_name'] = 'vacancy-form';
                }

                return send_to_mail($data);
            } else {
                echo $db_error_message;
            }

        } else {
            if ($action == 'edit_talent') {

                $talent = new TalentActions();

                $data['id'] = sanitize_text_field($_POST['talent-id']);

                $data['vacancy_type'] = $vacancy_type;

                if (!empty($interest_1)) {
                    $data['interest'] = $interest_1;
                }

                if (empty($file)) {
                    $data['pdf_url'] = $curriculum;
                }

                $data['interest_1'] = $interest_1;
                $talent->updateTalent($data);
            }
        }

    } else {
        echo '[' . rtrim($error, ',') . ']';
    }
    exit;
}

function send_to_mail ($data) {

    $to = 'rh@gauge.com.br';
    //$to = get_option('vacancy_email_settings');
    //$to = 'caos19@gmail.com';
    $from = 'contato@gauge.com.br';

    $subject = 'Site Gauge - Cadastro em Trabalhe Conosco';
    $headers = "Replay-To: " . $data['email'] . "\r\n";
    $headers .= "From: "     . $from . "\r\n";
    $headers .= "MIME-Version: 1.0\r\n";
    $headers .= "Content-Type: text/html; charset: UTF-8\r\n";

    $message = '<html><body><meta charset="utf-8">';
    $message .= '<h2>' . $subject          . '</h2>';
    $message .= '<b>Nome: </b>'            . $data['name'      ] . '<br>';
    $message .= '<b>Email: </b>'           . $data['email'     ] . '<br>';
    $message .= '<b>Estado: </b>'          . $data['state_name'] . '<br>';
    $message .= '<b>Cidade: </b>'          . $data['city_name' ] . '<br>';
    $message .= '<b>Telefone: </b>'        . $data['phone'     ] . '<br>';
    $message .= '<b>Interesse: </b>'       . $data['interest'  ] . '<br>';
    $message .= '<b>Perfil LinkedIn: </b>' . '<a href="https://www.linkedin.com/pub/' . $data['linkedin'] . '">' . $data['linkedin'] . '</a><br>';
    $message .= '<b>Curriculum: </b>'      . '<a href="' . $data['pdf_url']  . '">' . $data['pdf_file']  . '</a><br>';
    $message .= '</body></html>';

    $send = wp_mail($to, $subject, $message, $headers);

    $form_name = $data['form_name'];

    if ($send) {
        return success_message ($form_name);
    } else {
        echo '[' . json_encode(array('message' => __('Sending message error. Please try again', 'roots'), 'type_of' => 'process_error')) . ']';
    }
    exit;
}

function success_message ($form_name) {
    echo '[' . json_encode(array(
            'message'   => __('Your registration has been successfully completed', 'roots'),
            'tip'       => __('Your resume was added to our talent pool', 'roots'),
            'close'     => __('Close', 'roots'),
            'form_name' => $form_name,
            'type_of'   => 'success')) .
        ']';
    exit;
}