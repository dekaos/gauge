<?php
/**
 * Created by WebStorm.
 * User: anderson
 * Date: 12/6/14
 * Time: 8:58 PM
 */

if (!defined('VACANCY')) {
    header( 'Status: 403 Forbidden' );
    header( 'HTTP/1.1 403 Forbidden' );

    exit;
}

class VacancyActions {
    public $_db;

    function __construct () {
        global $wpdb;
        $this->_db = $wpdb;
    }

    public function newVacancy ($data) {

        $query = $this->_db->prepare(
            "
                INSERT INTO ytew5_vacancy (
                    vacancy, location, department, description, amount, published, created_at
                )
                VALUES (
                    %s, %s, %s, %s, %d, %d, %s
                )
            ",
            $data['vacancy'],
            $data['location'],
            $data['department'],
            $data['description'],
            $data['amount'],
            $data['published'],
            $data['created_at']
        );

        $result = $this->_db->query($query);
        $last_id = $this->_db->insert_id;

        if ($result) {
            echo '[' . json_encode(array('message' => 'Vaga cadastrada com sucesso.', 'last_id' => $last_id, 'message_type' => 'success')) . ']';
        } else {
            echo '[' . json_encode(array('message' => 'Não foi possível cadastrar a vaga. (Erro: ' . $this->_db->last_error . ')', 'message_type' => 'error')) . ']';
        }
    }

    public function getVacancy ($id) {
        $query = $this->_db->get_row("SELECT * FROM ytew5_vacancy WHERE id='$id'", ARRAY_A);
        return $query;
    }

    public function updateVacancy ($data) {

        $id = $data['id'];

        if ($data['action_type'] == 'status') {

            $statuses = $data['statuses'];

            if ($statuses == 1) {
                $statuses = 0;
            } else {
                $statuses = 1;
            }

            $query = $this->_db->prepare(
                "
                    UPDATE ytew5_vacancy SET published = %d WHERE id = $id
                ",
                $statuses
            );

        } else {
            $query = $this->_db->prepare(
                "
                    UPDATE ytew5_vacancy SET vacancy = %s, location = %s, department = %s, description = %s, amount = %d, published = %d WHERE id = $id
                ",
                $data['vacancy'],
                $data['location'],
                $data['department'],
                $data['description'],
                $data['amount'],
                $data['published']
            );
        }

        $result = $this->_db->query($query);

        if ($result || $result === 0) {
            echo '[' . json_encode(array('message' => 'Vaga atualizada com sucesso. ', 'message_type' => 'success')) . ']';
        } else {
            echo '[' . json_encode(array('message' => 'Não foi possível modificar a vaga. (Erro: ' . $this->_db->last_error . ')', 'message_type' => 'error')) . ']';
        }
    }

    public function newDepartment ($data) {
        $query = $this->_db->prepare(
            "
                INSERT INTO ytew5_vacancy_departments (
                    title, created_at
                ) VALUES (
                    %s, %s
                )
            ",
            $data['title'],
            $data['created_at']
        );

        $result = $this->_db->query($query);

        if ($result) {
            echo '[' . json_encode(array('message' => 'Departamento cadastrado com sucesso.', 'message_type' => 'success')) . ']';
        } else {
            echo '[' . json_encode(array('message' => 'Não foi possível cadastrar o departamento. (Erro: ' . $this->_db->last_error . ')', 'message_type' => 'error')) . ']';
        }
    }

    public function getDepartments () {
        $departments = $this->_db->get_results(
            "SELECT id, title FROM ytew5_vacancy_departments ORDER BY title ASC", ARRAY_A);

        return $departments;
    }

    // get city offices
    public function getCitys () {

        $citys = $this->_db->get_row("SELECT offices FROM ytew5_offices", ARRAY_A);

        return $citys;
    }

    // front-end action
    public function addNewTalent($data) {

        if ($data['action'] == 'no_vacancy') {
            $vacancy_type = 1;
        } else {
            $vacancy_type = 0;
        }

        $query = $this->_db->prepare(
            "
                INSERT INTO ytew5_vacancy_talents (
                    vacancy_type, name, email, state, city, phone, interest, linkedin, pdf_url, created_at
                ) VALUES (
                    %d, %s, %s, %s, %s, %s, %s, %s, %s, %s
                )
            ",
            $vacancy_type,
            $data['name'],
            $data['email'],
            $data['state'],
            $data['city'],
            $data['phone'],
            $data['interest'],
            $data['linkedin'],
            $data['pdf_url'],
            $data['created_at']
        );

        $result = $this->_db->query($query);

        if ($result) {
            return true;
        } else {
            print_r($this->_db->last_error);
           exit;
        }
    }

    public function getState($state_id) {
        $state = $this->_db->get_row("SELECT state FROM ytew5_states WHERE id=$state_id", ARRAY_A);
        return $state['state'];
    }

    public function getCity($city_id) {
        $city = $this->_db->get_row("SELECT city FROM ytew5_citys WHERE id=$city_id", ARRAY_A);
        return $city['city'];
    }

    public function getInterest($interest_id) {
        $interest = $this->_db->get_row("SELECT title FROM ytew5_vacancy_departments WHERE id=$interest_id", ARRAY_A);
        return $interest['title'];
    }

    public function getVacancyTitle($interest_id) {
        $interest = $this->_db->get_row("SELECT vacancy FROM ytew5_vacancy WHERE id=$interest_id", ARRAY_A);
        return $interest['vacancy'];
    }
}