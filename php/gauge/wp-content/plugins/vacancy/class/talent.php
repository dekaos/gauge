<?php
/**
 * Created by WebStorm.
 * User: anderson
 * Date: 12/12/14
 * Time: 4:39 PM
 */

class TalentActions {

    public $_db;

    function __construct () {
        global $wpdb;
        $this->_db = $wpdb;
    }

    public function getTalent($id) {
        $query = $this->_db->get_row("SELECT * FROM ytew5_vacancy_talents WHERE id='$id'", ARRAY_A);
        return $query;
    }

    public function getStates () {
        $query = $this->_db->get_results("SELECT * FROM ytew5_states");
        return $query;
    }

    public function getVacancies () {
        $query = $this->_db->get_results("SELECT id, vacancy FROM ytew5_vacancy");
        return $query;
    }

    public function getCity ($id) {
        $query = $this->_db->get_row("SELECT city FROM ytew5_citys WHERE id=$id", ARRAY_A);
        return $query;
    }

    public function updateTalent ($data) {

        $id = $data['id'];

        $query = $this->_db->prepare(
            "
                UPDATE ytew5_vacancy_talents SET vacancy_type = %s, name = %s, email = %s, state = %s, city = %s, phone = %s, interest = %s, linkedin = %s, pdf_url = %s WHERE id=$id
            ",
            $data['vacancy_type'],
            $data['name'],
            $data['email'],
            $data['state'],
            $data['city'],
            $data['phone'],
            $data['interest'],
            $data['linkedin'],
            $data['pdf_url']
        );

        $result = $this->_db->query($query);

        if ($result || $result === 0) {
            echo '[' . json_encode(array('message' => 'Cadastro atualizado com sucesso.', 'message_type' => 'success')) . ']';
        } else {
            echo '[' . json_encode(array('message' => 'Não foi possível modificar o cadastro. (Erro: ' . $this->_db->last_error . ')', 'message_type' => 'error')) . ']';
        }
    }
}