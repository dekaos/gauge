<?php
/**
 * Created by PhpStorm.
 * User: anderson
 * Date: 11/9/14
 * Time: 4:21 PM
 */

if (!class_exists('WP_List_Table')) {
    require_once(ABSPATH . 'wp-admin/includes/class-wp-list-table.php');
}

class TalentListTable extends WP_List_Table {

    /** ************************************************************************
     * REQUIRED. Set up a constructor that references the parent constructor. We
     * use the parent reference to set some default configs.
     ***************************************************************************/
    function __construct(){
        global $status, $page;

        //Set parent defaults
        parent::__construct( array(
            'singular'  => 'talent',     //singular name of the listed records
            'plural'    => 'talents',    //plural name of the listed records
            'ajax'      => true        //does this table support ajax?
        ));
    }

    function column_default($item, $column_name) {
        switch ($column_name) {
            case 'name' :
            case 'email':
            case 'phone':
            case 'city':
            case 'state':
            case 'interest':
            case 'linkedin':
            case 'pdf_url':
                return $item[$column_name];
                break;
            default:
                return print_r($item,true); //Show the whole array for troubleshooting purposes
        }
    }

    function column_name($item) {

        //Build row actions
        $actions = array    (
            'edit'   => sprintf('<a class="edit-row" href="?page=%s&action=%s&id=%s">Edit</a>',$_REQUEST['page'], 'edit_talent', $item['id']),
            'delete' => sprintf('<a class="delete-talent" href="?page=%s&action=%s&id=%s">Delete</a>',$_REQUEST['page'], 'delete', $item['id']),
        );

        //Return the title contents
        return sprintf('%1$s %3$s',
            /*$1%s*/ '<a href="?page=' . $_REQUEST['page'] . '&action=edit_talent&id=' . $item['id'] . '"><b>' . $item['name'] . '</b></a>',
            /*$2%s*/ $item['id'],
            /*$3%s*/ $this->row_actions($actions)
        );
    }


    function column_cb($item) {
        return sprintf(
            '<input type="checkbox" name="%1$s[]" value="%2$s" />',
            /*$1%s*/ $this->_args['singular'],  //Let's simply repurpose the table's singular label
            /*$2%s*/ $item['id']               //The value of the checkbox should be the record's id
        );
    }

    function get_columns(){
        $columns = array(
            'cb'       => '<input type="checkbox" />', //Render a checkbox instead of text
            'name'     => 'Nome',
            'email'    => 'E-mail',
            'phone'    => 'Telefone',
            'city'     => 'Cidade',
            'state'    => 'Estado',
            'interest' => 'Interessado em',
            'linkedin' => 'Perfil LinkedIn',
            'pdf_url'  => 'Curriculum'
        );
        return $columns;
    }

    function get_sortable_columns() {
        $sortable_columns = array(
            'name'     => array('name' , false),     //true means it's already sorted
            'email'    => array('email', false),
            'phone'    => array('phone', false),
            'city'     => array('city', false),
            'state'    => array('state', false),
            'interest' => array('interest', false),
            'linkedin' => array('linkedin', false),
            'pdf_url'  => array('pdf_url', false)
        );
        return $sortable_columns;
    }

    function get_bulk_actions() {
        $actions = array(
            'delete' => 'Delete'
        );
        return $actions;
    }

    function process_bulk_action() {
        wp_reset_query();
        global $wpdb;

        if ('delete' === $this->current_action() ) {

            $talent_id = $_GET['talent'];

            if ($talent_id) {
                for ($i = 0; $i < count($talent_id); $i++) {
                    $query = $wpdb->prepare(
                        "
                            DELETE FROM ytew5_vacancy_talents WHERE id = %d
                        ",
                        $talent_id[$i]
                    );
                    $wpdb->query($query);
                }
            }

        }
    }

    function delete_item () {
        global $wpdb;

        if ('delete' === $this->current_action() && isset($_GET['id'])) {
            $id = $_GET['id'];
            $query = $wpdb->prepare(
                "
                    DELETE FROM ytew5_vacancy_talents WHERE id = %d
                ",
                $id
            );
            $wpdb->query($query);
        }
    }

    function edit_item() {
        if ('edit' === $this->current_action()) {
        }
    }

    function prepare_items() {
        global $wpdb;

        if (isset($_GET['s']) && !empty($_GET['s'])) {

            $s = sanitize_text_field($_GET['s']);

            $query = "SELECT a.*, b.state, c.city FROM ytew5_vacancy_talents a LEFT JOIN ytew5_states b ON a.state = b.id LEFT JOIN ytew5_citys c ON a.city = c.id WHERE a.name LIKE '%" .$s. "%' OR a.email LIKE '%" . $s . "%' OR a.phone LIKE '%" . $s . "%' OR c.city LIKE '%" . $s . "%' OR b.state LIKE '%" . $s . "%' OR a.linkedin LIKE '%" . $s . "%' OR a.pdf_url LIKE '%" . $s . "%'";

        } else {
            $query = "SELECT a.*, b.state, c.city FROM ytew5_vacancy_talents a LEFT JOIN ytew5_states b ON a.state = b.id LEFT JOIN ytew5_citys c ON a.city = c.id";
        }
        $per_page = 20;

        $columns = $this->get_columns();
        $hidden = array();
        $sortable = $this->get_sortable_columns();

        $this->_column_headers = array($columns, $hidden, $sortable);

        $this->get_bulk_actions();
        $this->process_bulk_action();
        $this->delete_item();
        $this->edit_item();
        //$this->search_items();
        $items = $wpdb->get_results($query);

        if ($items) {

            foreach($items as $item) {
                if ($item->vacancy_type == 0 ) {
                    $vacancy = $wpdb->get_row("SELECT vacancy FROM ytew5_vacancy WHERE id=$item->interest", ARRAY_A);
                    $item->vacancy = $vacancy['vacancy'];
                } else {
                    $vacancy = $wpdb->get_row("SELECT title FROM ytew5_vacancy_departments WHERE id=$item->interest", ARRAY_A);
                    $item->vacancy = $vacancy['title'];
                }
                $data[] = array(
                    'id'       => $item->id,
                    'name'     => $item->name,
                    'email'    => $item->email,
                    'phone'    => $item->phone,
                    'city'     => $item->city,
                    'state'    => $item->state,
                    'interest' => $item->vacancy,
                    'linkedin' => '<a target="_blank" href="https://www.linkedin.com/pub/' . $item->linkedin . '">' . $item->linkedin . '</a>',
                    'pdf_url'  => '<a target="_blank" class="pdf-icon" href="' . $item->pdf_url . '"></a>'
                );
            }
        } else {
            $data = array();
        }

        function usort_reorder($a,$b) {
            $orderby = (!empty($_REQUEST['orderby'])) ? $_REQUEST['orderby'] : 'id';
            $order   = (!empty($_REQUEST['order'])) ? $_REQUEST['order'] : 'asc';
            $result  = strcmp($a[$orderby], $b[$orderby]);
            return ($order==='asc') ? $result : - $result;
        }

        usort($data, 'usort_reorder');

        $current_page = $this->get_pagenum();

        $total_items = count($data);

        $data = array_slice($data,(($current_page-1) * $per_page),$per_page);

        $this->items = $data;

        $this->set_pagination_args( array(
            'total_items' => $total_items,
            'per_page'    => $per_page,
            'total_pages' => ceil($total_items / $per_page)
        ));
    }
}

$talent = new TalentListTable();
$talent->prepare_items();