<?php
/**
 * Created by PhpStorm.
 * User: anderson
 * Date: 11/9/14
 * Time: 4:21 PM
 */

if (!class_exists('WP_List_Table')) {
    require_once(ABSPATH . 'wp-admin/includes/class-wp-list-table.php');
}

class VacancyListTable extends WP_List_Table {

    /** ************************************************************************
     * REQUIRED. Set up a constructor that references the parent constructor. We
     * use the parent reference to set some default configs.
     ***************************************************************************/
    function __construct(){
        global $status, $page;

        //Set parent defaults
        parent::__construct( array(
            'singular'  => 'vacancy',     //singular name of the listed records
            'plural'    => 'vacancys',    //plural name of the listed records
            'ajax'      => true        //does this table support ajax?
        ));
    }

    function column_default($item, $column_name) {
        switch ($column_name) {
            case 'name' :
            case 'location':
            case 'amount':
                return $item[$column_name];
            break;
            case 'published':
                return $this->statuses($item);
            break;
            default:
                return print_r($item,true); //Show the whole array for troubleshooting purposes
        }
    }

    function statuses($item) {
        $class = $item['published'] == 1 ? 'enabled' : 'disabled';
        $statuses = sprintf('<a class="vacancy-status '  . $class . '" href="?page=%s&action=%s&id=%s&action_type=%s&status=%s"></a>',$_REQUEST['page'], 'edit' ,$item['id'], 'status', $item['published']);
        $action = array(
            'edit_status' => $statuses
        );

        return sprintf('%1$s', $this->row_actions($action, true));
    }

    function column_name($item) {

        //Build row actions
        $actions = array    (
            'edit'   => sprintf('<a class="edit-row" href="?page=%s&action=%s&id=%s">Edit</a>',$_REQUEST['page'], 'edit_vacancy', $item['id']),
            'delete' => sprintf('<a class="delete-vacancy" href="?page=%s&action=%s&id=%s">Delete</a>',$_REQUEST['page'], 'delete', $item['id']),
        );

        //Return the title contents
        return sprintf('%1$s %3$s',
            /*$1%s*/ '<a href="?page=' . $_REQUEST['page'] . '&action=edit_vacancy&id=' . $item['id'] . '"><b>' . $item['name'] . '</b></a>',
            /*$2%s*/ $item['id'],
            /*$3%s*/ $this->row_actions($actions)
        );
    }


    function column_cb($item) {
        return sprintf(
            '<input type="checkbox" name="%1$s[]" value="%2$s" />',
            /*$1%s*/ $this->_args['singular'],  //Let's simply repurpose the table's singular label
            /*$2%s*/ $item['id']               //The value of the checkbox should be the record's id
        );
    }

    function get_columns(){
        $columns = array(
            'cb'        => '<input type="checkbox" />', //Render a checkbox instead of text
            'name'   => 'Título da vaga',
            'location'  => 'Cidade',
            'amount'    => 'Número de vagas',
            'published' => 'Publicado'
        );
        return $columns;
    }

    function get_sortable_columns() {
        $sortable_columns = array(
            'name'   => array('vacancy' , false),     //true means it's already sorted
            'location'  => array('location', false),
            'amount'    => array('amount', false),
            'published' => array('published', false)
        );
        return $sortable_columns;
    }

    function get_bulk_actions() {
        $actions = array(
            'delete' => 'Delete'
        );
        return $actions;
    }

    function process_bulk_action() {
        wp_reset_query();
        global $wpdb;

        if ('delete' === $this->current_action() ) {

            $vacancy_id = $_GET['vacancy'];

            if ($vacancy_id) {
                for ($i = 0; $i < count($vacancy_id); $i++) {
                    $query = $wpdb->prepare(
                        "
                            DELETE FROM ytew5_vacancy WHERE id = %d
                        ",
                        $vacancy_id[$i]
                    );
                    $wpdb->query($query);
                }
            }

        }
    }

    function delete_item () {
        global $wpdb;

        if ('delete' === $this->current_action() && isset($_GET['id'])) {
            $id = $_GET['id'];
            $query = $wpdb->prepare(
                "
                    DELETE FROM ytew5_vacancy WHERE id = %d
                ",
                $id
            );
            $wpdb->query($query);
        }
    }

    function edit_item() {
        if ('edit' === $this->current_action()) {
        }
    }

    function prepare_items() {
        global $wpdb;

        if (isset($_GET['s']) && !empty($_GET['s'])) {

            $s = sanitize_text_field($_GET['s']);

            $query = "SELECT * FROM ytew5_vacancy WHERE vacancy LIKE '%" .$s. "%' OR location LIKE '%" . $s . "%' OR description LIKE '%" . $s . "%'";

        } else {
            $query = "SELECT * FROM ytew5_vacancy";
        }

        $per_page = 20;

        $columns = $this->get_columns();
        $hidden = array();
        $sortable = $this->get_sortable_columns();

        $this->_column_headers = array($columns, $hidden, $sortable);

        $this->get_bulk_actions();
        $this->process_bulk_action();
        $this->delete_item();
        $this->edit_item();
        //$this->search_items();
        $items = $wpdb->get_results($query);

        if ($items) {

            foreach($items as $item) {
                $data[] = array(
                    'id'        => $item->id,
                    'name'      => $item->vacancy,
                    'location'  => $item->location,
                    'amount'    => $item->amount,
                    'published' => $item->published
                );
            }
        } else {
            $data = array();
        }

        function usort_reorder($a,$b) {
            $orderby = (!empty($_REQUEST['orderby'])) ? $_REQUEST['orderby'] : 'id';
            $order   = (!empty($_REQUEST['order'])) ? $_REQUEST['order'] : 'asc';
            $result  = strcmp($a[$orderby], $b[$orderby]);
            return ($order==='asc') ? $result : - $result;
        }

        usort($data, 'usort_reorder');

        $current_page = $this->get_pagenum();

        $total_items = count($data);

        $data = array_slice($data,(($current_page-1) * $per_page),$per_page);

        $this->items = $data;

        $this->set_pagination_args( array(
            'total_items' => $total_items,
            'per_page'    => $per_page,
            'total_pages' => ceil($total_items / $per_page)
        ));
    }
}

$vacancy = new VacancyListTable();
$vacancy->prepare_items();