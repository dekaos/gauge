<?php
/**
 * Created by WebStorm.
 * User: anderson
 * Date: 12/5/14
 * Time: 6:13 PM
 */
if (!defined('VACANCY')) {

    header( 'Status: 403 Forbidden' );
    header( 'HTTP/1.1 403 Forbidden' );
    exit;
}

require_once(ABSPATH . 'wp-content/plugins/vacancy/class/table-vacancy-actions.php');
?>

<div class="wrap">
    <h2>Vagas <a href="?page=vacancy_list&action=new_vacancy" class="add-new-h2">Adicionar nova</a></h2>
    <form method="get">
        <input type="hidden" name="page" value="vacancy_list" />
        <?php $vacancy->search_box('search', 'search_id'); ?>
    </form>
    <form id="vacancy-filter" method="get">
        <?php $vacancy->display() ?>
        <input type="hidden" name="page" value="<?php echo $_REQUEST['page'] ?>" />
    </form>
</div>