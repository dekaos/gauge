<?php
    if (!defined('VACANCY')) {
        header( 'Status: 403 Forbidden' );
        header( 'HTTP/1.1 403 Forbidden' );
        exit;
    }
?>
<div class="wrap">
    <h2>
        <?php
            if ($_GET['action'] == 'new_vacancy') {
                echo 'Adicionar nova vaga';
            } else {
                echo 'Editar Vaga';
                echo '<a href="?page=vacancy_list&action=new_vacancy" class="add-new-h2">Adicionar nova</a>';
            }
        ?>
    </h2>
    <div id="poststuff">
        <div id="post-body" class="columns-2 vacancy">
            <div id="post-body-content">
                <form id="new-vacancy" name="new-vacancy">
                    <div id="titlediv">
                        <label for="vacancy-title">Título da vaga</label>
                        <input id="vacancy-title" name="vacancy-name" type="text" value="<?php echo $data['vacancy']; ?>" required>
                    </div>
                    <div id="locationdiv">
                        <label for="vacancy-location">Cidade</label>
                        <select name="vacancy-location" id="vacancy-location" required>
                            <option value="<?php echo $data['location'] ? $data['location'] : ''; ?>" selected><?php echo $data['location'] ? $data['location'] : ''; ?></option>
                            <?php foreach($citys as $city) :
                                $local = json_decode($city, true); ?>
                            <option value="<?php echo $local[0]['office_one']['city'] . ' - ' . $local[0]['office_one']['state'] ; ?>">
                                <?php echo $local[0]['office_one']['city'] . ' - ' . $local[0]['office_one']['state'] ; ?>
                            </option>
                            <option value="<?php echo $local[0]['office_two']['city'] . ' - ' . $local[0]['office_two']['state'] ; ?>">
                                <?php echo $local[0]['office_two']['city'] . ' - ' . $local[0]['office_two']['state'] ; ?>
                            </option>
                            <option value="<?php echo $local[0]['office_tree']['city'] . ' - ' . $local[0]['office_tree']['state'] ; ?>">
                                <?php echo $local[0]['office_tree']['city'] . ' - ' . $local[0]['office_tree']['state'] ; ?>
                            </option>
                            <?php endforeach; ?>
                        </select>
                    </div>
                    <div class="departmentdiv">
                        <label for="vacancy-department">Área</label>
                        <select name="vacancy-department" id="vacancy-department">
                            <option value="" selected></option>
                            <?php foreach ($departments as $department) : ?>
                                <option value="<?php echo $department['id']; ?>" <?php echo $data['department'] == $department['id'] ? ' selected' : ''; ?>><?php echo $department['title']; ?></option>
                            <?php endforeach; ?>
                        </select>
                        <a href="#" class="button button-large" id="new-department">Adicionar</a>
                    </div>
                    <?php wp_editor($data['description'], 'vacancy-description'); ?>
                    <div id="amountdiv">
                        <label for="vacancy-amount">Número de vagas</label>
                        <input id="vacancy-amount" name="vacancy-amount" type="number" min="1" value="<?php echo $data['amount']; ?>" required>
                    </div>
                    <div id="publisheddiv">
                        <input type="checkbox" value="1" name="vacancy-published" id="vacancy-published" <?php echo $data['published'] == 1 ? ' checked' : ''; ?>><label for="vacancy-published"><b>Publicado</b></label>
                    </div>
                    <div id="submitdiv">
                        <input name="vacancy-id" id="vacancy-id" type="hidden" value="<?php echo $data['id']; ?>">
                        <input value="<?php echo $data['id'] ? 'Atualizar' : 'Adicionar'; ?>" class="button button-primary button-large" id="vacancy-submit" type="submit">
                        <span class="sending hide">
                            <img src="<?php echo get_site_url() . '/wp-includes/images/spinner.gif'; ?>" alt=""/>
                        </span>
                        <span class="message"></span>
                    </div>
                </form>
            </div>
        </div>
    </div>
</div>
<div id="departments" style="display: none">
    <form id="departments-form" name="departments-form" action="">
        <span id="form-close">X</span>
        <span class="message"></span>
        <h2>Novo departamento</h2>
        <input type="text" id="department-title" name="department-title" placeholder="Nome" required>
        <input name="action" value="add_department" type="hidden">
        <input class="button button-primary button-large" type="submit" value="<?php echo ($_GET['action'] == 'new_vacancy' ? 'Adicionar' : 'Editar'); ?>">
        <span class="sending hide">
            <img src="<?php echo get_site_url() . '/wp-includes/images/spinner.gif'; ?>" alt=""/>
        </span>
    </form>
</div>