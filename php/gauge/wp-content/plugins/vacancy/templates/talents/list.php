<?php
/**
 * Created by WebStorm.
 * User: anderson
 * Date: 12/5/14
 * Time: 6:13 PM
 */
if (!defined('VACANCY')) {

    header( 'Status: 403 Forbidden' );
    header( 'HTTP/1.1 403 Forbidden' );
    exit;
}

require_once(ABSPATH . 'wp-content/plugins/vacancy/class/table-talent-actions.php');

?>

<div class="wrap">
    <h2>Cadastrados</h2>
    <form method="get">
        <input type="hidden" name="page" value="talent_list" />
        <?php $talent->search_box('search', 'search_id'); ?>
    </form>
    <form id="vacancy-filter" method="get">
        <?php $talent->display() ?>
        <input type="hidden" name="page" value="<?php echo $_REQUEST['page'] ?>" />
    </form>
</div>
