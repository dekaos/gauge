<?php
if (!defined('VACANCY')) {
    header( 'Status: 403 Forbidden' );
    header( 'HTTP/1.1 403 Forbidden' );
    exit;
}
?>

<div class="wrap">
    <h2>Editar Candidato</h2>
    <div id="poststuff">
        <div id="post-body" class="columns-2 talent">
            <div id="post-body-content">
                <form id="talent-form-edit" name="talent-form-edit" method="post" enctype="multipart/form-data">
                    <div class="input-group">
                        <label for="nv-name"><?php _e('Name', 'roots'); ?></label>
                        <input id="nv-name" name="nv-name" class="form-control" type="text" value="<?php echo $data['name']; ?>" required>
                    </div>
                    <div class="input-group">
                        <label for="nv-email">E-mail</label>
                        <input id="nv-email" name="nv-email" class="form-control" type="text" value="<?php echo $data['email']; ?>" required>
                    </div>
                    <div class="clearfix"></div>
                    <div class="input-group select-state pull-left">
                        <label for="nv-state">
                            <?php _e('State', 'roots'); ?>
                        </label>
                        <div class="form-inline">
                            <div class="btn-group">
                                <select id="nv-state" name="nv-state" required>
                                    <?php foreach($states as $state) : ?>
                                    <option id="nv-state-<?php echo $state->id; ?>" value="<?php echo $state->id; ?>" <?php echo $data['state'] == $state->id ? ' selected' : ''; ?>><?php echo $state->state; ?></option>
                                    <?php endforeach; ?>
                                </select>
                            </div>
                        </div>
                    </div>
                    <div class="input-group select-city pull-left">
                        <label for="nv-city">
                            <?php _e('City', 'roots'); ?>
                        </label>
                        <div class="form-inline">
                            <div class="btn-group">
                                <select id="nv-city" name="nv-city" required>
                                    <option value="<?php echo $data['city']; ?>">
                                        <?php echo $city['city']; ?>
                                    </option>
                                </select>
                            </div>
                        </div>
                    </div>
                    <div style="clear: both">
                    <div class="input-group">
                        <label for="nv-phone"><?php _e('DDD + Phone', 'roots'); ?></label>
                        <input id="nv-phone" class="form-control" name="nv-phone" type="text" value="<?php echo $data['phone']; ?>" required>
                    </div>
                    <?php if (isset($departments)) : ?>
                    <div class="input-group departments">
                        <label for="nv-interest">
                            <?php _e('Area of interest','roots'); ?>
                        </label>
                        <select id="nv-interest" name="nv-interest-0">
                            <?php echo $data['vacancy_type'] != 1 ? '<option value="" selected></option>' : ''; ?>
                            <?php foreach($departments as $department) : ?>
                                <?php if ($data['vacancy_type'] == 1) : ?>
                                    <?php if ($data['interest'] == $department['id']) : ?>
                                        <?php $selected = 'selected'; ?>
                                        <?php else : ?>
                                            <?php $selected = ''; ?>
                                    <?php endif; ?>
                                <?php endif; ?>
                                <option id="nv-interest-<?php echo $department['id']; ?>" value="<?php echo $department['id']; ?>" <?php echo $selected; ?>>
                                    <?php echo $department['title']; ?>
                                </option>
                            <?php endforeach; ?>
                        </select>
                    </div>
                    <?php endif; ?>
                        <?php if (isset($vacancies)) : ?>
                            <b>Ou</b>
                            <div class="input-group vacancies">
                            <label for="nv-interest">
                                Vaga
                            </label>
                            <div class="form-inline">
                                <div class="btn-group interest-area">
                                    <select id="nv-interest-1" name="nv-interest-1">
                                        <?php echo $data['vacancy_type'] != 0 ? '<option value="" selected></option>' : ''; ?>
                                        <?php foreach($vacancies as $vacancy) : ?>
                                            <?php if ($data['vacancy_type'] == 0) : ?>
                                                <?php if ($data['interest'] == $vacancy->id) : ?>
                                                    <?php $selected = 'selected'; ?>
                                                    <?php else : ?>
                                                        <?php $selected = ''; ?>
                                                <?php endif; ?>
                                            <?php endif; ?>
                                            <option id="nv-interest-<?php echo $vacancy->id; ?>" value="<?php echo $vacancy->id; ?>" <?php echo $selected; ?>>
                                                <?php echo $vacancy->vacancy; ?>
                                            </option>
                                        <?php endforeach; ?>
                                    </select>
                                </div>
                            </div>
                        </div>
                    <?php endif; ?>
                    <div style="clear:both"></div>
                    <div class="input-group linkedin">
                        <label for="nv-likedin">
                            <?php _e('LinkedIn profile', 'roots'); ?>
                        </label>
                        <input class="form-control" id="nv-linkedin" name="nv-linkedin" type="text" value="<?php echo $data['linkedin']; ?>" required>
                    </div>
                    <div class="input-group">
                        <label for="curriculum">Curriculum</label>
                        <a target="_blank" class="pdf-icon" href="<?php echo $data['pdf_url']; ?>"></a>
                    </div>
                    <div class="input-group file-upload">
                        <label for="nv-upload">
                            <?php _e('Insert a file', 'roots'); ?>
                        </label>
                        <input name="nv-upload" class="nv-upload" id="nv-upload" type="file" accept="application/pdf">
                        <span>
                            <?php _e('Maximum size: 5mb / Format: pdf', 'roots'); ?>
                        </span>
                    </div>
                    <div class="input-group">
                        <input name="nv-interest" value="<?php echo $data['interest']; ?>" type="hidden"/>
                        <input name="action" id="nv-action" value="edit_talent" type="hidden">
                        <input name="vacancy-type" id="vacancy-type" value="<?php echo $data['vacancy_type']; ?>" type="hidden">
                        <input name="talent-id" id="talent-id" type="hidden" value="<?php echo $data['id']; ?>">
                        <input name="curriculum" id="talent-curriculum" type="hidden" value="<?php echo $data['pdf_url']; ?>">
                        <input name="nv-interest" type="hidden" value="<?php echo $data['interest']; ?>">
                        <input id="nv-submit" type="submit" class="button button-primary button-large" value="Atualizar cadastro">
                        <span class="sending hide">
                            <img src="<?php echo get_site_url() . '/wp-includes/images/spinner.gif'; ?>" alt=""/>
                        </span>
                        <span class="message"></span>
                    </div>
                    </div>
                </form>
            </div>
        </div>
    </div>
</div>