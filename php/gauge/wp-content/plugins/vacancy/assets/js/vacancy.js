var action, response;

jQuery('.delete-vacancy, .delete-talent').on('click', function () {
    return confirm('Deseja realmente deletar este item?');
});

jQuery('#new-vacancy').on('submit', function (e) {
    e.preventDefault();
    jQuery(this).find( '.wp-editor-area' ).each(function() {
        var id = jQuery( this ).attr( 'id' ),
            sel = '#wp-' + id + '-wrap',
            container = jQuery( sel ),
            editor = tinyMCE.get( id );

        // If the editor is in "visual" mode then we need to do something.
        if ( editor && container.hasClass( 'tmce-active' ) ) {
            // Saves the contents from a editor out to the textarea:
            editor.save();
        }
    });

    jQuery('#new-vacancy .message').stop().fadeOut();
    jQuery('#new-vacancy .message').removeClass('success error');

    if (jQuery('#vacancy-id').val().length > 0) {
        action = 'edit_vacancy';
    } else {
        action = 'add_vacancy';
    }

    jQuery('#new-vacancy .sending').removeClass('hide');

    jQuery.ajax({
        url: ajaxurl,
        type: 'POST',
        data: jQuery(this).serialize() + '&action=' + action,
        success: function (data) {
            response = jQuery.parseJSON(data);
            jQuery('#new-vacancy .message').addClass(response[0].message_type).text(response[0].message).stop().fadeIn();
            jQuery('#new-vacancy .sending').addClass('hide');

            if (action === 'add_vacancy' && response[0].message_type !== 'error') {
                jQuery('#vacancy-submit').attr('disabled', 'disabled');
            }

            setTimeout(function () {
                if(typeof response[0].last_id !== 'undefined') {
                    jQuery(location).attr('href', '?page=vacancy_list&action=edit_vacancy&id=' + response[0].last_id);
                }

            }, 2000);
        }
    });
});

jQuery('#new-department').on('click', function (e) {
    jQuery('#departments').css({
        'display': 'block',
        'position': 'fixed',
        'top': '0',
        'left': '0',
        'right': '0',
        'bottom': '0',
        'z-index': '99999',
        'background-color': 'rgba(0, 0, 0, 0.75)'
    });
});

jQuery('#departments-form').on('submit', function (e) {

    jQuery('#departments-form .message').fadeOut();

    jQuery('#departments .sending').removeClass('hide');

    jQuery.ajax({
        url: ajaxurl,
        type: 'POST',
        data: jQuery(this).serialize(),
        success: function (data) {
            response = jQuery.parseJSON(data);
            jQuery('#departments-form .message').addClass(response[0].message_type).text(response[0].message).fadeIn();
            jQuery('#departments .sending').addClass('hide');
        }
    });

    e.preventDefault();
});

jQuery('#form-close').on('click', function () {
    jQuery('#departments').css({
        'display': 'none'
    })
});

jQuery('.vacancy-status').on("click", function (e) {
    e.preventDefault();

		var urlHref     = jQuery(this).attr('href');
    var vacancyId   = getURLParameter(urlHref, 'id');
    var status      = getURLParameter(urlHref, 'status');
    var actionType  = getURLParameter(urlHref, 'action_type');
    var changeClass = jQuery(this);

		jQuery.ajax({
        url: ajaxurl,
        type: 'POST',
        data: {
            action: 'edit_vacancy',
            'vacancy-action-type': actionType,
            'vacancy-status'     : status,
            'vacancy-id'         : vacancyId
        },
        success: function (data) {
	        changeClass.toggleClass('disabled');

	        setTimeout(function () {
						window.location = urlHref;
					}, 100);
        }
    });
});

jQuery('#nv-state').on('change', function () {

    jQuery.ajax({
        url: ajaxurl,
        type: 'post',
        data: {
            action: 'get-citys',
            state_id: jQuery(this).val()
        },
        success: function (data) {
            if (data) {
                var city = jQuery.parseJSON(data);
                var values = '';

                for (var i in city) {
                    values += '<option id="city-' + city[i].city_id + '" value="' + city[i].city_id + '">' + city[i].city_name + '</option>';
                }
                jQuery('#nv-city').html(values);
            }
        }
    });
});

jQuery('#nv-interest').on('change', function () {
    jQuery('#vacancy-type').val('1');
    jQuery('#nv-interest-1 option').filter(function() {
        return !this.value || jQuery.trim(this.value).length == 0;
    }).remove();
    jQuery('#nv-interest-1').prepend('<option value="" selected></option>');
    jQuery('input[name="nv-interest"]').val(jQuery(this).val());
});

jQuery('#nv-interest-1').on('change', function () {
    jQuery('#vacancy-type').val('0');
    jQuery('#nv-interest option').filter(function() {
        return !this.value || jQuery.trim(this.value).length == 0;
    }).remove();
    jQuery('#nv-interest').prepend('<option value="" selected></option>');
    jQuery('input[name="nv-interest"]').val(jQuery(this).val());
});

jQuery('#talent-form-edit').ajaxForm({
    url: ajaxurl,
    beforeSend: function () {
        jQuery('#talent-form-edit .message').hide();
        jQuery('.talent .sending').removeClass('hide');
    },
    success: function (data) {
        var response = jQuery.parseJSON(data);
        jQuery('#talent-form-edit .message').addClass(response[0].message_type).text(response[0].message).fadeIn();
        jQuery('#talent-form-edit .message').show();
        jQuery('.talent .sending').addClass('hide');
    }
});

jQuery('#nv-upload').on('change', function () {

    var file = jQuery(this)[0].files[0];
    var size = file.size;
    var fileName = file.name;
    var fileExt = '.' + fileName.split('.').pop();

    if (size > 5242880 ) {
        alert('O arquivo deve ser menor que 5MB.');
        return false;
    }

    if (fileExt.toString().toLowerCase() !== '.pdf') {
        alert('Você só pode enviar arquivos do tipo PDF');
        return false;
    }
});

function getURLParameter(url, name) {
    return (RegExp(name + '=' + '(.+?)(&|$)').exec(url)||[,null])[1];
}