<?php
/**
 * Created by WebStorm.
 * User: anderson
 * Date: 1/12/15
 * Time: 3:22 PM
 */

if (!class_exists('WP_List_Table')) {
    require_once(ABSPATH . 'wp-admin/includes/class-wp-list-table.php');
}

class AnalysesListTable extends WP_List_Table {

    /** ************************************************************************
     * REQUIRED. Set up a constructor that references the parent constructor. We
     * use the parent reference to set some default configs.
     ***************************************************************************/
    function __construct(){
        global $status, $page;

        //Set parent defaults
        parent::__construct( array(
            'singular' => 'analyse',     //singular name of the listed records
            'plural'   => 'analyses',    //plural name of the listed records
            'ajax'     => true        //does this table support ajax?
        ));
    }

    function column_default($item, $column_name) {
        switch ($column_name) {
            case 'name'           :
            case 'group_text'     :
            case 'group_images'   :
            case 'group_shortcode':
                return $item[$column_name];
                break;
            default:
                return print_r($item,true); //Show the whole array for troubleshooting purposes
        }
    }

    function column_name($item) {

        //Build row actions
        $actions = array    (
            'edit'   => sprintf('<a class="edit-row" href="?page=%s&action=%s&id=%s">Edit</a>',$_REQUEST['page'], 'edit_group', $item['id']),
            'delete' => sprintf('<a class="delete-row" href="?page=%s&action=%s&id=%s">Delete</a>',$_REQUEST['page'], 'delete', $item['id']),
        );

        //Return the title contents
        return sprintf('%1$s %3$s',
            /*$1%s*/ '<a href="?page=' . $_REQUEST['page'] . '&action=edit_group&id=' . $item['id'] . '"><b>' . $item['name'] . '</b></a>',
            /*$2%s*/ $item['id'],
            /*$3%s*/ $this->row_actions($actions)
        );
    }


    function column_cb($item) {
        return sprintf(
            '<input type="checkbox" name="%1$s[]" value="%2$s" />',
            /*$1%s*/ $this->_args['singular'],  //Let's simply repurpose the table's singular label
            /*$2%s*/ $item['id']               //The value of the checkbox should be the record's id
        );
    }

    function get_columns(){
        $columns = array(
            'cb'              => '<input type="checkbox" />',
            'name'            => 'Título',
            'group_text'      => 'Texto',
            'group_images'    => 'Imagens',
            'group_shortcode' => 'Shortcode'
        );
        return $columns;
    }

    function get_sortable_columns() {
        $sortable_columns = array(
            'name' => array('group_title' , false),     //true means it's already sorted
        );
        return $sortable_columns;
    }

    function get_bulk_actions() {
        $actions = array(
            'delete' => 'Delete'
        );
        return $actions;
    }

    function process_bulk_action() {
        wp_reset_query();
        global $wpdb;

        if ('delete' === $this->current_action() ) {

            $group_id = $_GET['analyse'];

            if ($group_id) {
                for ($i = 0; $i < count($group_id); $i++) {
                    $query = $wpdb->prepare(
                        "
                            DELETE FROM ytew5_analyses WHERE id = %d
                        ",
                        $group_id[$i]
                    );
                    $wpdb->query($query);
                }
            }

        }
    }

    function delete_item () {
        global $wpdb;

        if ('delete' === $this->current_action() && isset($_GET['id'])) {
            $id = $_GET['id'];
            $query = $wpdb->prepare(
                "
                    DELETE FROM ytew5_analyses WHERE id = %d
                ",
                $id
            );
            $wpdb->query($query);
        }
    }

    function edit_item() {
        if ('edit' === $this->current_action()) {
        }
    }

    function prepare_items() {
        global $wpdb;

        if (isset($_GET['s']) && !empty($_GET['s'])) {

            $s = sanitize_text_field($_GET['s']);

            $query = "SELECT * FROM ytew5_analyses WHERE group_title LIKE '%" .$s. "%' OR group_text LIKE '%" . $s . "%'";

        } else {
            $query = "SELECT * FROM ytew5_analyses";
        }
        $per_page = 20;

        $columns = $this->get_columns();
        $hidden = array();
        $sortable = $this->get_sortable_columns();

        $this->_column_headers = array($columns, $hidden, $sortable);

        $this->get_bulk_actions();
        $this->process_bulk_action();
        $this->delete_item();
        $this->edit_item();
        //$this->search_items();
        $items = $wpdb->get_results($query);

        if ($items) {

            foreach($items as $item) {
                $data[] = array(
                    'id'              => $item->id,
                    'name'            => $item->group_title,
                    'group_text'      => $item->group_text,
                    'group_images'    => '<div class="brands-list">' . $item->group_images . '</div>',
                    'group_shortcode' => '[analise group-id=' . $item->id . ']'
                );
            }
        } else {
            $data = array();
        }

        function usort_reorder($a,$b) {
            $orderby = (!empty($_REQUEST['orderby'])) ? $_REQUEST['orderby'] : 'id';
            $order   = (!empty($_REQUEST['order'])) ? $_REQUEST['order'] : 'asc';
            $result  = strcmp($a[$orderby], $b[$orderby]);
            return ($order==='asc') ? $result : - $result;
        }

        usort($data, 'usort_reorder');

        $current_page = $this->get_pagenum();

        $total_items = count($data);

        $data = array_slice($data,(($current_page-1) * $per_page),$per_page);

        $this->items = $data;

        $this->set_pagination_args( array(
            'total_items' => $total_items,
            'per_page'    => $per_page,
            'total_pages' => ceil($total_items / $per_page)
        ));
    }
}

$analyse = new AnalysesListTable();
$analyse->prepare_items();