/**
 * Created by anderson on 1/11/15.
 */

jQuery(document).ready(function ($) {

    var elFrame, contentImages, urlImages = [];

    jQuery('#upload-images').click(function() {

        if ( elFrame ) {
            elFrame.open();
            return;
        }

        elFrame = wp.media.frames.elFrame = wp.media({
            multiple: true,
            library: {
                type: 'image'
            }
        });

        elFrame.on('select', function(){
            var selection = elFrame.state().get('selection');
            selection.map( function( attachment ) {
                attachment    = attachment.toJSON();
                contentImages = jQuery('#images-hold').append('<span class="brands"><img src="' + attachment.url + '" width="80"></span>');

                //newUrlImages.push(attachment.url);
            });
        });

        elFrame.open();
    });

    jQuery('#images-hold').on('click', '.brands', function () {
        urlImages = [];

        jQuery(this).remove();

        getUrlImages();
    });

    jQuery('#new-group').on('submit', function (e) {
        e.preventDefault();

        jQuery(this).find( '.wp-editor-area' ).each(function() {
            var id = jQuery( this ).attr( 'id' ),
              sel = '#wp-' + id + '-wrap',
              container = jQuery( sel ),
              editor = tinyMCE.get( id );

            // If the editor is in "visual" mode then we need to do something.
            if ( editor && container.hasClass( 'tmce-active' ) ) {
                // Saves the contents from a editor out to the textarea:
                editor.save();
            }
        });

        jQuery('.sending').removeClass('hide');
        jQuery('#response').hide();

        jQuery.ajax({
            type: 'post',
            url: ajaxurl,
            data: {
                action         : 'analyse-group',
                group_action   : jQuery('#group-action').val(),
                group_id       : jQuery('#group-id').val(),
                group_title    : jQuery('#group-title').val(),
                group_images   : getUrlImages(),
                group_text     : jQuery('#group-text').val()
            },
            success: function (data) {
                console.log(data);
                jQuery('.sending').addClass('hide');

                var response = jQuery.parseJSON(data);

                jQuery('#response').addClass(response.type).text(response.message).show();

                if (response.type === 'error') {
                    jQuery('#new-group-button').removeAttr('disabled');
                } else if (response.type === 'success') {
                    jQuery('#new-group-button').attr('disabled', 'disabled');
                }
            }
        });
    });

    function getUrlImages () {

        jQuery('.brands img').each(function (i) {
            urlImages[i] = jQuery(this).attr('src');
        });

        return urlImages;
    }
});
