<?php
/**
 * Created by WebStorm.
 * User: anderson
 * Date: 1/10/15
 * Plugin Name: Gauge Análises
 * Time: 11:11 AM
 */

if ( ! defined( 'ABSPATH' ) ) exit; // Exit if accessed directly

add_action('admin_menu', 'register_gauge_analises');
add_action('admin_enqueue_scripts', 'admin_analyses_js');
add_action('init', 'register_shortcodes');
add_action('wp_ajax_analyse-group', 'analyse_group');
wp_enqueue_style('admin_css_analises', '/wp-content/plugins/' . basename(dirname(__FILE__)) . '/css/analises.css', false, '1.0.0' );

function register_shortcodes(){

}

function register_gauge_analises () {
    add_menu_page('Gauge análises', 'Análises', 'manage_options', 'gauge_analises', 'gauge_analises', ' dashicons-clipboard', 71);
}



function gauge_analises () {

    if (!current_user_can( 'manage_options' )) {
        wp_die( __( 'You do not have sufficient permissions to access this page.' ) );
    }

    if ($_GET['action'] == 'new_group' || $_GET['action'] == 'edit_group') {

        wp_enqueue_media();

        if ($_GET['action'] == 'edit_group') {
            $id   = sanitize_text_field($_GET['id']);
            $data = get_group($id);
        }
        require_once('templates/html/edit.php');
    } else {
        require_once('templates/html/list.php');
    }
}

function get_analise () {
    global $post, $wpdb;

    //$an_pattern = get_shortcode_regex();

    $group_analyse = (explode('[analise group-id=', $post->post_content));
    if ($group_analyse[1]) {
        //$an_shortcode = $an_matches[3];
        $group_id     = $group_analyse[1];

        $group  = $wpdb->get_row("SELECT * FROM ytew5_analyses WHERE id='$group_id'", ARRAY_A);
        if ($group['group_images'] || $group['group_text']) {
            $an_output  = '<div class="analyses-wrapper">';
            $an_output .= '<div class="analyses">';
            $an_output .= '<div class="analyses-inner">';
            if ($group['group_images']) {
                $an_output .= '<div class="group-images">';
                $an_output .= $group['group_images'];
                $an_output .= '</div>';
            }
            if ($group['group_text']) {
                //$lines   = explode(' ', $group['group_text']);
                $an_output .= '<div class="group-txt">';
                $an_output .= wpautop($group['group_text']);
                $an_output .= '<div class="clearfix"></div>';
                /*foreach ($lines as $line) {
                    if (strpos($line, '.')) {
                        $line = str_replace('.', '', $line);
                    }

                    if (is_numeric($line)) {
                        $an_output .= '<span class="group-number">' . str_replace(',', '.', number_format($line)) . '</span><span class="clearfix"></span>';
                    } else {
                        $an_output .= '<span class="group-legend">' . $line . '</span>';
                    }
                }*/
                $an_output .= '</div>';
            }
            $an_output .= '</div>';
            $an_output .= '</div>';
            $an_output .= '</div>';
            $an_output .= '<div class="clearfix"></div>';

            return $an_output;
        }
    }
}

function analyse_group () {
    $group_id     = sanitize_text_field($_POST['group_id']);
    $group_title  = sanitize_text_field($_POST['group_title']);
    $group_images = $_POST['group_images'];
    $group_text   = stripslashes_deep($_POST['group_text']);
    $group_action = sanitize_text_field($_POST['group_action']);
    $brand_images = '';

    if ($group_images) {
        foreach ($group_images as $url) {
            $image_url     = sanitize_text_field($url);
            $brand_images .= '<span class="brands">';
            $brand_images .= '<img src="' . $image_url . '">';
            $brand_images .= '</span>';
        }
    }

    $group_content = array(
        'group-id'     => $group_id,
        'group-title'  => $group_title,
        'group-text'   => $group_text,
        'group-images' => $brand_images,
        'group-action' => $group_action
    );

    process_group($group_content);
}

function process_group($group_content) {
    global $wpdb;

    if ($group_content['group-action'] == 'new-group') {
        $query = $wpdb->insert('ytew5_analyses',
            array(
                'group_title'  => $group_content['group-title'],
                'group_text'   => $group_content['group-text'],
                'group_images' => $group_content['group-images']
            ),
            array('%s', '%s', '%s')
        );
        /*$query = $wpdb->prepare(
            "
                INSERT INTO ytew5_analyses (
                    group_title, group_text, group_images
                ) VALUES (
                    %s, %s, %s
                )
            ",
            $group_content['group-title'],
            $group_content['group-text'],
            $group_content['group-images']
        );*/

        if ($wpdb->query($query)) {
            echo json_encode(array('message' => 'Grupo adicionado com sucesso', 'type' => 'success'));
        } else {
            echo json_encode(array('message' => 'Erro ao tentar adicionar, por favor tente novamente.', 'type' => 'error'));
        }

    } else {
        $id = $group_content['group-id'];

        $query = $wpdb->prepare(
            "
                UPDATE ytew5_analyses SET group_title = %s, group_text = %s, group_images = %s WHERE id = $id
            ",
            $group_content['group-title'],
            $group_content['group-text'],
            $group_content['group-images']
        );

        if ($wpdb->query($query) || $wpdb->query($query) == 0) {
            echo json_encode(array('message' => 'Grupo atualizado com sucesso', 'type' => 'success'));
        } else {
            echo json_encode(array('message' => 'Erro ao tentar atualizar, por favor tente novamente', 'type' => 'error'));
        }
    }

    exit;
}

function get_group ($id) {
    global $wpdb;

    $query = $wpdb->get_row("SELECT * FROM ytew5_analyses WHERE id='$id'", ARRAY_A);

    return $query;
}

function admin_analyses_js() {
    wp_enqueue_script('admin_js_analyses', '/wp-content/plugins/analises/js/admin_script.js', array(), '1.0.1', true );
}

add_shortcode('analise', 'get_analise');