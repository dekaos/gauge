<?php
/**
 * Created by WebStorm.
 * User: anderson
 * Date: 1/11/15
 * Time: 5:15 PM
 */

if ( ! defined( 'ABSPATH' ) ) exit;

if (isset($_GET['action']) && $_GET['action'] == 'new_group') {
    $title  = 'Adicionar grupo';
    $action = 'new-group';
    $submit = 'Salvar';
} else {
    $title  = 'Editar grupo';
    $action = 'edit-group';
    $submit = 'Atualizar';
}

?>
<div class="group-form">
    <h2><?php echo $title; ?> <a href="?page=gauge_analises&action=new_group" class="add-new-h2">Adicionar novo</a></h2>
    <form name="new-group" id="new-group">
        <div class="form-group">
            <label for="group-title">Título</label>
            <input name="group-title" id="group-title" class="form-control" type="text" value="<?php echo $data['group_title']; ?>" required>
        </div>
        <div class="form-group">
            <div id="images-hold">
                <?php echo $data['group_images']; ?>
            </div>
            <input id="upload-images" class="button button-large" value="Adicionar imagens" type="button">
        </div>
        <div class="form-group text-editor">
            <?php wp_editor($data['group_text'], 'group-text'); ?>
        </div>
        <div class="form-group">
            <input name="group-id" id="group-id" value="<?php echo $data['id']; ?>" type="hidden">
            <input name="group-action" id="group-action" type="hidden" value="<?php echo $action; ?>">
            <input value="<?php echo $submit; ?>" id="<?php echo $action; ?>-button" class="button button-primary button-large" type="submit">
        <span class="sending hide">
            <img src="<?php echo get_site_url() . '/wp-includes/images/spinner.gif'; ?>" alt=""/>
        </span>
            <span id="response"></span>
        </div>
    </form>
</div>