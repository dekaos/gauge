<?php
/**
 * Created by WebStorm.
 * User: anderson
 * Date: 1/12/15
 * Time: 3:21 PM
 */

require_once(ABSPATH . 'wp-content/plugins/analises/table-list.php');

?>

<div class="wrap">
    <h2>Grupos <a href="?page=gauge_analises&action=new_group" class="add-new-h2">Adicionar novo</a></h2>
    <form method="get">
        <input type="hidden" name="page" value="gauge_analises" />
        <?php $analyse->search_box('search', 'search_id'); ?>
    </form>
    <form id="analyse-filter" method="get">
        <?php $analyse->display() ?>
        <input type="hidden" name="page" value="<?php echo $_REQUEST['page'] ?>" />
    </form>
</div>