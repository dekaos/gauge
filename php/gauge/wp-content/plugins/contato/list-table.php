<?php
/**
 * Created by PhpStorm.
 * User: anderson
 * Date: 11/9/14
 * Time: 4:21 PM
 */

if (!defined('ABSPATH')) {
    header( 'Status: 403 Forbidden' );
    header( 'HTTP/1.1 403 Forbidden' );
    exit;
}

if (!class_exists('WP_List_Table')) {
    require_once(ABSPATH . 'wp-admin/includes/class-wp-list-table.php');
}

class ResearchListTable extends WP_List_Table {

    /** ************************************************************************
     * REQUIRED. Set up a constructor that references the parent constructor. We
     * use the parent reference to set some default configs.
     ***************************************************************************/
    function __construct(){
        global $status, $page;

        //Set parent defaults
        parent::__construct( array(
            'singular'  => 'research',     //singular name of the listed records
            'plural'    => 'researchs',    //plural name of the listed records
            'ajax'      => true        //does this table support ajax?
        ));
    }

    function column_default($item, $column_name) {
        switch ($column_name) {
            case 'name' :
            case 'email':
            case 'phone':
            case 'city' :
            case 'state':
                return $item[$column_name];
            default:
                return print_r($item,true); //Show the whole array for troubleshooting purposes
        }
    }


    function column_name($item) {

        //Build row actions
        $actions = array    (
            'edit'   => sprintf('<a class="edit-row" href="?page=%s&action=%s&id=%s">Edit</a>',$_REQUEST['page'],'edit',$item['id']),
            'delete' => sprintf('<a class="delete-row" href="?page=%s&action=%s&id=%s">Delete</a>',$_REQUEST['page'],'delete',$item['id']),
        );

        //Return the title contents
        return sprintf('%1$s %3$s',
            /*$1%s*/ '<a href="?page=' . $_REQUEST['page'] . '&action=edit&id=' . $item['id'] . '"><b>' . $item['name'] . '</b></a>',
            /*$2%s*/ $item['id'],
            /*$3%s*/ $this->row_actions($actions)
        );
    }


    function column_cb($item) {
        return sprintf(
            '<input type="checkbox" name="%1$s[]" value="%2$s" />',
            /*$1%s*/ $this->_args['singular'],  //Let's simply repurpose the table's singular label
            /*$2%s*/ $item['id']               //The value of the checkbox should be the record's id
        );
    }

    function get_columns(){
        $columns = array(
            'cb'    => '<input type="checkbox" />', //Render a checkbox instead of text
            'name'  => 'Nome',
            'email' => 'Email',
            'phone' => 'Telefone',
            'city'  => 'Cidade',
            'state' => 'Estado'
        );
        return $columns;
    }

    function get_sortable_columns() {
        $sortable_columns = array(
            'name'  => array('name' , false),     //true means it's already sorted
            'email' => array('email', false),
            'phone' => array('phone', false),
            'city'  => array('city' , false),
            'state' => array('state', false)
        );
        return $sortable_columns;
    }

    function get_bulk_actions() {
        $actions = array(
            'delete' => 'Delete'
        );
        return $actions;
    }

    function process_bulk_action() {
        wp_reset_query();
        global $wpdb;

        if ('delete' === $this->current_action() ) {
            $research_id = $_GET['research'];
            $id = $_GET['id'];
            if ($research_id) {
                for ($i = 0; $i < count($research_id); $i++) {
                    $query = $wpdb->prepare(
                        "
                            DELETE FROM ytew5_research_contacts WHERE id = %d
                        ",
                        $research_id[$i]
                    );
                    $wpdb->query($query);
                }
            }

        }
    }

    function delete_item () {
        global $wpdb;

        if ('delete' === $this->current_action() && isset($_GET['id'])) {
            $id = $_GET['id'];
            $query = $wpdb->prepare(
                "
                        DELETE FROM ytew5_research_contacts WHERE id = %d
                    ",
                $id
            );
            $wpdb->query($query);
        }
    }

    function edit_item() {
        if ('edit' === $this->current_action()) {
        }
    }

    function prepare_items() {
        global $wpdb;

        if (isset($_GET['s']) && !empty($_GET['s'])) {
            $s = sanitize_text_field($_GET['s']);
            $query = "SELECT a.*, b.state FROM ytew5_research_contacts a LEFT JOIN ytew5_states b ON a.state = b.id WHERE a.name LIKE '%" .$s. "%' OR a.email LIKE '%" . $s . "%' OR a.phone LIKE '%" . $s . "%' OR a.email LIKE '%" . $s . "%' OR a.city LIKE '%" . $s . "%' OR a.state LIKE '%" . $s . "%' OR a.occupation LIKE '%" . $s . "%'";
        } else {
            $query = "SELECT a.*, b.state FROM ytew5_research_contacts a LEFT JOIN ytew5_states b ON a.state = b.id";
        }

        $per_page = 20;

        $columns = $this->get_columns();
        $hidden = array();
        $sortable = $this->get_sortable_columns();

        $this->_column_headers = array($columns, $hidden, $sortable);

        $this->get_bulk_actions();
        $this->process_bulk_action();
        $this->delete_item();
        $this->edit_item();
        //$this->search_items();
        $items = $wpdb->get_results($query);

        if ($items) {

            foreach($items as $item) {

                $city = $wpdb->get_row("SELECT city FROM ytew5_citys WHERE id='$item->city'", ARRAY_A);
                $city = $city['city'];

                $data[] = array(
                    'id'    => $item->id,
                    'name'  => $item->name,
                    'email' => $item->email,
                    'phone' => $item->phone,
                    'city'  => $city,
                    'state' => $item->state
                );
            }
        } else {
            $data = array();
        }

        function usort_reorder($a,$b) {
            $orderby = (!empty($_REQUEST['orderby'])) ? $_REQUEST['orderby'] : 'id';
            $order   = (!empty($_REQUEST['order'])) ? $_REQUEST['order'] : 'asc';
            $result  = strcmp($a[$orderby], $b[$orderby]);
            return ($order==='asc') ? $result : - $result;
        }

        usort($data, 'usort_reorder');

        $current_page = $this->get_pagenum();

        $total_items = count($data);

        $data = array_slice($data,(($current_page-1) * $per_page),$per_page);

        $this->items = $data;

        $this->set_pagination_args( array(
            'total_items' => $total_items,
            'per_page'    => $per_page,
            'total_pages' => ceil($total_items / $per_page)
        ));
    }
}

$list_items = new ResearchListTable();
$list_items->prepare_items();