<?php
/**
 * Created by WebStorm.
 * User: anderson
 * Date: 2/23/15
 * Time: 11:01 AM
 */

if (!defined('ABSPATH')) {
    header( 'Status: 403 Forbidden' );
    header( 'HTTP/1.1 403 Forbidden' );
    exit;
}

?>
<style>
    .research-user input[type="text"] {
        padding: 3px 8px;
        font-size: 1.7em;
        line-height: 100%;
        height: 1.7em;
        width: 100%;
        outline: 0px none;
        margin-bottom: 30px;
        background-color: #FFF;
    }
    .columns-2 {
        width: 50%;
    }

    .research-user label {
        display: block;
        font-size: 1.2em;
        font-weight: bold;
        padding-bottom: 5px;
    }

    #edit-user-research {
        margin-top: 20px;
    }

    .left {
        float: left;
        -webkit-box-sizing: border-box;
        -moz-box-sizing: border-box;
        box-sizing: border-box;
    }

    .right {
        float: right;
        -webkit-box-sizing: border-box;
        -moz-box-sizing: border-box;
        box-sizing: border-box;
    }

    .clearfix {
        display: block;
        clear: both
    }

    .col-1, .col-2 {
        width: 49%;
    }

    .rs-nasc .col-2 label {
        display: inline-block;
    }

    .rs-nasc .col-2 {
        margin-left: 15px;
        width: 48%
    }

    .rs-nasc input[type="radio"] {
        margin-right: 15px
    }

    .rs-phone {
        width: 30%;
    }

    .small {
        width: 30%;
    }

    .profile .col-2 {
        margin-left: 15px;
    }

    #rs-state, #rs-city {
        width: 100%;
        height: 40px;
        line-height: 40px;
        margin-bottom: 25px;
    }

    .rs-local .col-1 {
        width: 30%;
        margin-right: 15px;
    }

    .submit-update .message.success {
        color: #3C763D;
        display: inline-block;
        margin: 0 0 0 10px;
        background-color: #DFF0D8;
        height: 35px;
        line-height: 35px;
        border-color: #D6E9C6;
        font-weight: bold;
        padding: 0 10px;
    }

    .submit-update .message.error {
        color: #ca172e;
        display: inline-block;
        margin: 0 0 0 10px;
        padding: 0 10px;
        background-color: #f0e4df;
        height: 35px;
        line-height: 35px;
        border-color: #e96b72;
        font-weight: bold;
    }

    .submit-update .sending {
        margin-top: 5px;
        display: none;
    }
</style>
<script>
    jQuery(document).ready(function () {
        jQuery('#edit-user-research').on('submit', function (e) {

            var data = jQuery(this).serializeArray();

            jQuery('.message').remove();

            jQuery('.submit-update .sending').css({'display': 'inline-block'});

            e.preventDefault();
            jQuery.ajax({
                type: 'POST',
                url: ajaxurl,
                data: data,
                success: function (data) {

                    var response = jQuery.parseJSON(data);

                    jQuery('.submit-update .sending').css({'display': 'none'});

                    if (response.type == 'success') {
                        jQuery('.submit-update').append('<span class="message success">' + response.message + '</span>');

                        setTimeout(function() {
                            document.location.reload(true);
                        }, 2000);

                    } else {
                        jQuery('.submit-update').append('<span class="message error">' + response.message + '</span>');
                    }
                }
            });
        });
        jQuery('#rs-state').on('change', function () {
            var state_id = jQuery(this).find('option:selected').val();

            jQuery.ajax({
                type: 'post',
                url: ajaxurl,
                data: {
                    action: 'get-citys',
                    state_id: state_id
                },
                success: function (data) {

                    var citys = jQuery.parseJSON(data);
                    var citys_list = '';

                    for (var i in citys) {
                        citys_list += '<option value="' + citys[i].city_id + '">' + citys[i].city_name +'</opton>';
                    }

                    jQuery('#rs-city').html(citys_list);
                }
            });
        });
    });
</script>
<div class="wrap">
    <div class="columns-2 research-user">
        <h2>Pesquisa - Editar resposta do usuário</h2>
        <form method="post" id="edit-user-research">
            <div class="input-group">
                <label for="rs-name">Nome</label>
                <input type="text" id="rs-name" name="rs-name" class="form-control" value="<?php echo $data['name']; ?>">
            </div>
            <div class="input-group rs-nasc">
                <div class="col-1 left">
                    <label for="rs-nasc">Data de nascimento</label>
                    <input name="rs-nasc" id="rs-nasc" class="form-control" type="text" value="<?php echo $data['date_of_birth']; ?>">
                </div>
                <div class="col-2 left">
                    <label for="rs-sex">Sexo</label>
                    <br/>
                    <label for="rs-sex-m">Masculino</label>
                    <input id="rs-sex-m" name="rs-sex" type="radio" value="m" <?php echo $data['sex'] == 'm' ? 'checked': ''; ?>>
                    <label for="rs-sex-f">Feminino</label>
                    <input id="rs-sex-f" name="rs-sex" type="radio" value="f" <?php echo $data['sex'] == 'f' ? 'checked': ''; ?>>
                </div>
                <div class="clearfix"></div>
            </div>
            <div class="input-group rs-local">
                <div class="col-1 left">
                    <label for="rs-state">Estado</label>
                    <select name="rs-state" id="rs-state">
                        <?php foreach ($states as $state) : ?>
                            <option value="<?php echo $state['id']; ?>" <?php echo $data['state'] == $state['id'] ? 'selected': ''; ?>><?php echo $state['uf']; ?></option>
                        <?php endforeach; ?>
                    </select>
                </div>
                <div class="col-2 left">
                    <label for="rs-city">Cidade</label>
                    <select name="rs-city" id="rs-city">
                        <?php foreach ($citys as $city) : ?>
                            <option value="<?php echo $city->id; ?>" <?php echo $data['id'] == $city->id ? 'selected': ''; ?>><?php echo $city->city; ?></option>
                        <?php endforeach; ?>
                    </select>
                </div>
                <div class="clearfix"></div>
            </div>
            <div class="input-group">
                <label for="rs-email">Email</label>
                <input id="rs-email" name="rs-email" class="form-control" type="text" value="<?php echo $data['email']; ?>">
            </div>
            <div class="input-group rs-phone">
                <label for="rs-phone">Telefone</label>
                <input name="rs-phone" id="rs-phone" type="text" value="<?php echo $data['phone']; ?>">
            </div>
            <div class="input-group rs-occupation">
                <label for="rs-occupation">Profissão</label>
                <input name="rs-occupation" id="rs-occupation" type="text" class="form-control" value="<?php echo $data['occupation']; ?>">
            </div>
            <h2>Perfil socioeconômico</h2>
            <br/>
            <div class="input-group profile">
                <div class="col-1 left small">
                    <label for="rs-tv">Televisores em cores</label>
                    <input name="rs-tv" id="rs-tv" type="text" value="<?php echo $data['tvs']; ?>">
                    <label for="rs-radio">Rádios</label>
                    <input name="rs-radio" id="rs-radio" type="text" value="<?php echo $data['radios']; ?>">
                    <label for="rs-toilets">Banheiros</label>
                    <input name="rs-toilets" id="rs-toilets" type="text" value="<?php echo $data['bathrooms']; ?>">
                    <label for="rs-car">Automóveis</label>
                    <input name="rs-car" id="rs-car" type="text" value="<?php echo $data['cars']; ?>">
                    <label for="rs-employers">Empregados mensalistas</label>
                    <input name="rs-employers" id="rs-employers" type="text" value="<?php echo $data['employers']; ?>">
                </div>
                <div class="col-2 left small">
                    <label for="rs-washer">Máquinas de lavar</label>
                    <input name="rs-washer" id="rs-washer" type="text" value="<?php echo $data['washing_machines']; ?>">
                    <label for="rs-video-player">Video Cassetes ou DVDs</label>
                    <input name="rs-video-player" id="rs-video-player" type="text" value="<?php echo $data['dvds']; ?>">
                    <label for="rs-refrigerator">Geladeiras</label>
                    <input name="rs-refrigerator" id="rs-refrigerator" type="text" value="<?php echo $data['refrigerators']; ?>">
                    <label for="rs-freezer">Freezers</label>
                    <input name="rs-freezer" id="rs-freezer" type="text" value="<?php echo $data['freezers']; ?>">
                </div>
            </div>
            <div class="clearfix"></div>
            <div class="input-group">
                <label for="rs-education">Grau de instrução do chefe de família</label>
                <input name="rs-education" id="rs-education" type="text" value="<?php echo $data['education']; ?>">
            </div>
            <div class="input-group submit-update">
                <input id="rs-id" name="rs-id" value="<?php echo $data['id']; ?>" type="hidden">
                <input name="action" value="update-user-research" type="hidden">
                <input id="rs-submit" type="submit" value="Atualizar" class="button button-primary button-large">
                <span class="sending hide">
                    <img src="http://www.gauge.com.br/wp-includes/images/spinner.gif" alt="">
                </span>
            </div>
        </form>
    </div>
</div>