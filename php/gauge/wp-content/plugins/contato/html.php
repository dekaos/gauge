<?php
/**
 * Created by PhpStorm.
 * User: anderson
 * Date: 10/15/14
 * Time: 6:23 PM
 */

if (!defined('ABSPATH')) {
    header( 'Status: 403 Forbidden' );
    header( 'HTTP/1.1 403 Forbidden' );
    exit;
}

function admin_html($data) {

    $html =
       '<div class="wrap">
        <h2>Básico</h2>
        <form method="post" name="address-and-contacts">
            <table class="form-table">
                <tr>
                    <th>
                        <label for="address-and-contacts-title">Header</label>
                    </th>
                    <td>
                        <input id="address-and-contacts-title" name="title" class="regular-text" value="' . $data['title'] . '" type="text"/>
                    </td>
                </tr>
                <tr>
                    <th>
                        <label for="address-and-contacts-desc">Descrição</label>
                    </th>
                    <td>
                        <input id="address-and-contacts-desc" class="regular-text" name="description" value="' . $data['description'] . '" type="text"/>
                    </td>
                </tr>
                <tr>
                    <th>
                        <label for="address-and-contacts-phone">Telefone</label>
                    </th>
                    <td>
                        <input id="address-and-contacts-phone" name="phone" class="regular-text" value="' . $data['phone'] . '" type="text"/>
                    </td>
                </tr>
                <tr>
                    <th>
                        <label for="address-and-contacts-email">Email</label>
                    </th>
                    <td>
                        <input id="address-and-contacts-email" name="email" class="regular-text" value="' . $data['email'] . '" type="text"/>
                    </td>
                </tr>
                <tr>
                    <td colspan="2">
                        <hr/>
                    </td>
                </tr>
                <tr>
                    <th colspan="2">
                        <h3>Escritório 1</h3>
                    </th>
                </tr>
                <tr>
                    <th>
                        <label for="address-and-contacts-local">Local</label>
                    </th>
                    <td>
                        <input id="address-and-contacts-local" name="local-one" class="regular-text" value="' . $data['office_one_local'] . '" type="text"/>
                    </td>
                </tr>
                <tr>
                    <th>
                        <label for="address-and-contacts-address-one">Endereço</label>
                    </th>
                    <td>
                        <textarea id="address-and-contacts-address-one" name="address-one" rows="4" cols="39">' . $data['office_one_address'] . '</textarea>
                    </td>
                </tr>
                <tr>
                    <th>
                        <label for="address-and-contacts-city-one">Cidade</label>
                    </th>
                    <td>
                        <input id="address-and-contacts-city-one" name="city-one" class="regular-text" value="' . $data['office_one_city'] . '" type="text"/>
                    </td>
                </tr>
                <tr>
                    <th>
                        <label for="address-and-contacts-state-one">Estado</label>
                    </th>
                    <td>
                        <input id="address-and-contacts-state-one" name="state-one" class="regular-text" value="' . $data['office_one_state'] . '" type="text"/>
                    </td>
                </tr>
                <tr>
                    <th>
                        <label for="address-and-contacts-phone-one">Telefone</label>
                    </th>
                    <td>
                        <input id="address-and-contacts-phone-one" name="phone-one" class="regular-text" value="' . $data['office_one_phone'] . '" type="text"/>
                    </td>
                </tr>
                <tr>
                    <th>
                        <label for="address-and-contacts-email-one">Email</label>
                    </th>
                    <td>
                        <input id="address-and-contacts-email-one" name="email-one" class="regular-text" value="' . $data['office_one_email'] . '" type="text"/>
                    </td>
                </tr>
                <tr>
                    <td colspan="2">
                        <hr/>
                    </td>
                </tr>
                <tr>
                    <th colspan="2">
                        <h3>Escritório 2</h3>
                    </th>
                </tr>
                <tr>
                    <th>
                        <label for="address-and-contacts-local-two">Local</label>
                    </th>
                    <td>
                        <input id="address-and-contacts-local-two" name="local-two" class="regular-text" value="' . $data['office_two_local'] . '" type="text"/>
                    </td>
                </tr>
                <tr>
                    <th>
                        <label for="address-and-contacts-address-two">Endereço</label>
                    </th>
                    <td>
                        <textarea id="address-and-contacts-address-two" name="address-two" rows="4" cols="39">' . $data['office_two_address'] . '</textarea>
                    </td>
                </tr>
                <tr>
                    <th>
                        <label for="address-and-contacts-city-two">Cidade</label>
                    </th>
                    <td>
                        <input id="address-and-contacts-city-two" name="city-two" class="regular-text" value="' . $data['office_two_city'] . '" type="text"/>
                    </td>
                </tr>
                <tr>
                    <th>
                        <label for="address-and-contacts-state-two">Estado</label>
                    </th>
                    <td>
                        <input id="address-and-contacts-state-two" name="state-two" class="regular-text" value="' . $data['office_two_state'] . '" type="text"/>
                    </td>
                </tr>
                <tr>
                    <th>
                        <label for="address-and-contacts-phone-two">Telefone</label>
                    </th>
                    <td>
                        <input id="address-and-contacts-phone-two" name="phone-two" class="regular-text" value="' . $data['office_two_phone'] . '" type="text"/>
                    </td>
                </tr>
                <tr>
                    <th>
                        <label for="address-and-contacts-email-two">Email</label>
                    </th>
                    <td>
                        <input id="address-and-contacts-email-two" name="email-two" class="regular-text" value="' . $data['office_two_email'] . '" type="text"/>
                    </td>
                </tr>
                <tr>
                    <td colspan="2">
                        <hr/>
                    </td>
                </tr>
                <tr>
                    <th colspan="2">
                        <h3>Escritório 3</h3>
                    </th>
                </tr>
                <tr>
                    <th>
                        <label for="address-and-contacts-local-tree">Local</label>
                    </th>
                    <td>
                        <input id="address-and-contacts-local-tree" name="local-tree" class="regular-text" value="' . $data['office_tree_local'] . '" type="text"/>
                    </td>
                </tr>
                <tr>
                    <th>
                        <label for="address-and-contacts-address-tree">Endereço</label>
                    </th>
                    <td>
                        <textarea id="address-and-contacts-address-tree" name="address-tree" rows="4" cols="39">' . $data['office_tree_address'] . '</textarea>
                    </td>
                </tr>
                <tr>
                    <th>
                        <label for="address-and-contacts-city-tree">Cidade</label>
                    </th>
                    <td>
                        <input id="address-and-contacts-city-tree" name="city-tree" class="regular-text" value="' . $data['office_tree_city'] . '" type="text"/>
                    </td>
                </tr>
                <tr>
                    <th>
                        <label for="address-and-contacts-state-tree">Estado</label>
                    </th>
                    <td>
                        <input id="address-and-contacts-state-tree" name="state-tree" class="regular-text" value="' . $data['office_tree_state'] . '" type="text"/>
                    </td>
                </tr>
                <tr>
                    <th>
                        <label for="address-and-contacts-phone-tree">Telefone</label>
                    </th>
                    <td>
                        <input id="address-and-contacts-phone-tree" name="phone-tree" class="regular-text" value="' . $data['office_tree_phone'] . '" type="text"/>
                    </td>
                </tr>
                <tr>
                    <th>
                        <label for="address-and-contacts-email-tree">Email</label>
                    </th>
                    <td>
                        <input id="address-and-contacts-email-tree" name="email-tree" class="regular-text" value="' . $data['office_tree_email'] . '" type="text"/>
                    </td>
                </tr>
                <tr>
                    <td colspan="2">
                        <input name="update" value="Atualizar" class="button button-primary button-large" type="submit"/>
                    </td>
                </tr>
            </table>
        </form>
    </div>';

    echo $html;
}

function front_html ($data) {

    $title = __('Our address', 'roots');

    if (!is_page(138)) {
        $header =
        '<div class="featured col-md-11">
            <h2>'. $data['title'] . '</h2>
            <span>' . $data['description'] . '</span>
         </div>
         <div class="info col-md-7">
            <div class="email-info col-md-6">
                <a id="rodape_fale_com_a_gente" class="btn btn-info" href="' . get_permalink(138) . '"><span></span>' . __('Talk with us', 'roots') . '</a>
            </div>
            <div class="phone-info col-md-6">
            <p><span></span>' . $data['phone'] . '</p>
         </div></div>';
    } else {
        $header =
        '<div class="address-title col-md-11">
            <h2>'. $title . '</h2>
        </div>';
    }

    $html = $header .
     '<div class="address col-md-12">
         <div class="col-sm-4" id="address-one">
            <div class="address-outer">
                <div class="address-inner">
                    <div class="office">
                        <h2 class="office-title">' . $data['office_one_local'] . '<span></span></h2>
                        <p class="office-address">' . $data['office_one_address'] . '</p>
                        <p class="office-local">' . $data['office_one_city'] . ' - ' . $data['office_one_state'] . '</p>
                    </div>
                    <a id="rodape_tracar_rota_sp" class="router-trace btn btn-info" target="_blank" href="https://www.google.com/maps/dir//' . $data['office_one_address'] . ',' . $data['office_one_city'] . ',' . $data['office_one_state'] . '">' .
                        __('See location', 'roots') .
                    '</a>
                </div>
            </div>
         </div>
         <div class="col-sm-4" id="address-two">
            <div class="address-outer">
                <div class="address-inner">
                    <div class="office">
                        <h2 class="office-title">' . $data['office_two_local'] . '<span></span></h2>
                        <p class="office-address">' . $data['office_two_address'] . '</p>
                        <p class="office-local">' . $data['office_two_city'] . ' - ' . $data['office_two_state'] . '</p>
                    </div>
                    <a id="rodape_tracar_rota_rj" class="router-trace btn btn-info" target="_blank" href="https://www.google.com/maps/dir//' . $data['office_tow_address'] . ',' . $data['office_two_city'] . ',' . $data['office_two_state'] . '">' .
                        __('See location', 'roots') .
                    '</a>
                </div>
            </div>
         </div>
         <div class="col-sm-4" id="address-tree">
            <div class="address-outer">
                <div class="address-inner">
                    <div class="office">
                        <h2 class="office-title">' . $data['office_tree_local'] . '<span></span></h2>
                        <p class="office-address">' . $data['office_tree_address'] . '</p>
                        <p class="office-local">' . $data['office_tree_city'] . ' - ' . $data['office_tree_state'] . '</p>
                    </div>
                    <a id="rodape_tracar_rota_scj" class="router-trace btn btn-info" target="_blank" href="https://www.google.com/maps/dir//' . $data['office_tree_address'] . ',' . $data['office_tree_city'] . ',' . $data['office_tree_state'] . '">' .
                        __('See location', 'roots') .
                    '</a>
                </div>
            </div>
         </div>
     </div>';

    echo $html;
}

function office_phone_and_email ($data) {
    echo '<div class="office-info" itemscope itemtype="http://schema.org/LocalBusiness">';
    echo '<h3><span class="phone-icon"></span>' . __('Our phone', 'roots') . '<a itemprop="telephone" href="tel:' . $data['phone'] . '"><span class="phone-txt">' . $data['phone'] . '</span></a></h3>';
    echo '<h3><span class="email-icon"></span>' . __('Our e-mail', 'roots') . '<a itemprop="email" href="mailto:'. $data['email'] . '"><span class="email-txt">' . $data['email'] . '</span></a></h3>';
    echo '</div>';
}