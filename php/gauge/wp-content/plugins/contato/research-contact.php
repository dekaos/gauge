<?php
/**
 * Created by PhpStorm.
 * User: anderson
 * Date: 11/24/14
 * Time: 6:20 AM
 */

add_action('wp_ajax_nopriv_research-contact', 'research_contact');
add_action('wp_ajax_research-contact', 'research_contact');

add_action('wp_ajax_nopriv_get-citys', 'research_contact_citys');
add_action('wp_ajax_get-citys', 'research_contact_citys');

require_once('class.php');

function research_contact () {
    $name          = sanitize_text_field($_POST['re-name']);
    $birth         = sanitize_text_field($_POST['re-birth']);
    $sex           = sanitize_text_field($_POST['re-sex']);
    $state         = sanitize_text_field($_POST['re-state']);
    $city          = sanitize_text_field($_POST['re-city']);
    $email         = sanitize_text_field($_POST['re-email']);
    $phone         = sanitize_text_field($_POST['re-phone']);
    $ocupation     = sanitize_text_field($_POST['re-occupation']);
    $tvs           = sanitize_text_field($_POST['re-tv']);
    $radios        = sanitize_text_field($_POST['re-radio']);
    $bathrooms     = sanitize_text_field($_POST['re-bathroom']);
    $cars          = sanitize_text_field($_POST['re-car']);
    $employers     = sanitize_text_field($_POST['re-employer']);
    $washings      = sanitize_text_field($_POST['re-washing-machine']);
    $videos        = sanitize_text_field($_POST['re-videos']);
    $refrigerators = sanitize_text_field($_POST['re-refrigerator']);
    $freezers      = sanitize_text_field($_POST['re-freezer']);
    $education     = sanitize_text_field($_POST['re-education']);

    /*$city_name = new DbActions();
    $city      = $city_name->get_single_city($city_id);*/

    if ($email && !is_email($email)) {
        echo '[' . json_encode(array('message' => __('Invalid e-mail', 'roots'), 'type_of' => 'error', 'field_id' => 're-email')) . ']';
        exit;
    }

    $data = array(
        'name'          => $name,
        'birth'         => $birth,
        'sex'           => $sex,
        'state'         => $state,
        'city'          => $city,
        'email'         => $email,
        'phone'         => $phone,
        'occupation'    => $ocupation,
        'tvs'           => $tvs,
        'radios'        => $radios,
        'bathrooms'     => $bathrooms,
        'cars'          => $cars,
        'employers'     => $employers,
        'washings'      => $washings,
        'videos'        => $videos,
        'refrigerators' => $refrigerators,
        'freezers'      => $freezers,
        'education'     => $education
    );
    foreach ($data as $key => $value) {
        if ($value == '') {
            echo '[' . json_encode(array('message' => __('This field is mandatory', 'roots'), 'type_of' => 'error', 'field_id' => $key)) . ']';
            exit;
        }
    }

    $data['created_at'] = date("Y-m-d H:i:s");

    return save_to_db($data);
}

function save_to_db($data) {
    $new_research = new DbActions();

    $new_research->insert_new_research($data);
}

function research_contact_citys() {
    $action_type = sanitize_text_field($_POST['action_type']);

    if ($action_type == 'search_by_string') {
        $by_string = trim(sanitize_text_field($_POST['string']));
    } else {
        $by_string = '';
    }

    $citys    = new DbActions();
    $state_id = intval($_POST['state_id']);
    $results  = $citys->get_citys($state_id, $by_string);
    $last     = count($results);
    $counter  = 1;

    if ($results) {
        echo '[';
        foreach ($results as $key => $result) {

            if ($counter != $last) {
                $coma = ',';
            } else {
                $coma = '';
            }

            echo json_encode(array('city_id' => $result->id, 'city_name' => $result->city)) . $coma;

            $counter++;
        }
        echo ']';
    }

    exit;
}