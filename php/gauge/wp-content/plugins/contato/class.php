<?php
/**
 * Created by PhpStorm.
 * User: anderson
 * Date: 10/15/14
 * Time: 5:08 PM
 */
if (!defined('ABSPATH')) {
    header( 'Status: 403 Forbidden' );
    header( 'HTTP/1.1 403 Forbidden' );
    exit;
}
class DbActions {

    public function selectId() {
        global $wpdb;

        return $wpdb->get_row("SELECT id FROM ytew5_offices");
    }

    public function select() {
        global $wpdb;

        $query = $wpdb->get_row("SELECT * FROM ytew5_offices");

        $title       = $query->title;
        $description = $query->description;
        $phone       = $query->phone;
        $email       = $query->email;
        $offices     = $query->offices;
        $offices     = json_decode($offices);

        $office_one_address  = $offices[0]->office_one->address;
        $office_two_address  = $offices[0]->office_two->address;
        $office_tree_address = $offices[0]->office_tree->address;
        $office_one_phone    = $offices[0]->office_one->phone;
        $office_two_phone    = $offices[0]->office_two->phone;
        $office_tree_phone   = $offices[0]->office_tree->phone;
        $office_one_email    = $offices[0]->office_one->email;
        $office_two_email    = $offices[0]->office_two->email;
        $office_tree_email   = $offices[0]->office_tree->email;
        $office_one_local    = $offices[0]->office_one->local;
        $office_two_local    = $offices[0]->office_two->local;
        $office_tree_local   = $offices[0]->office_tree->local;
        $office_one_city     = $offices[0]->office_one->city;
        $office_one_state    = $offices[0]->office_one->state;
        $office_two_city     = $offices[0]->office_two->city;
        $office_two_state    = $offices[0]->office_two->state;
        $office_tree_state   = $offices[0]->office_tree->state;
        $office_tree_city    = $offices[0]->office_tree->city;

        $data = array(
            'title'       => $title,
            'description' => $description,
            'phone'       => $phone,
            'email'       => $email,
            'office_one_address'  => $office_one_address,
            'office_one_city'     => $office_one_city,
            'office_one_state'    => $office_one_state,
            'office_two_address'  => $office_two_address,
            'office_two_city'     => $office_two_city,
            'office_two_state'    => $office_two_state,
            'office_tree_address' => $office_tree_address,
            'office_tree_city'    => $office_tree_city,
            'office_tree_state'   => $office_tree_state,
            'office_one_phone'    => $office_one_phone,
            'office_one_email'    => $office_one_email,
            'office_two_phone'    => $office_two_phone,
            'office_two_email'    => $office_two_email,
            'office_tree_phone'   => $office_tree_phone,
            'office_tree_email'   => $office_tree_email,
            'office_one_local'    => $office_one_local,
            'office_two_local'    => $office_two_local,
            'office_tree_local'   => $office_tree_local
        );

        return $data;
    }

    public function update($title, $description, $phone, $email, $offices, $id) {
        global $wpdb;

        return $wpdb->update(ytew5_offices, array(
                'title'       => $title,
                'description' => $description,
                'phone'       => $phone,
                'email'       => $email,
                'offices'     => $offices
            ),
            array('id' => $id),
            array('%s', '%s','%s', '%s', '%s'),
            array('%d')
        );
    }

    public function select_phone_and_email () {
        global $wpdb;

        $query = $wpdb->get_row("SELECT phone, email FROM ytew5_offices", ARRAY_A);

        return $query;
    }

    public function get_citys($state_id, $by_string) {
        global $wpdb;

        if (!empty($by_strings)) {
            $query = $wpdb->get_results("SELECT * FROM ytew5_citys WHERE state_id='$state_id'");
        } else {
            $query = $wpdb->get_results("SELECT * FROM ytew5_citys WHERE state_id='$state_id' AND city LIKE '%" . $by_string . "%'");
        }

        return $query;
    }

    public function insert_new_research ($data) {
        global $wpdb;

        $prepare = $wpdb->prepare(
            "
                INSERT INTO ytew5_research_contacts
                (
                  name, date_of_birth, sex, state,
                  city, email, phone, occupation,
                  tvs, radios, bathrooms, cars,
                  employers, washing_machines,
                  dvds, refrigerators, freezers,
                  education, created_at
                ) VALUES (
                  %s, %s, %s, %s, %s, %s, %s, %s, %s, %s, %s, %s, %s, %s, %s, %s, %s, %s, %s
                )
            ",
            $data['name'],
            $data['birth'],
            $data['sex'],
            $data['state'],
            $data['city'],
            $data['email'],
            $data['phone'],
            $data['occupation'],
            $data['tvs'],
            $data['radios'],
            $data['bathrooms'],
            $data['cars'],
            $data['employers'],
            $data['washings'],
            $data['videos'],
            $data['refrigerators'],
            $data['freezers'],
            $data['education'],
            $data['created_at']
        );

        $query = $wpdb->query($prepare);

        if ($query) {
            echo '[' . json_encode
            (
                array(
                    'message' => __('Your subscription was successfully completed', 'roots'),
                    'tip'     => __('Wait to be recruited for a our research.', 'roots'),
                    'close'   => __('Close', 'roots'),
                    'type_of' => 'success'
                )
            ) . ']';

        } else {
            echo '[' . json_encode
            (
                array(
                    'message' => __('Data error insert. Please try again.'),
                    'type_of' => 'process_error'
                )
            ) . ']';
        }
        exit;
    }

    public function select_user_research ($user_id) {
        global $wpdb;

        $query = $wpdb->get_row("SELECT * FROM ytew5_research_contacts WHERE id='$user_id'", ARRAY_A);

        return $query;
    }

    public function select_states() {
        global $wpdb;

        $query = $wpdb->get_results("SELECT * FROM ytew5_states", ARRAY_A);

        return $query;
    }

    public function update_user_research_data($data) {
        global $wpdb;

        $query = $wpdb->update('ytew5_research_contacts', array(
            'name'             => $data['name'],
            'date_of_birth'    => $data['date_of_birth'],
            'sex'              => $data['sex'],
            'state'            => $data['state'],
            'city'             => $data['city'],
            'email'            => $data['email'],
            'phone'            => $data['phone'],
            'occupation'       => $data['occupation'],
            'tvs'              => $data['tvs'],
            'radios'           => $data['radios'],
            'bathrooms'        => $data['bathrooms'],
            'cars'             => $data['cars'],
            'employers'        => $data['employers'],
            'washing_machines' => $data['washing_machines'],
            'dvds'             => $data['dvds'],
            'refrigerators'    => $data['refrigerators'],
            'freezers'         => $data['freezers'],
            'education'        => $data['education']
        ),
            array('id' => $data['id']),
            array('%s', '%s', '%s', '%s', '%s', '%s', '%s', '%s', '%s', '%s', '%s', '%s', '%s', '%s', '%s', '%s', '%s', '%s'),
            array('%d')
        );

        if ($query || $query === 0) {
            $message = json_encode(array('message' => 'Usuário atualizado com sucesso.', 'type' => 'success'));
        } else {
            $message = json_encode(array('message' => 'Erro ao atualizar usuário.', 'type' => 'error'));
        }

        echo $message;
    }
}