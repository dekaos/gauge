<?php
/**
 * Created by PhpStorm.
 * User: anderson
 * Date: 10/14/14
 * Time: 7:32 PM
 * Plugin name: Escritórios
 * Description: Escritórios Gauge
 */

if (!defined('ABSPATH')) {
    header( 'Status: 403 Forbidden' );
    header( 'HTTP/1.1 403 Forbidden' );
    exit;
}

require_once('class.php');
require_once('html.php');

add_action( 'admin_menu', 'register_gauge_contacts' );

function register_gauge_contacts() {
    add_menu_page('contatos', 'Endereços', 'manage_options', 'officers', 'gauge_contacts', 'dashicons-admin-home', 73);
    add_menu_page('researchs', 'Pesquisas', 'manage_options', 'research', 'research_admin', 'dashicons-search', 75);
}

function gauge_contacts() {

    if ( !current_user_can( 'manage_options' ) )  {
        wp_die( __( 'You do not have sufficient permissions to access this page.' ) );
    }

    return gauge_inputs();
}

add_action('wp_ajax_nopriv_update-user-research', 'update_user_research');
add_action('wp_ajax_update-user-research', 'update_user_research');

function research_admin() {
    if ($_GET['action'] == 'edit') {

        $user_id = intval($_GET['id']);
        $select  = new DbActions;
        $data    = $select->select_user_research($user_id);
        $states  = $select->select_states();
        $citys   = $select->get_citys($data['state'], null);
        require_once('research/edit.php');
    } else {
        require_once('research-admin.php');
    }
}

function update_user_research () {
    $rs_id           = sanitize_text_field($_POST['rs-id']);
    $rs_name         = sanitize_text_field($_POST['rs-name']);
    $rs_nasc         = sanitize_text_field($_POST['rs-nasc']);
    $rs_sex          = sanitize_text_field($_POST['rs-sex']);
    $rs_state        = sanitize_text_field($_POST['rs-state']);
    $rs_city         = sanitize_text_field($_POST['rs-city']);
    $rs_email        = sanitize_text_field($_POST['rs-email']);
    $rs_phone        = sanitize_text_field($_POST['rs-phone']);
    $rs_occupation   = sanitize_text_field($_POST['rs-occupation']);
    $rs_tv           = sanitize_text_field($_POST['rs-tv']);
    $rs_radio        = sanitize_text_field($_POST['rs-radio']);
    $rs_toilets      = sanitize_text_field($_POST['rs-toilets']);
    $rs_car          = sanitize_text_field($_POST['rs-car']);
    $rs_employers    = sanitize_text_field($_POST['rs-employers']);
    $rs_washer       = sanitize_text_field($_POST['rs-washer']);
    $rs_video_player = sanitize_text_field($_POST['rs-video-player']);
    $rs_refrigerator = sanitize_text_field($_POST['rs-refrigerator']);
    $rs_freezer      = sanitize_text_field($_POST['rs-freezer']);
    $rs_education    = sanitize_text_field($_POST['rs-education']);

    $data = array(
        'id'               => $rs_id,
        'name'             => $rs_name,
        'date_of_birth'    => $rs_nasc,
        'sex'              => $rs_sex,
        'state'            => $rs_state,
        'city'             => $rs_city,
        'email'            => $rs_email,
        'phone'            => $rs_phone,
        'occupation'       => $rs_occupation,
        'tvs'              => $rs_tv,
        'radios'           => $rs_radio,
        'bathrooms'        => $rs_toilets,
        'cars'             => $rs_car,
        'employers'        => $rs_employers,
        'washing_machines' => $rs_washer,
        'dvds'             => $rs_video_player,
        'refrigerators'    => $rs_refrigerator,
        'freezers'         => $rs_freezer,
        'education'        => $rs_education
    );

    $updata_user_data = new DbActions();
    $updata_user_data->update_user_research_data($data);

    exit;
}

function gauge_inputs() {

    $action = new DbActions;

    if ($_POST['update'] == 'Atualizar') {

        $query = $action->selectId();

        $id           = $query->id;
        $title        = sanitize_text_field($_POST['title']);
        $description  = sanitize_text_field($_POST['description']);
        $phone        = sanitize_text_field($_POST['phone']);
        $email        = sanitize_text_field($_POST['email']);
        $local_one    = sanitize_text_field($_POST['local-one']);
        $address_one  = sanitize_text_field($_POST['address-one']);
        $city_one     = sanitize_text_field($_POST['city-one']);
        $state_one    = sanitize_text_field($_POST['state-one']);
        $phone_one    = sanitize_text_field($_POST['phone-one']);
        $email_one    = sanitize_email($_POST['email-one']);
        $local_two    = sanitize_text_field($_POST['local-two']);
        $address_two  = sanitize_text_field($_POST['address-two']);
        $city_two     = sanitize_text_field($_POST['city-two']);
        $state_two    = sanitize_text_field($_POST['state-two']);
        $phone_two    = sanitize_text_field($_POST['phone-two']);
        $email_two    = sanitize_email($_POST['email-two']);
        $local_tree   = sanitize_text_field($_POST['local-tree']);
        $address_tree = sanitize_text_field($_POST['address-tree']);
        $city_tree    = sanitize_text_field($_POST['city-tree']);
        $state_tree   = sanitize_text_field($_POST['state-tree']);
        $phone_tree   = sanitize_text_field($_POST['phone-tree']);
        $email_tree   = sanitize_email($_POST['email-tree']);

        $offices_inputs = array(
            'office_one'  => array(
                'local'   => $local_one,
                'address' => $address_one,
                'city'    => $city_one,
                'state'   => $state_one,
                'phone'   => $phone_one,
                'email'   => $email_one
            ),
            'office_two'  => array(
                'local'   => $local_two,
                'address' => $address_two,
                'city'    => $city_two,
                'state'   => $state_two,
                'phone'   => $phone_two,
                'email'   => $email_two
            ),
            'office_tree' => array(
                'local'   => $local_tree,
                'address' => $address_tree,
                'city'    => $city_tree,
                'state'   => $state_tree,
                'phone'   => $phone_tree,
                'email'   => $email_tree
            )
        );

        $offices = '[' . json_encode($offices_inputs) . ']';
        $action->update($title, $description, $phone, $email, $offices, $id);
    }

    $data = $action->select();

    return admin_html($data);
}

function offices_front_end () {

    $action = new DbActions();

    $data = $action->select();

    return front_html($data);
}

function get_office_phone_and_email () {

    $action = new DbActions();

    $data = $action->select_phone_and_email();

    return office_phone_and_email($data);
}


// contact form 'Commercial'
require_once('commercial-contact.php');

// contact form 'Research'
require_once('research-contact.php');

// contact form 'Others'
require_once('others-contact.php');