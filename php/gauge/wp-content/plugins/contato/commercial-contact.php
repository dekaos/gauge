<?php
/**
 * Created by PhpStorm.
 * User: anderson
 * Date: 11/21/14
 * Time: 9:22 AM
 */

if (!defined('ABSPATH')) {
    header( 'Status: 403 Forbidden' );
    header( 'HTTP/1.1 403 Forbidden' );
    exit;
}

add_action( 'wp_ajax_nopriv_commercial-contact', 'commercial_contact' );
add_action( 'wp_ajax_commercial-contact', 'commercial_contact' );

function commercial_contact() {
    $name    = sanitize_text_field($_POST['cm-form-name']);
    $email   = $_POST['cm-form-email'];
    $phone   = sanitize_text_field($_POST['cm-form-phone']);
    $message = sanitize_text_field($_POST['cm-form-message']);
    $error = '';

    $error_message = __('This field is mandatory', 'roots');

    if (!$name)
        $error = json_encode(array('message' => $error_message, 'type_of' => 'error', 'field_id' => 'cm-form-name')) . ',';

    if (!$email) {
        $error .= json_encode(array('message' => $error_message, 'type_of' => 'error', 'field_id' => 'cm-form-email')) . ',';
    } else if ($email && !is_email(($email))) {
        $error .= json_encode(array('message' => __('Invalid e-mail', 'roots'), 'type_of' => 'error', 'field_id' => 'cm-form-email')) . ',';
    }

    if (!$phone)
        $error .= json_encode(array('message' => $error_message, 'type_of' => 'error', 'field_id' => 'cm-form-phone')) . ',';

    if (!$message)
        $error .= json_encode(array('message' => $error_message, 'type_of' => 'error', 'field_id' => 'cm-form-message'));

    if ($error) {
        echo '[' . rtrim($error, ',') . ']';
        exit;
    } else {
        $data = array(
            'name'       => substr($name,    0, 70),
            'email'      => substr($email,   0, 70),
            'phone'      => substr($phone,   0, 70),
            'message'    => substr($message, 0, 5000),
            'created_at' => date("Y-m-d H:i:s")
        );

        return save_contact_data($data);
    }
}

function save_contact_data($data) {
    global $wpdb;

    $query = $wpdb->insert('ytew5_commercial_contacts', array(
        'name'       => $data['name'],
        'email'      => $data['email'],
        'phone'      => $data['phone'],
        'message'    => $data['message'],
        'created_at' => $data['created_at']
    ), array(
        '%s',
        '%s',
        '%s',
        '%s',
        '%s',
    ));

    if ($query) {

        // commercial email
        $type = '1';
        return send_to_email ($data, $type);
    }
    else {
        echo '[' . json_encode(array('message' => 'Error on data insert', 'type_of' => 'error')) . ']';
    }
}

function send_to_email ($data, $type) {
    //$to = 'caos19@gmail.com';
    $to = 'contato@gauge.com.br';
    $from = 'site@gauge.com.br';

    switch($type) {
        case 1:
            $subject = 'Contato comercial do site Gauge';
            break;
        default:
            $subject = 'Contato do site Gauge';
    }

    $headers = "Replay-To: " . $data['email'] . "\r\n";
    $headers .= "From: " . $from . "\r\n";
    $headers .= "MIME-Version: 1.0\r\n";
    $headers .= "Content-Type: text/html; charset: UTF-8\r\n";

    $message = '<html><body><meta charset="utf-8">';
    $message .= '<h2>' . $subject   . '</h2>';
    $message .= '<b>Nome: </b>'     . $data['name']    . '<br>';
    $message .= '<b>Email: </b>'    . $data['email']   . '<br>';
    $message .= '<b>Telefone: </b>' . $data['phone']   . '<br>';
    $message .= '<b>Mensagem: </b>' . $data['message'] . '<br>';
    $message .= '</body></html>';

    $send = wp_mail($to, $subject, $message, $headers);

    if ($send) {
        echo '[' . json_encode(array(
                'message' => __('Your message has been sent sucess', 'roots'),
                'tip'     => __('Will soon return your contact', 'roots'),
                'close'   => __('Close', 'roots'),
                'type_of' => 'success')) . ']';
    } else {
        echo '[' . json_encode(array('message' => __('Sending e-mail error. Please try again', 'roots'), 'type_of' => 'process_error')) . ']';
    }
    exit;
}