<?php
/**
 * Created by PhpStorm.
 * User: anderson
 * Date: 11/25/14
 * Time: 6:40 PM
 */

if (!defined('ABSPATH')) {
    header( 'Status: 403 Forbidden' );
    header( 'HTTP/1.1 403 Forbidden' );
    exit;
}

require_once('list-table.php');

?>
<div class="wrap">
    <h2>Pesquisas</h2>
    <form method="get">
        <input type="hidden" name="page" value="research" />
        <?php $list_items->search_box('search', 'search_id'); ?>
    </form>
    <form id="cases-filter" method="get">
        <?php $list_items->display() ?>
        <input type="hidden" name="page" value="<?php echo $_REQUEST['page'] ?>" />
    </form>
</div>
<script>
    jQuery('.delete-row').click(function(){
        return confirm("Tem certeza que deseja deletar este item?");
    });
</script>