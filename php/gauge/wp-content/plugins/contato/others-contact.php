<?php
/**
 * Created by PhpStorm.
 * User: anderson
 * Date: 11/25/14
 * Time: 11:28 PM
 */

if (!defined('ABSPATH')) {
    header( 'Status: 403 Forbidden' );
    header( 'HTTP/1.1 403 Forbidden' );
    exit;
}

add_action('wp_ajax_nopriv_others-contacts', 'others_contacts');
add_action('wp_ajax_others-contacts', 'others_contacts');

function others_contacts () {
    $ot_name    = sanitize_text_field($_POST['ot-name']);
    $ot_email   = sanitize_text_field($_POST['ot-email']);
    $ot_subject = sanitize_text_field($_POST['ot-subject']);
    $ot_message = sanitize_text_field($_POST['ot-message']);

    $error = '';

    if (!$ot_name) {
        $error = json_encode(array('message' => __('This field is mandatory', 'roots'), 'type_of' => 'error', 'field_id' => 'ot-name')) . ',';
    }

    if ($ot_email && !is_email($ot_email)) {
        $error .= json_encode(array('message' => __('Invalid e-mail', 'roots'), 'type_of' => 'error', 'field_id' => 'ot-email')) . ',';
    } else if (!$ot_email) {
        $error .= json_encode(array('message' => __('This field is mandatory', 'roots'), 'type_of' => 'error', 'field_id' => 'ot-email')) . ',';
    }

    if (!$ot_subject) {
        $error .= json_encode(array('message' => __('This field is mandatory', 'roots'), 'type_of' => 'error', 'field_id' => 'ot-subject')) . ',';
    }

    if (!$ot_message) {
        $error .= json_encode(array('message' => __('This field is mandatory', 'roots'), 'type_of' => 'error', 'field_id' => 'ot-message')) . ',';
    }

    if ($error) {
        echo '['. rtrim($error, ',') . ']';
        exit;
    } else {
        return others_contacts_save ($ot_name, $ot_email, $ot_subject, $ot_message);
    }
}

function others_contacts_save ($ot_name, $ot_email, $ot_subject, $ot_message) {
    global $wpdb;

    $ot_created_at = date("Y-m-d H:i:s");

    $query = $wpdb->prepare(
        "
            INSERT INTO ytew5_others_contacts
            (
              ot_name, ot_email, ot_subject, ot_message, ot_created_at
            ) VALUES (
              %s, %s, %s, %s, %s
            )
        ",
        $ot_name,
        $ot_email,
        $ot_subject,
        $ot_message,
        $ot_created_at
    );

    $wpdb->query($query);

    return send_ot_contact($ot_name, $ot_email, $ot_subject, $ot_message);
}

function send_ot_contact($ot_name, $ot_email, $ot_subject, $ot_message) {

    $to   = 'contato@gauge.com.br';
    //$to = 'caos19@gmail.com';
    $from = 'site@gauge.com.br';

    $ot_subject = 'Contato - Outros';

    $headers = "Replay-To: " . $ot_email . "\r\n";
    $headers .= "From: " . $from . "\r\n";
    $headers .= "MIME-Version: 1.0\r\n";
    $headers .= "Content-Type: text/html; charset: UTF-8\r\n";

    $message = '<html><body><meta charset="utf-8">';
    $message .= '<h2>' . $ot_subject. '</h2>';
    $message .= '<b>Nome: </b>'     . $ot_name    . '<br>';
    $message .= '<b>Email: </b>'    . $ot_email   . '<br>';
    $message .= '<b>Assunto: </b>'  . $ot_subject . '<br>';
    $message .= '<b>Mensagem: </b>' . $ot_message . '<br>';
    $message .= '</body></html>';

    $send = wp_mail($to, $ot_subject, $message, $headers);

    if ($send) {
        echo '[' . json_encode(array(
                'message' => __('Your message has been sent sucess', 'roots'),
                'tip'     => __('Will soon return your contact', 'roots'),
                'close'   => __('Close', 'roots'),
                'type_of' => 'success')) . ']';
    } else {
        echo '[' . json_encode(array('message' => __('Sending e-mail error. Please try again', 'roots'), 'type_of' => 'process_error')) . ']';
    }

    exit;
}