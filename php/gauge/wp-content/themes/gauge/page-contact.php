<?php
/**
 * Created by PhpStorm.
 * User: anderson
 * Template Name: Contato
 * Date: 11/20/14
 * Time: 7:33 AM
 */
?>

<div id="contact-form-header-wrapper" class="yellow-bg">
    <header class="container" id="contact-form-header">
        <h1>
            <?php _e('Where can we help?', 'roots'); ?>
        </h1>
    </header>
</div>
<div id="tablist-wrapper">
    <div role="tabpanel">
        <div class="tablist-control">
            <div class="container">
                <ul class="nav nav-tabs" role="tablist">
                    <li role="presentation" class="active">
                        <a href="#others" aria-controls="others" role="tab" data-toggle="tab">
                            <span class="tablist-icon others"></span>
                            <span class="tablist-txt">
                                <?php _e('Contact us', 'roots'); ?>
                            </span>
                        </a>
                    </li>
                    <li role="presentation">
                        <a href="#commercial" aria-controls="commercial" role="tab" data-toggle="tab">
                            <span class="tablist-icon commercial"></span>
                            <span class="tablist-txt">
                                <?php _e('Get a free quote', 'roots'); ?>
                            </span>
                        </a>
                    </li>
                    <li role="presentation">
                        <a href="#research" aria-controls="research" role="tab" data-toggle="tab">
                            <span class="tablist-icon research"></span>
                            <span class="tablist-txt">
                                <?php _e('Join research', 'roots'); ?>
                            </span>
                        </a>
                    </li>
                    <li role="presentation">
                        <a href="#work-with-us" aria-controls="work-with-us" role="tab" data-toggle="tab">
                            <span class="tablist-icon work-with-us"></span>
                            <span class="tablist-txt">
                                <?php _e('Work with us', 'roots'); ?>
                            </span>
                        </a>
                    </li>
                </ul>
            </div>
        </div>
        <div class="tab-content">
            <div role="tabpanel" class="container tab-pane fade in active" id="commercial">
                <?php require_once('templates/partials/_commercial-contact.php'); ?>
            </div>
            <div role="tabpanel" class="container tab-pane fade" id="research">
                <?php require_once('templates/partials/_research-contact.php'); ?>
            </div>
            <div role="tabpanel" class="container tab-pane fade" id="work-with-us">
                <div id="work-with-us-inner">
                    <?php require_once('templates/partials/_work-with-us.php'); ?>
                </div>
            </div>
            <div role="tabpanel" class="container tab-pane fade" id="others">
                <?php require_once('templates/partials/_others.php'); ?>
            </div>
        </div>
    </div>
</div>