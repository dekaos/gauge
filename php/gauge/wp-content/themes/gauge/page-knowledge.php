<?php
/**
 * Created by WebStorm.
 * User: anderson
 * Date: 11/28/14
 * Time: 9:21 AM
 * Template Name: Conhecimento
 */

$args = array(
    'type'       => 'post',
    'child_of'   => 17,
    'orderby'    => 'name',
    'order'      => 'ASC',
    'hide_empty' => 0,
    'taxonomy'   => 'category'
);

$categories = get_categories($args);
$cat_id     = get_query_var('cat');

?>
<div id="know-wrapper">
    <div id="know-header-wrapper" class="yellow-bg">
        <header class="know container">
            <div class="col-sm-8">
                <h1 class="category-title dynamic">
                <span>
                    <?php
                        if ($cat_id) {
                           $cat_name = get_category($cat_id)->name;
                            echo $cat_name;
                        } else {
                            _e('Knowledge', 'roots');
                        }
                    ?>
                </span>
                </h1>
            </div>
            <div class="col-sm-4 form-one">
                <form id="categories-list" name="categories-list">
                <span class="legend">
                    <?php _e('Show', 'roots'); ?>
                </span>
                    <bottom data-toggle="dropdown" id="category-name" type="text" class="btn btn-default dropdown-toggle type-bottom"><?php echo $cat_name ? $cat_name : __('View all', 'roots'); ?></bottom>
                    <ul class="dropdown-menu">
                        <li>
                            <input name="category" id="category-all" value="all" type="radio" <?php echo !$cat_id ? 'checked' : ''; ?>>
                            <label for="category-all"><?php _e('View all', 'roots'); ?></label>
                        </li>
                        <?php foreach($categories as $category) : ?>
                            <li>
                                <input name="category" id="category-<?php echo $category->cat_ID; ?>" value="<?php echo $category->cat_ID; ?>" type="radio" <?php echo $category->cat_ID == $cat_id ? 'checked' : ''; ?>>
                                <label for="category-<?php echo $category->cat_ID; ?>"><?php echo $category->name; ?></label>
                            </li>
                        <?php endforeach; ?>
                    </ul>
                    <input value="posts-by-categories" name="action" type="hidden">
                </form>
            </div>
        </header>
    </div>
    <div id="search-form" class="container">
        <?php echo get_search_form(); ?>
    </div>
    <?php wp_reset_query(); ?>

    <div id="know-main" class="container">
        <div id="know-main-inner" class="masonry">
            <?php
            if (get_query_var('search')) :
                require_once(get_template_directory(). '/search.php');
                $hide = 'hide';
            else :
                require_once(get_template_directory(). '/templates/partials/_knowledge.php'); ?>
                <?php ($counter >= 10 ? $hide = '': $hide = 'hide') ; ?>

            <?php endif; ?>
        </div>
    </div>

    <div class="container charge <?php echo $hide;?>">
        <div class="col-md-12">
            <a id="more-posts"><?php _e('Load more posts', 'roots'); ?></a>
        </div>
    </div>
</div>