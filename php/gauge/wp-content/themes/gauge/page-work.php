<?php
/**
 * Created by WebStorm.
 * User: anderson
 * Template Name: Trabalhos
 * Date: 12/30/14
 * Time: 1:35 PM
 */
wp_reset_query();
$args = array(
    'type'        => 'post',
    'child_of'    => 38,
    'orderby'     => 'name',
    'order'       => 'ASC',
    'hide_empty'  => 0,
    'taxonomy'    => 'category',
    'post_status' => 'publish'
);

$categories = get_categories($args);
$cat_id     = get_query_var('cat');

?>
<div id="work-wrapper">
    <div id="work-header-wrapper" class="yellow-bg">
        <header id="work" class="container">
            <div class="col-sm-7">
                <h1 class="category-title">
                <span>
                    <?php
                        if ($cat_id) {
                            $cat_name = get_category($cat_id)->name;
                            echo $cat_name;
                        } else {
                            _e('Meet our work and clients', 'roots');
                        }
                    ?>
                </span>
                </h1>
            </div>
            <div class="col-sm-4 form-one">
                <form id="categories-list" name="categories-list">
                <span class="legend">
                    <?php _e('Service', 'roots'); ?>
                </span>
                    <bottom data-toggle="dropdown" id="category-name" type="text" class="btn btn-default dropdown-toggle type-bottom"><?php echo $cat_name ? $cat_name : __('View all', 'roots'); ?></bottom>
                    <ul class="dropdown-menu">
                        <li>
                            <input name="category" id="category-all" value="all" type="radio" <?php echo !$cat_id ? 'checked' : ''; ?>>
                            <label for="category-all"><?php _e('View all', 'roots'); ?></label>
                        </li>
                        <?php foreach($categories as $category) : ?>
                            <?php if ($category->count > 0) : ?>
                            <li>
                                <input name="category" id="category-<?php echo $category->cat_ID; ?>" value="<?php echo $category->cat_ID; ?>" type="radio" <?php echo $category->cat_ID == $cat_id ? 'checked' : ''; ?>>
                                <label for="category-<?php echo $category->cat_ID; ?>"><?php echo $category->name; ?></label>
                            </li>
                            <?php endif; ?>
                        <?php endforeach; ?>
                    </ul>
                    <input value="posts-by-categories" name="action" type="hidden">
                </form>
            </div>
        </header>
    </div>
    <div id="work-main" class="container">
        <div id="work-main-inner" class="masonry">
            <?php
                require_once(get_template_directory() . '/templates/partials/_work.php');
            ?>
        </div>
        <div class="clearfix"></div>
    </div>
    <?php ($counter >= 10 ? $hide = '': $hide = 'hide') ; ?>
    <div class="container charge <?php echo $hide;?>">
        <div class="col-md-12">
            <a id="more-posts"><?php _e('Load more posts', 'roots'); ?></a>
        </div>
    </div>
    <?php
        require_once(get_template_directory() . '/templates/partials/_clients.php');
    ?>
</div>