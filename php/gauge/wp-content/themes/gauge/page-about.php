<?php
/**
 * Created by WebStorm.
 * User: anderson
 * Template Name: Sobre
 * Date: 2/12/15
 * Time: 10:09 AM
 */

// Posts of "Sobre" category
// Featured category ID 10

query_posts('cat=10&p=74');

while(have_posts()) :

    the_post();

    $id = get_the_ID();
    $featured_bg = wp_get_attachment_image_src(get_post_thumbnail_id($id), 'full');
    $style = '';

    if (get_post_meta( $id, 'post-video-url')[0]) {
        $video     = true;
        $video_url = get_post_meta( $id, 'post-video-url')[0];

        if (strpos($video_url, 'youtube')) {
            $video_url = explode('v=', $video_url);
            $query = '?vq=720&enablejsapi=1';
            $embed = '//www.youtube.com/embed/' . $video_url[1];
        }
    } else {
        $video = false;
    }

    if ($featured_bg[0]) {
        $style = 'style="background-image: url(' .$featured_bg[0] . ');"';
    }
    ?>
    <div class="about-featured-wrapper" <?php echo $style; ?>>
        <?php if ($video) : ?>
            <!--<iframe src="<?php //echo $embed; ?>" frameborder="0" allowfullscreen></iframe>-->
            <div id="ytplayer"></div>
            <script>
                var player;
                function onYouTubePlayerAPIReady() {
                    player = new YT.Player('ytplayer', {
                        videoId: <?php echo '"'. $video_url[1] .'"'; ?>
                    });
                }
            </script>
            <?php $controls = '
                <div class="toggle-content-type col-md-12">
                    <div class="about-content-type pull-right">
                        <!--<a href="#" class="to-txt"></a>-->
                        <a id="video_inovacao_em _negocios" href="#" class="to-video"></a>
                        <a href="#" class="stop-video"></a>
                    </div>
                </div>
            '; ?>
        <?php endif; ?>
        <div class="about-featured">
            <div class="container">
                <?php echo $controls; ?>
                <div class="content-txt col-md-12">
                    <div class="content-txt-inner">
                        <h1 class="content-featured-title">
                            <?php the_title(); ?>
                        </h1>
                        <?php the_content(); ?>
                    </div>
                </div>
            </div>
        </div>
        <div class="container controls">
            <a id="know-more" href="#about">
                <span id="know-more-inner"></span>
            </a>
        </div>
    </div>
<?php endwhile; ?>

<?php wp_reset_query(); ?>
<?php // about section ?>
<?php get_template_part('templates/about'); ?>
<?php // end about sectioin ?>
<?php $post_78 = get_post(78); ?>
<div class="clearfix"></div>
<div class="our-services c320 <?= (isset($_GET['only-text']) ? ' is-not-visible' : ''); ?>">
    <div class="container">
        <div class="col-md-12">
            <?php $post_44 = get_post(44); ?>
            <h2>
                <?php echo $post_78->post_title; ?>
            </h2>
            <p>
                <?php echo $post_78->post_content; ?>
            </p>
        </div>
    </div>
</div>
<?php $post_80 = get_post(80); ?>
<div id="open-image-wrapper" class="container <?= (isset($_GET['only-text']) ? ' is-not-visible' : ''); ?>">
    <button class="open-image btn btn-info">
        <?php _e('See work process', 'roots'); ?>
    </button>
</div>
<div id="our-process-wrapper" class="<?= (isset($_GET['only-text']) ? ' is-visible' : ''); ?>">
    <div class="our-process">
        <div class="container">
            <div class="col-md-12">
                <h2 class="our-process-featured-title">
                    <?php echo $post_78->post_title; ?>
                </h2>
                <div class="our-process-featured-txt">
                    <?php echo $post_78->post_content; ?>
                </div>
            </div>
            <div class="table-style first">
                <div class="table-row-style">
                    <div class="table-cell-style single">
                        <div class="col-md-9">
                            <span class="arrow-up"></span>
                            <div class="left-column">
                                <h2 class="our-process-title">
                                    <?php echo $post_80->post_title; ?>
                                </h2>
                                <div class="our-process-txt">
                                    <?php echo $post_80->post_content; ?>
                                </div>
                            </div>
                        </div>
                    </div>

                    <?php
                    $args = array(
                        'posts_per_page' => '-1',
                        'category'    => '13',
                        'orderby'     => 'none',
                        'order'       => 'DESC',
                        'post__not_in' => array(78, 80),
                        'post_status' => 'publish'
                    );

                    $posts = get_posts($args);
                    ?>
                    <div class="table-cell-style">
                        <?php foreach($posts as $post) : ?>
                            <div class="col-md-7 pull-right">
                                <h2 class="our-process-title">
                                    <?php echo $post->post_title; ?>
                                </h2>
                                <div class="our-process-txt">
                                    <?php echo $post->post_content; ?>
                                </div>
                            </div>
                        <?php endforeach; ?>
                    </div>
                </div>
            </div>
        </div>
    </div>
</div>
<?php wp_reset_query(); ?>

<?php
$args = array(
    'posts_per_page' => '-1',
    'category'    => '14',
    'orderby'     => 'none',
    'order'       => 'DESC',
    'post_status' => 'publish',
);

$posts = get_posts($args);
$counter = 1;
?>

<div id="our-specialties-wrapper">
    <div class="our-specialties">
        <div class="container">
            <?php foreach($posts as $post) : ?>
                <?php if($post->ID == 91) : ?>
                    <div class="col-md-12">
                        <h2 class="specialty-featured-title">
                            <?php echo $post->post_title; ?>
                        </h2>
                        <div class="specialty-featured-txt">
                            <?php echo $post->post_content; ?>
                        </div>
                    </div>
                <?php else : ?>
                    <div class="col-md-6 specialty-content article-outer post-<?php echo $post->ID; ?>">
                        <h2 class="specialty-title <?php echo $counter == 1 ? ' first' : '';?>" data-id="#section-<?php echo $post->ID; ?>">
                            <div class="table-style">
                                <span class="our-specialty-icon">
                                </span>
                                <span class="title-txt">
                                <?php echo $post->post_title; ?>
                                </span>
                                <span class="indicator plus"></span>
                            </div>
                        </h2>
                        <span class="clearfix"></span>

                        <div id="section-<?php echo $post->ID; ?>" post-id="<?php echo $post->ID; ?>" class="specialty-txt details-links">
                            <?php $excerpt = $post->post_excerpt; ?>
                            <?php $content = $post->post_content; ?>
                            <?php $content = str_replace('&nbsp;', '', $content); ?>
                            <span class="content-list" id="<?php echo str_replace('-', '_', $post->post_name); ?>">
                                <?php echo $content; ?>
                            </span>
                        </div>
                    </div>
                    <?php if ($counter % 2 == 0 ) : ?>
                        <div class="post-separator clearfix"></div>
                    <?php endif; ?>
                    <?php $counter++; ?>
                <?php endif; ?>
            <?php endforeach; ?>
        </div>
    </div>
</div>
<?php wp_reset_query(); ?>

