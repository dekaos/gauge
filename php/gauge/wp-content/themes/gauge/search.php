<?php
/**
 * Created by WebStorm.
 * User: anderson
 * Date: 11/30/14
 * Time: 3:48 PM
 */

wp_reset_query();

global $wp_query;
?>
<div class="search-results-box">
<?php

if (!empty($_GET['search'])) {

    if (!empty($_GET['cat'])) {
        $cat = sanitize_text_field($_GET['cat']);
    }

    $s = sanitize_text_field($_GET['search']);

    if ($cat != 'all') {
        $s_query = array('s' => $s, 'category__in' => $cat);
        $cat_name = get_cat_name($cat);
    } else {
        $s_query = array('s' => $s, 'category__in' => '17');
    }

    $items   = new WP_Query($s_query);
    $results = count($items->posts);

    if ($results > 0) : ?>
        <?php $results > 1 ? $plural = 's' : $plural = ''; ?>
        <div class="col-md-12 text-center">
            <div class="search-result-info">
                <?php
                    if (!empty($cat_name)) {
                        printf(__('Your search for %s, found %s result%s in %s.', 'roots'),
                            '<span>"' . $s . '</span>"', '<span>' . $results . '</span>', $plural, '<span>' . $cat_name . '</span>');
                    } else {
                        printf(__('Your search for %s, found %s result%s.', 'roots'),
                            '<span>"' . $s . '</span>"', '<span>' . $results . '</span>', $plural);
                    }
                ?>
            </div>
        </div>
        <div class="clearfix"></div>
        <?php

            require_once(get_template_directory() . '/templates/partials/_items.php');

        else : ?>
            <div class="col-md-12 text-center">
                <div class="search-result-info">
                    <?php printf(__('You search for %s did not return any results.', 'roots'), '<span>"' . $s . '"</span>'); ?>
                    <span class="second-line">
                    <?php _e('Try a new search using other words.', 'roots'); ?>
                    </span>
                </div>
            </div>
            <div class="col-md-12">
                <h2 class="highlight-title">
                    <?php _e('Also Check out our highlights.', 'roots'); ?>
                </h2>
            </div>
            <div class="clearfix"></div>
            <div class="no-results-box">
                <?php require_once(get_template_directory() . '/templates/partials/_knowledge.php'); ?>
            </div>
    <?php endif; ?>
<?php
}
wp_reset_query(); ?>
</div>
