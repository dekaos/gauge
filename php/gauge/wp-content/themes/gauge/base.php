<?php get_template_part('templates/head'); ?>
<body <?php body_class(); ?>>
<!-- Google Tag Manager -->
<noscript>
  <iframe src="//www.googletagmanager.com/ns.html?id=GTM-MBDG82" height="0" width="0" style="display:none;visibility:hidden"></iframe>
</noscript>
<script>
  (function(w,d,s,l,i){w[l]=w[l]||[];w[l].push({'gtm.start': new Date().getTime(),event:'gtm.js'});
    var f=d.getElementsByTagName(s)[0],j=d.createElement(s),dl=l!='dataLayer'?'&l='+l:'';j.async=true;j.src='//www.googletagmanager.com/gtm.js?id='+i+dl;f.parentNode.insertBefore(j,f);
  })(window,document,'script','dataLayer','GTM-MBDG82');
</script>
<!-- End Google Tag Manager -->
  <!--[if lt IE 10]>
    <div class="alert alert-warning">
      <?php _e('You are using an <strong>outdated</strong> browser. Please <a href="http://browsehappy.com/">upgrade your browser</a> to improve your experience.', 'roots'); ?>
    </div>
  <![endif]-->
  <?php
    do_action('get_header');
    // Use Bootstrap's navbar if enabled in config.php
    if (current_theme_supports('bootstrap-top-navbar')) {
      get_template_part('templates/header-top-navbar');
    } else {
      get_template_part('templates/header');
    }
  ?>
  <main class="main" role="main">
  <?php

    if (is_search()) :
      require_once('search.php');
    else :

    if (is_home()) :
      get_template_part('templates/home');
    else :
    ?>
      <?php
        include roots_template_path();
      ?>

    <?php if (roots_display_sidebar() && !is_category() && !is_page()) : ?>
      <aside class="sidebar <?php echo roots_sidebar_class(); ?>" role="complementary">
        <?php include roots_sidebar_path(); ?>
      </aside><!-- /.sidebar -->
    <?php endif; ?>
  <?php endif; ?>
  <?php endif; ?>
  </main><!-- /.main -->
  <?php get_template_part('templates/footer'); ?>

  <?php

  wp_reset_query();

  $specialty_id = $_POST['specialty_id'];
  $specialty    = get_post(94);

  ?>
</body>
</html>