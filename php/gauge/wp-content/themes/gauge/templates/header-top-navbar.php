<?php if (is_page('775') || is_page('774')) : ?>
<div class="image-zoom-wrapper">
  <div class="zoom-close"></div>
  <img class="img-target" src="<?php echo get_template_directory_uri(); ?>/assets/img/process.jpg" alt=""/>
  <div class="zoom-buttons">
    <div id="zoomInButton" class="zoom-plus pull-left"></div>
    <div id="zoomOutButton" class="zoom-minus pull-left"></div>
  </div>
</div>
<?php endif; ?>
<header class="banner navbar navbar-default navbar-fixed-top" role="banner">
  <div class="container">
    <div class="navbar-header">
      <button type="button" class="navbar-toggle" data-toggle="collapse" data-target=".navbar-collapse">
        <span class="sr-only">Toggle navigation</span>
        <span class="icon-bar"></span>
        <span class="icon-bar"></span>
        <span class="icon-bar"></span>
      </button>
      <a class="navbar-brand" href="<?php echo home_url(); ?>/">
        <img src="<?php echo get_template_directory_uri(); ?>/assets/img/logo.png" width="129" height="39" alt="<?php bloginfo('name'); ?>"/>
      </a>
      <div class="language-selection pull-left">
          <?php $blog_id = get_current_blog_id(); ?>
          <?php switch($blog_id) {
              case '5':
                  $lang_label = 'En';
                  break;
              case '6':
                  $lang_label = 'Es';
                  break;
              case '7':
                  $lang_label = 'Ds';
                  break;
              default:
                  $lang_label = 'Pt';
            }
        ?>
        <!--<span class="active-lang">
            <?php //echo $lang_label; ?><span class="v-arrow"></span>
        </span>-->
        <!--<div class="langs">
            <?php if (is_active_sidebar('sidebar-top')) : ?>
                <div class="sidebar-outer">
                    <div class="sidebar-inner">
                        <?php dynamic_sidebar('sidebar-top'); ?>
                    </div>
                </div>
            <?php endif; ?>
        </div>-->
        <div class="clearfix"></div>
      </div>
    </div>
    <nav class="collapse navbar-collapse top-nav" role="navigation">
      <?php
        if (has_nav_menu('primary_navigation')) :
          wp_nav_menu(array('theme_location' => 'primary_navigation', 'menu_class' => 'nav navbar-nav top-menu', 'menu_id' => 'menu_id', 'link_before' => '<span></span>'));
        endif;
      ?>
      <a id="toggle-menu" class="visible" href="#"></a>
    </nav>
  </div>
</header>