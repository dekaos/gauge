<footer id="footer" class="footer-wrapper">
    <?php if (!is_home()) : ?>
    <div id="division-bell">
        <hr/>
        <div class="g-title">
            <span></span>
        </div>
    </div>
    <?php endif; ?>
    <div id="contact" class="contact-wrapper">
        <div id="contact-inner" class="container">
            <?php
            // plugin contato
            if (function_exists(offices_front_end()))
                offices_front_end();
            ?>
        </div>
        <div class="bg-line"></div>
    </div>
    <div id="footer-inner">
        <div class="container">
            <div class="col-md-6">
                <p class="pull-left">
                    <?php echo '&copy; ' . date('Y'); ?> Gauge - Todos os direitos revervados
                </p>
            </div>
            <div class="col-md-6">
                <ul class="social-networks pull-right">
                    <li>
                        <a id="rodape_twitter" target="_blank" href="https://twitter.com/gauge"></a>
                    </li>
                    <li>
                        <a id="rodape_facebook" target="_blank" href="https://www.facebook.com/gauge"></a>
                    </li>
                    <li>
                        <a id="rodape_linkedin" target="_blank" href="https://www.linkedin.com/company/gauge"></a>
                    </li>
                    <li>
                        <a id="rodape_instagram" target="_blank" href="http://instagram.com/beanexperience"></a>
                    </li>
                </ul>
            </div>
        </div>
    </div>
</footer>
<?php if(is_single()) : ?>
    <?php global $post; ?>
     <div class="social-share">
        <div class="share-wrapper">
            <div class="share-box">
                <span class="close-bottom"></span>
                <div class="share-title">
                    <h2>
                        <?php _e('Share', 'roots'); ?>
                    </h2>
                </div>
                <div class="share-post-title">
                    <h2><?php the_title(); ?></h2>
                </div>
                <div class="share-options">
                    <div class="addthis_sharing_toolbox"></div>
                </div>
                <div class="share-link">
                    <h2>Link</h2>
                    <input id="copy-url" class="pull-left" value="<?php echo get_permalink(); ?>">
                    <a id="compartilhar_copiar_<?php echo str_replace('-', '_', $post->post_name); ?>" class="copy-bottom btn btn-info pull-right"><?php _e('Copy link', 'roots'); ?></a>
                    <div class="clearfix"></div>
                </div>
            </div>
        </div>
     </div>
<?php endif; ?>
<script src="//www.youtube.com/player_api"></script>
<?php wp_footer(); ?>
<script>
    (function(f,b){
        var c;
        f.hj=f.hj||function(){(f.hj.q=f.hj.q||[]).push(arguments)};
        f._hjSettings={hjid:34844, hjsv:4};
        c=b.createElement("script");c.async=1;
        c.src="//static.hotjar.com/c/hotjar-"+f._hjSettings.hjid+".js?sv="+f._hjSettings.hjsv;
        b.getElementsByTagName("head")[0].appendChild(c);
    })(window,document);
</script>