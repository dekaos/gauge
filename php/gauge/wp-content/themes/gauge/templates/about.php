<?php
$args = array(
    'posts_per_page' => '-1',
    'category'    => '4, -10, 12, -13, -14',
    'orderby'     => 'none',
    'order'       => 'DESC',
    'post_status' => 'publish'
);
?>
<?php $posts = get_posts( $args ); ?>
    <div id="about" class="about-wrapper">
        <div id="about-inner" class="container">
            <?php if($_GET['v']  == 2) : ?>
                <div class="featured v2 col-md-11">
                    <h2>
                        <?php echo $posts[0]->post_title; ?>
                    </h2>
                    <?php echo $posts[0]->post_content; ?>
                </div>
                <div class="clearfix"></div>
                <div class="col-md-6">
                    <div class="about-txt v2">
                        <?php echo $posts[1]->post_content; ?>
                    </div>
                </div>
                <div class="col-md-6">
                    <div class="about-txt v2">
                        <?php echo $posts[2]->post_content; ?>
                    </div>
                </div>
                <div class="clearfix"></div>
                <div class="col-md-6">
                    <div class="about-txt v2">
                        <?php echo $posts[3]->post_content; ?>
                    </div>
                </div>
                <div class="col-md-6">
                    <div class="about-txt v2">
                        <?php echo $posts[4]->post_content; ?>
                    </div>
                </div>
                <div class="col-md-6">
                    <div class="about-txt v2">
                        <?php echo $posts[5]->post_content; ?>
                    </div>
                </div>
            <?php else : ?>
                <?php foreach($posts as $post) : ?>
                    <?php if($post->ID == '27') : ?>
                        <div class="featured col-md-12">
                            <h2>
                                <?php echo $post->post_title; ?>
                            </h2>
                            <?php echo $post->post_content; ?>
                        </div>
                    <?php else : ?>
                        <div class="about-detail-wrapper">
                            <div class="col-md-3 about-detail post-<?php echo $post->ID; ?>">
                                <div class="post-image"></div>
                                <div class="about-txt">
                                    <?php
                                    if (isset($_GET['left'])) {
                                        echo '<div style="text-align: left">' . preg_replace('/<p[^>]*>(.*)<\/p[^>]*>/i', '$1', $post->post_content) . '</div>';
                                    } else {
                                        echo '<div>' . preg_replace('/<p[^>]*>(.*)<\/p[^>]*>/i', '$1', $post->post_content) . '</div>';
                                    }
                                    ?>
                                </div>
                                <div class="clearfix"></div>
                            </div>
                        </div>
                    <?php endif; ?>
                <?php endforeach; ?>
            <?php endif; ?>
        </div>
        <div class="clearfix"></div>
    </div>
<?php wp_reset_query();