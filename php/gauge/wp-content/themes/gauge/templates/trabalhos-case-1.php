<?php
/**
 * Created by WebStorm.
 * User: anderson
 * Single Post Template: Trabalhos - Case 1
 * Date: 1/4/15
 * Time: 8:06 PM
 */

global $post;

?>

<div class="single-post-wrapper template-case template-case-1">
    <div class="yellow-bg">
        <header class="entry-meta">
            <div class="container">
                <a id="sp-back" href="<?php echo get_permalink(359); ?>">
                    <?php _e('Back', 'roots'); ?>
                </a>
                <h1 class="post-title">
                    <?php echo get_the_title($post->ID); ?>
                </h1>
            </div>
        </header>
    </div>
    <?php
        require_once(get_template_directory() . '/templates/partials/_sp-specialties.php');
    ?>
    <div class="container post-outer">
        <article <?php post_class(); ?>>
            <?php while (have_posts()) : the_post(); ?>
                <div class="entry-content">
                    <?php the_content(); ?>
                </div>
            <?php endwhile; ?>
        </article>
    </div>
    <div class="clearfix"></div>
</div>
<?php wp_reset_query(); ?>
<?php require_once(get_template_directory() . '/templates/partials/_related-posts.php'); ?>