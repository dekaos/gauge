<?php
/**
 * Created by WebStorm.
 * User: anderson
 * Single Post Template: Conhecimento - Template 1
 * Date: 12/18/14
 * Time: 9:50 AM
 */
global $post;

?>
<div class="single-post-wrapper template-1">
    <?php
        $featured_bg = wp_get_attachment_image_src(get_post_thumbnail_id($post->ID), 'full');
        $style = '';

    if ($featured_bg[0]) {
        $style = ' style="background-image: url(' .$featured_bg[0] . ');"';
    }
    ?>
    <div class="featured-image" data-media="<?php echo $featured_bg[0]; ?>">
        <header>
            <?php $title = mb_substr(get_the_title($post->ID), 0, 70); ?>
            <div class="container">
                <h1 class="entry-title"><?php echo $title; ?></h1>
            </div>
        </header>
    </div>
    <div class="container post-outer">
        <article <?php post_class(); ?>>
            <?php while (have_posts()) : the_post(); ?>
                <div class="entry-content">
                    <?php the_content(); ?>
                </div>
            <?php endwhile; ?>
            <?php require_once(get_template_directory() . '/templates/partials/_sidebar-right.php'); ?>
        </article>
    </div>
    <div class="clearfix"></div>
</div>
<?php wp_reset_query(); ?>
<?php require_once(get_template_directory() . '/templates/partials/_related-posts.php'); ?>
