<!DOCTYPE html>
<html class="no-js" <?php language_attributes(); ?>>
<head>
  <meta charset="utf-8">
  <meta http-equiv="X-UA-Compatible" content="IE=edge">
  <title><?php wp_title('|', true, 'right'); ?></title>
  <meta name="viewport" content="width=device-width, initial-scale=1">

  <?php wp_head(); ?>
  <link rel="icon" type="image/png"  href="<?php echo esc_url( get_template_directory_uri() ); ?>/assets/img/favicon.png">
  <!--[if IE 8]>
  <script src='<?php echo get_template_directory_uri(); ?>/assets/js/respond.min.js'></script>
  <![endif]-->
</head>
