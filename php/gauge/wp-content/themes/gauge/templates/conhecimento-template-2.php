<?php
/**
 * Created by WebStorm.
 * User: anderson
 * Single Post Template: Conhecimento - Template 2
 * Date: 12/18/14
 * Time: 9:50 AM
 */

global $post; ?>

<div class="single-post-wrapper template-2">
    <div class="yellow-bg">
        <header class="entry-meta">
            <div class="container">
                <h1 class="post-title">
                    <?php echo get_the_title($post->ID); ?>
                </h1>
            </div>
        </header>
    </div>
    <div class="container post-outer">
        <article <?php post_class(); ?>>
            <?php while (have_posts()) : the_post(); ?>
                <div class="entry-content">
                    <?php the_content(); ?>
                </div>
            <?php endwhile; ?>
        </article>
        <?php require_once(get_template_directory() . '/templates/partials/_sidebar-right.php'); ?>
    </div>
    <div class="clearfix"></div>
</div>
<?php wp_reset_query(); ?>
<?php require_once(get_template_directory() . '/templates/partials/_related-posts.php'); ?>
