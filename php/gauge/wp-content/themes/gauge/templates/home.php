<?php if (isset($_GET['c']) && $_GET['c'] == 'static') {
  $posts_per_page = 1;
  $carousel_class = 'no-swiper-slide';
} else {
    $posts_per_page = -1;
    $carousel_class = 'swiper-slide';
}
?>
<?php query_posts('cat=3&posts_per_page=' . $posts_per_page); ?>
<?php global $post; ?>

    <div id="carousel" class="swiper-container">
        <div class="swiper-wrapper">
            <?php while(have_posts()) : ?>
            <?php the_post(); ?>
            <div class="<?php echo $carousel_class; ?>">
                <?php $img_src = wp_get_attachment_image_src(get_post_thumbnail_id(), 'full'); ?>
                <div class="inner-slide">
                    <div class="post-txt">
                        <div class="post-txt-inner" id="home-slide-<?php echo $post->ID; ?>">
                            <h2 class="post-title">
                                <?php the_title(); ?>
                            </h2>
                            <?php the_content(); ?>
                            <div class="clearfix"></div>
                        </div>
                    </div>
                    <div class="post-img">
                        <?php $img_src = wp_get_attachment_image_src(get_post_thumbnail_id(), 'full'); ?>
                        <?php if($img_src[0]) : ?>
                            <img src="<?php echo $img_src[0]; ?>" alt="<?php the_title(); ?>">
                        <?php endif; ?>
                    </div>
                </div>
            </div>
            <?php endwhile; ?>
        </div>
    </div>
<div class="container controls">
    <div class="pagination"></div>
    <a id="know-more" href="#about">
        <span id="know-more-inner"></span>
    </a>
</div>
<?php if(isset($_GET['v'])) : ?>
<div class="home-b" id="about">&nbsp;</div>
<?php endif; ?>
<?php if(!isset($_GET['v'])) : ?>
<?php wp_reset_query(); ?>
<?php // about section ?>
<?php require_once('about.php'); ?>
<?php // end about section ?>
<?php

$args = array(
    'posts_per_page' => '-1',
    'category'    => '5',
    'orderby'     => 'none',
    'order'       => 'DESC',
    'post_status' => 'publish'
);
?>
<?php $post_44 = get_post(44); ?>
<div id="services" class="services-wrapper">
    <div id="services-inner" class="container">
        <div class="featured col-md-11">
            <h2>
                <?php echo $post_44->post_title; ?>
            </h2>
            <?php echo $post_44->post_content; ?>
        </div>
        <?php $post_559 = get_post(559); ?>
        <div class="col-sm-3 post-<?php echo $post_559->ID; ?>">
            <div class="box">
                <a id="home_servicos_<?php echo str_replace('-', '_', $post_559->post_name); ?>" href="<?php echo get_the_permalink($post_559->ID); ?>">
                    <div class="service-image"></div>
                    <div class="services-txt">
                        <h3>
                            <?php echo $post_559->post_title; ?>
                        </h3>
                        <span class="h-arrow"></span>
                    </div>
                    <div class="clearfix"></div>
                </a>
            </div>
        </div>
        <?php $post_561 = get_post(561); ?>
        <div class="col-sm-3 post-<?php echo $post_561->ID; ?>">
            <div class="box">
                <a id="home_servicos_<?php echo str_replace('-', '_', $post_561->post_name); ?>" href="<?php echo get_the_permalink($post_561->ID); ?>">
                    <div class="service-image"></div>
                    <div class="services-txt">
                        <h3>
                            <?php echo $post_561->post_title; ?>
                        </h3>
                        <span class="h-arrow"></span>
                    </div>
                    <div class="clearfix"></div>
                </a>
            </div>
        </div>
        <?php $post_563 = get_post(563); ?>
        <div class="col-sm-3 post-<?php echo $post_563->ID; ?>">
            <div class="box">
                <a id="home_servicos_<?php echo str_replace('-', '_', $post_563->post_name); ?>" href="<?php echo get_the_permalink($post_563->ID); ?>">
                    <div class="service-image"></div>
                    <div class="services-txt">
                        <h3>
                            <?php echo $post_563->post_title; ?>
                        </h3>
                        <span class="h-arrow"></span>
                    </div>
                    <div class="clearfix"></div>
                </a>
            </div>
        </div>
        <?php $post_565 = get_post(565); ?>
        <div class="col-sm-3 post-<?php echo $post_565->ID; ?>">
            <div class="box">
                <a id="home_servicos_<?php echo str_replace('-', '_', $post_565->post_name); ?>" href="<?php echo get_the_permalink($post_565->ID); ?>">
                    <div class="service-image"></div>
                    <div class="services-txt">
                        <h3>
                            <?php echo $post_565->post_title; ?>
                        </h3>
                        <span class="h-arrow"></span>
                    </div>
                    <div class="clearfix"></div>
                </a>
            </div>
        </div>
    </div>
    <div class="clearfix"></div>
</div>
<?php endif; ?>
<div id="cases" class="cases-wrapper">
    <div id="cases-inner" class="container">
        <div class="se-modular-items">
            <?php require_once(get_template_directory() . '/templates/partials/_se-modular.php'); ?>
        </div>
    </div>
    <div class="clearfix"></div>
</div>
<?php if (!isset($_GET['v'])) : ?>
<div id="g-title-wrapper">
    <hr/>
    <div class="container">
        <div class="g-title">
            <span></span>
        </div>
    </div>
</div>
<?php endif; ?>