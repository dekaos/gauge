<?php
/**
 * Created by WebStorm.
 * User: anderson
 * Single Post Template: Trabalhos - Full Image
 * Date: 1/4/15
 * Time: 8:06 PM
 */

global $post;

$category_class = '';

if (has_category(6, $post->ID)) {
    $color_class = 'color-one';
}

if (has_category(7, $post->ID)) {
    $color_class = 'color-two';
}

if (has_category(8, $post->ID)) {
    $color_class = 'color-tree';
}

if (has_category(9, $post->ID)) {
    $color_class = 'color-four';
}

?>

<div class="single-post-wrapper template-case template-case-full-image-wrapper  <?= $category_class; ?>">
    <?php $img = wp_get_attachment_image_src(get_post_thumbnail_id($post->ID), 'full'); ?>
    <div class="template-full-image-outer" style="background: url(<?= $img[0]; ?>) no-repeat center center;">
        <div class="template-full-image-inner">
            <div class="container">
                <h1 class="single-post-tile">
                    <?= get_the_title($post->ID); ?>
                </h1>
                <a class="page-more" href="#">
                    <span id="know-more-inner"></span>
                </a>
            </div>
        </div>
    </div>
    <div class="yellow-bg">
        <header class="entry-meta">
            <div class="container">
                <a id="sp-back" href="<?php echo get_permalink(359); ?>">
                    <?php _e('Back', 'roots'); ?>
                </a>
                <h1 class="post-title">
                    <?php echo get_the_title($post->ID); ?>
                </h1>
            </div>
        </header>
    </div>
    <?php
    require_once(get_template_directory() . '/templates/partials/_sp-specialties.php');
    ?>
    <div class="container post-outer">
        <article <?php post_class(); ?>>
            <?php while (have_posts()) : the_post(); ?>
                <div class="entry-content">
                    <?php the_content(); ?>
                </div>
            <?php endwhile; ?>
        </article>
    </div>
    <div class="clearfix"></div>
</div>
<?php wp_reset_query(); ?>
<?php require_once(get_template_directory() . '/templates/partials/_related-posts.php'); ?>