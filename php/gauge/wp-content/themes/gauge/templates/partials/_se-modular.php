<?php
wp_reset_postdata();

global $wpdb;

$dims = array(
    'size_220-220',
    'size_220-460',
    'size_460-460',
    'size_460-220'
);

if (is_page(565)) {
    $cat_jb_id = 9;
}

if (is_page(559)) {
    $cat_jb_id = 6;
}

if (is_page(561)) {
    $cat_jb_id = 7;
}

if (is_page(563)) {
    $cat_jb_id = 8;
}

if (is_home()) {
    $cat_jb_id = 38;
}

$counter = 0;

$size = $class = $dims[0];

if (isset($_POST['se_cat'])) {
    $cat_id = $_POST['se_cat'];

    if ($cat_id == 38) {
        require_once(get_template_directory() . '/templates/partials/_cat-jb.php');
    }

    if ($cat_id == 17) {
        require_once(get_template_directory() . '/templates/partials/_cat-kn.php');
    }

    if ($cat_id == 'net') {
        require_once(get_template_directory() . '/templates/partials/_cat-net.php');
    }
}

if (!$_POST['se_cat'] || $_POST['se_cat'] == 'all') {
    if ($_POST['se_cat'] == 'all') {
        $cat_jb_id = $_POST['single_cat_id'];
    }

    $args_kn = array(
        'posts_per_page'   => 3,
        'offset'           => 0,
        'category__in'     => array(17),
        'orderby'          => 'post_date',
        'order'            => 'DESC',
        'post_type'        => 'post',
        'post_status'      => 'publish',
        'suppress_filters' => true
    );
    $limit = 3;

    $cat_in_query = $wpdb->get_results("SELECT * FROM ytew5_cases WHERE type_case='in' AND published=1 ORDER BY id ASC LIMIT 3");
    $cat_fb_query = $wpdb->get_results("SELECT * FROM ytew5_cases WHERE type_case='fb' AND published=1 ORDER BY id ASC LIMIT 3");
    //$cat_tw_query = $wpdb->get_results("SELECT * FROM ytew5_cases WHERE type_case='tw' AND published=1 ORDER BY id ASC LIMIT 3");
    $cat_kn_query = get_posts($args_kn);

    $cat_sufix_kn = 'know';
    $index_zero   = 0;
    $index_one    = 1;

    $args_jb = array(
        'posts_per_page'   => 4,
        'offset'           => 0,
        'category__in'     => array(9, 6, 7, 8, 38),
        'orderby'          => 'post_date',
        'order'            => 'DESC',
        'post_type'        => 'post',
        'post_status'      => 'publish',
        'suppress_filters' => true
    );

    $cat_jb_query = get_posts($args_jb);

    if (!$cat_kn_query) {
        $cat_sufix_kn = 'jb';
        $cat_kn_query = $cat_jb_query;
        $index_zero   = 2;
        $index_one    = 3;
    }

    if ($cat_fb_query[0]->text == '') {
        $text_0 = $cat_fb_query[0]->description;
    } else {
        $text_0 = $cat_fb_query[0]->text;
    }

    if ($cat_fb_query[1]->text == '') {
        $text_1 = $cat_fb_query[1]->description;
    } else {
        $text_1 = $cat_fb_query[1]->text;
    }

    $all_items = array(
        0 => array(
            'id'    => $cat_jb_query[0]->ID,
            'title' => $cat_jb_query[0]->post_title,
            'text'  => $cat_jb_query[0]->post_content,
            'media' => 'hold',
            'user'  => '',
            'class' => 'category-jb', //get_category($cat_jb_query[0]->ID)->slug,
            'link'  => get_the_permalink($cat_jb_query[0]->ID)
        ),
        1 => array(
            'id'    => $cat_in_query[0]->id,
            'title' => '',
            'text'  => $cat_in_query[0]->description,
            'media' => $cat_in_query[0]->media_url,
            'user'  => '',
            'class' => 'category-in',
            'link'  => $cat_in_query[0]->link
        ),
        2 => array(
            'id'    => $cat_in_query[1]->id,
            'title' => '',
            'text'  => $cat_in_query[1]->description,
            'media' => $cat_in_query[1]->media_url,
            'user'  => '',
            'class' => 'category-in',
            'link'  => $cat_in_query[1]->link
        ),
        3 => array(
            'id'    => $cat_kn_query[$index_one]->ID,
            'title' => $cat_kn_query[$index_one]->post_title,
            'text'  => $cat_kn_query[$index_one]->post_content,
            'media' => 'hold',
            'user'  => '',
            'class' => 'category-' . $cat_sufix_kn,
            'link'  => get_the_permalink($cat_kn_query[$index_one]->ID)
        ),
        4 => array(
            'id'    => $cat_jb_query[1]->ID,
            'title' => $cat_jb_query[1]->post_title,
            'text'  => $cat_jb_query[1]->post_content,
            'media' => 'hold',
            'user'  => '',
            'class' => 'category-jb', //get_category($cat_jb_query[1]->ID)->slug,
            'link'  => get_the_permalink($cat_jb_query[1]->ID)
        ),
        5 => array(
            'id'    => $cat_fb_query[0]->id,
            'title' => '',
            'text'  => $text_0,
            'media' => '',
            'user'  => '',
            'class' => 'category-fb',
            'link'  => $cat_fb_query[0]->link
        ),
        6 => array(
            'id'    => $cat_kn_query[$index_zero]->ID,
            'title' => $cat_kn_query[$index_zero]->post_title,
            'text'  => $cat_kn_query[$index_zero]->post_content,
            'media' => 'hold',
            'user'  => '',
            'class' => 'category-' . $cat_sufix_kn,
            'link'  => get_the_permalink($cat_kn_query[$index_zero]->ID)
        ),
        7 => array(
            'id'    => $cat_in_query[2]->id,
            'title' => '',
            'text'  => $cat_in_query[2]->description,
            'media' => $cat_in_query[2]->media_url,
            'user'  => '',
            'class' => 'category-in',
            'link'  => $cat_in_query[2]->link
        ),

        8 => array(
            'id'    => $cat_fb_query[1]->id,
            'title' => '',
            'text'  => $text_1,
            'media' => '',
            'user'  => '',
            'class' => 'category-fb',
            'link'  => $cat_fb_query[1]->link
        )
    );

    if (count($cat_jb_query) == 0) {
        require_once(get_template_directory() . '/templates/partials/_cat-net.php');
        $all_items = array();
    }

    $class_size = $dims[0];

    foreach ($all_items as $key => $item) {

        if ($key == 0) {
            $class_size = $dims[2];
        }

        if ($key == 1 || $key == 2 || $key == 7 || $key == 8) {
            $class_size = $dims[0];
        }

        if ($key == 3 || $key == 4) {
            $class_size = $dims[3];
        }

        if ($key == 5 || $key == 6) {
            $class_size = $dims[1];
        }

        if (!empty($item['media'])) {
            $style = 'style="background-image:url(' . $item['media'] . ')"';
        } else {
            $style = '';
        }

        if ($item['class'] == 'category-jb' || $item['class'] == 'category-know') {
            $style = 'style="background-image:url(' . wp_get_attachment_image_src(get_post_thumbnail_id($item['id']), $class_size)[0] . ')"';
        }

        if ($item['id']) {
            $item_post           = get_post($item['id']);
            $template_name       = str_replace('templates/', '', get_post_meta($item['id'], '_wp_post_template'));
            $template_name       = str_replace('.php', '', $template_name);
            $template_striped    = str_replace('-', '_', $template_name);
            $template_striped[0] = 'template_' . $template_striped[0] . '_';
            $post_name           = str_replace('-', '_', $item_post->post_name);

            if ($item['class'] == 'category-fb') {
                $template_striped[0] = 'facebook';
            }

            if ($item['class'] == 'category-tw') {
                $template_striped[0] = 'twitter';
            }

            if ($item['class'] == 'category-in') {
                $template_striped[0] = 'instagram';
            }

            echo '<a id="conteudo_' . $template_striped[0] . $post_name . '" href="' . $item['link'] . '" class="se-masonry ' . $item['class'] . ' category-' . get_the_category($item['id'])[0]->slug . ' ' . $class_size . ' ' . 'item-' . $key .'" ' . $style . '>';
            echo '<div class="se-item">';
            echo '<div class="bg-layer"></div>';
            echo '<div class="se-item-inner">';
            echo '<span class="se-icon"></span>';

            if (!empty($item['title'])) {
                echo '<h3 class="se-title">';

                if ($item['class'] == 'category-jb') {
                    echo '<span class="job-logo"></span>';
                }
                $childcat = get_the_category($item['id'])[0];

                if (cat_is_ancestor_of(38, $childcat)) {
                    $childcat_name = explode(' ', $childcat->cat_name);
                    $content = $item->post_content;
                    $se_item_title = $item['title'];
                    if (strlen($se_item_title) > 40) {
                        $se_item_title = mb_substr($se_item_title, 0, 40) . ' ...';
                    }
                    for ($i = 0; $i <= count($childcat_name); $i++) {
                        echo '<span class="cat-title part-' . $i . '">' . $childcat_name[$i] . '&nbsp;' . '</span>';
                    }
                    echo '<span class="last-part">' . $se_item_title . '</span>';
                } else {
                    echo '<span class="txt-title">' . $se_item_title . '</span>';
                }
                echo '</h3>';
            }

            if (!empty($item['text']) && $item['class'] != 'category-in' && $item['class'] != 'category-jb') {

                echo '<div class="se-text">';
                if ($item['user']) {
                    echo '<span class="se-user">' . $item['user'] . '</span>';
                }
                $text = $item['text'];
                $text = preg_replace('#<a.*?>(.*?)</a>#i', '\1', $text);
                $text = preg_replace('/<h[0-6][^>]*>([\s\S]*?)<\/h[0-6][^>]*>/', '', $text);
                if ($class_size == 'size_220-460' || $class_size == 'size_220-220') {
                    $length = 95;
                } else {
                    $length = 230;
                }
                if (strlen($text) > $length) {
                    $offset = ($length - 3) - strlen($text);
                    $text   = substr($text, 0, strrpos($text, ' ', $offset));
                    echo $text . ' ...';
                } else {
                    echo $text;
                }

                if ($item['class'] == 'category-know') {
                    echo '<span class="arrow-right"></span>';
                }
                echo '</div>';
            }
            echo '</div>';
            echo '</div>';
            echo '</a>';
        }
    }
}