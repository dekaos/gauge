<?php

$search_cat = array_filter(

    get_the_category(),

    function ($a) {
        return array_key_exists('term_id', $a) && $a->term_id == 38;
    }
);

if ($search_cat) {
    $category_class = 'category-trabalhos';
    get_post_class()[7];
} else {
    $category_class = '';
}

foreach($items->posts as $item) : ?>

    <?php $template_name    = str_replace('templates/', '', get_post_meta($item->ID, '_wp_post_template')); ?>
    <?php $template_name    = str_replace('.php', '', $template_name); ?>
    <?php $template_striped = str_replace('-', '_', $template_name); ?>
    <?php $post_name        = str_replace('-', '_', $item->post_name); ?>
    <?php $img              = wp_get_attachment_image_src(get_post_thumbnail_id($item->ID), array(220, 220)); ?>

    <a id="veja_tb_<?php echo $template_striped[0] . '_' . $post_name; ?>" class="item-link <?php echo $category_class . ' category-' . get_the_category()[0]->slug; ?>" href="<?php echo get_the_permalink($item->ID); ?>" style="background-image: url(<?php echo $img[0]; ?>);">
        <div class="item-content">
            <div class="item-bg-layer"></div>
            <h3 class="item-title">
                <span class="job-logo"></span>
                <?php

                $childcat = get_the_category()[0];

                if (cat_is_ancestor_of(38, $childcat)) :
                    $childcat_name = explode(' ', $childcat->cat_name);
                    $content = $item->post_content;
                    $item_title_txt = $item->post_title;
                    if (strlen($item_title_txt) > 25) {
                        $item_title_txt = mb_substr($item->post_title, 0, 25) . ' ...';
                    }
                    for ($i = 0; $i <= count($childcat_name); $i++) {
                        echo '<span class="cat-title part-' . $i . '">' . $childcat_name[$i] . '&nbsp;' . '</span>';
                    }
                    echo '<span class="last-part">' . $item_title_txt . '</span>';
                else :
                    echo $item->post_title;
                    ?>
                    <span class="arrow-right"></span>
                <?php endif; ?>
            </h3>
            <?php

            if ($content) {
                $content = preg_replace('/<h[0-6][^>]*>([\s\S]*?)<\/h[0-6][^>]*>/', '', $content);
                echo '<div class="excerpt">';
                echo '<div class="excerpt-inner">';
                if (strlen($content) > 50)
                {
                    $offset = (50 - 3) - strlen($content);
                    $content = substr($content, 0, strrpos($content, ' ', $offset));
                    echo $content . ' ...';
                } else {
                    echo $content;
                }
                echo '</div>';
                echo '<span class="arrow-right"></span>';
                echo '</div>';
            }
            ?>
        </div>
    </a>
<?php endforeach;