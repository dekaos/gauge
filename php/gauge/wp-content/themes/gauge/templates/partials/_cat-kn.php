<?php
/**
 * Created by WebStorm.
 * User: anderson
 * Date: 2/9/15
 * Time: 9:53 AM
 */

$args_kn = array(
    'posts_per_page'   => 9,
    'offset'           => 0,
    'category__in'     => array(17),
    'orderby'          => 'post_date',
    'order'            => 'DESC',
    'post_type'        => 'post',
    'post_status'      => 'publish',
    'suppress_filters' => true
);

$items_kn = get_posts($args_kn);

$class_size = $dims[0];

foreach ($items_kn as $key => $item) {
    if ($key == 0) {
        $class_size = $dims[2];
    }

    if ($key == 1 || $key == 2 || $key == 7 || $key == 8) {
        $class_size = $dims[0];
    }

    if ($key == 3 || $key == 4) {
        $class_size = $dims[3];
    }

    if ($key == 5 || $key == 6) {
        $class_size = $dims[1];
    }

    $style = 'style="background-image:url(' . wp_get_attachment_image_src(get_post_thumbnail_id($item->ID), $class_size)[0] . ')"';

    if ($class_size == 'size_220-460' || $class_size == 'size_220-220') {
        $length_txt = 95;
        $length_title = 30;
    } else {
        $length_txt = 230;
        $length_title = 70;
    }

    $title = $item->post_title;

    if (strlen($title) > $length_title) {
        $offset = ($length_title) - strlen($title);
        $title  = substr($title, 0, strrpos($title, ' ', $offset));
        $post_title = $title . '...';
    } else {
        $post_title = $title;
    }

    $item_post           = get_post($item->ID);
    $template_name       = str_replace('templates/', '', get_post_meta($item->ID, '_wp_post_template'));
    $template_name       = str_replace('.php', '', $template_name);
    $template_striped    = str_replace('-', '_', $template_name);
    $template_striped[0] = 'template_' . $template_striped[0] . '_';
    $post_name           = str_replace('-', '_', $item_post->post_name);

    echo '<a id="conteudo_' . $template_striped[0] . $post_name . '" href="' . get_the_permalink($item->ID) . '" class="se-masonry category-know category-' . get_the_category($item->ID)[0]->slug . ' ' . $class_size . ' ' . 'item-' . $key .'" ' . $style . '>';
    echo '<div class="se-item">';
    echo '<div class="bg-layer"></div>';
    echo '<div class="se-item-inner">';
    echo '<span class="se-icon"></span>';
    echo '<h3 class="se-title">';
    echo '<span class="txt-title">' . $post_title . '</span>';
    echo '</h3>';


    echo '<div class="se-text">';

    $text = $item->post_content;
    $text = preg_replace('#<a.*?>(.*?)</a>#i', '\1', $text);
    $text = preg_replace('/<h[0-6][^>]*>([\s\S]*?)<\/h[0-6][^>]*>/', '', $text);

    if (strlen($text) > $length_txt) {
        $offset = ($length_txt - 3) - strlen($text);
        $text   = substr($text, 0, strrpos($text, ' ', $offset));
        echo $text . ' ...';
    } else {
        echo $text;
    }

    echo '<span class="arrow-right"></span>';
    echo '</div>';
    echo '</div>';
    echo '</div>';
    echo '</a>';
}
