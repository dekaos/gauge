<?php
/**
 * Created by WebStorm.
 * User: anderson
 * Date: 12/30/14
 * Time: 1:50 PM
 */

$post_364 = get_post(364);
$doc      = new DOMDocument();
$doc->loadHTML($post_364->post_content);

?>
<div id="clients-wrapper">
    <div id="clients" class="container">
        <h2 class="clients-title">
            <?php echo $post_364->post_title; ?>
        </h2>

        <?php
        $items = $doc->getElementsByTagName('img');

        foreach ($items as $item) {
            $list[] = $item->getAttribute('src');
        }

        $counter  = 0;
        $counter2 = 0;

        echo '<div id="client-list" class="swiper-container">';
        echo '<div class="swiper-wrapper">';

        foreach (array_chunk($list, 6, true) as $items)
        {
            echo '<div class="swiper-slide">';

            foreach($items as $image_url)
            {
                echo $counter % 5 == 0 ? '<div class="item-logo no-border">' : '<div class="item-logo">';
                echo '<img src="' . $image_url . '">';
                echo '</div>';

                $counter++;

                /*if ($counter > 5) {
                    $counter = 0;
                }*/
            }
            echo '</div>';
            $counter2++;
        }

        echo '</div>';
        echo '<div class="mobile-pagination"></div>';
        echo '</div>'
        ?>
    </div>
    <div class="clearfix"></div>
</div>