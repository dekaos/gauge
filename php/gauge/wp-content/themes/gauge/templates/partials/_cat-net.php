<?php
/**
 * Created by WebStorm.
 * User: anderson
 * Date: 2/9/15
 * Time: 11:49 AM
 */

global $wpdb;

$cat_in_query = $wpdb->get_results("SELECT * FROM ytew5_cases WHERE type_case='in' AND published=1 ORDER BY id ASC LIMIT 5");
$cat_fb_query = $wpdb->get_results("SELECT * FROM ytew5_cases WHERE type_case='fb' AND published=1 ORDER BY id DESC LIMIT 5");
//$cat_tw_query = $wpdb->get_results("SELECT * FROM ytew5_cases WHERE type_case='tw' AND published=1 ORDER BY id DESC LIMIT 3");

if ($cat_fb_query[0]->text == '') {
    $text_fb_0 = $cat_fb_query[0]->description;
} else {
    $text_fb_0 = $cat_fb_query[0]->text;
}

if ($cat_fb_query[1]->text == '') {
    $text_fb_1 = $cat_fb_query[1]->description;
} else {
    $text_fb_1 = $cat_fb_query[1]->text;
}

if ($cat_fb_query[2]->text == '') {
    $text_fb_2 = $cat_fb_query[2]->description;
} else {
    $text_fb_2 = $cat_fb_query[2]->text;
}

if ($cat_fb_query[3]->text == '') {
    $text_fb_3 = $cat_fb_query[3]->description;
} else {
    $text_fb_3 = $cat_fb_query[3]->text;
}

$all_items = array(
    0 => array(
        'id'    => $cat_in_query[0]->id,
        'title' => '',
        'text'  => $cat_in_query[0]->description,
        'media' => $cat_in_query[0]->media_url,
        'user'  => '',
        'class' => 'category-in',
        'link'  => $cat_in_query[0]->link
    ),
    1 => array(
        'id'    => $cat_in_query[1]->id,
        'title' => '',
        'text'  => $cat_in_query[1]->description,
        'media' => $cat_in_query[1]->media_url,
        'user'  => '',
        'class' => 'category-in',
        'link'  => $cat_in_query[1]->link
    ),
    2 => array(
        'id'    => $cat_in_query[2]->id,
        'title' => '',
        'text'  => $cat_in_query[2]->description,
        'media' => $cat_in_query[2]->media_url,
        'user'  => '',
        'class' => 'category-in',
        'link'  => $cat_in_query[2]->link
    ),
    3 => array(
        'id'    => $cat_fb_query[0]->id,
        'title' => '',
        'text'  => $text_fb_0,
        'media' => '',
        'user'  => '',
        'class' => 'category-fb',
        'link'  => $cat_fb_query[0]->link
    ),
    4 => array(
        'id'    => $cat_fb_query[1]->id,
        'title' => '',
        'text'  => $text_fb_1,
        'media' => '',
        'user'  => '',
        'class' => 'category-fb',
        'link'  => $cat_fb_query[1]->link
    ),
    5 => array(
        'id'    => $cat_in_query[3]->id,
        'title' => '',
        'text'  => $cat_in_query[3]->description,
        'media' => $cat_in_query[3]->media_url,
        'user'  => '',
        'class' => 'category-in',
        'link'  => $cat_in_query[3]->link
    ),
    6 => array(
        'id'    => $cat_fb_query[2]->id,
        'title' => '',
        'text'  => $text_fb_2,
        'media' => '',
        'user'  => '',
        'class' => 'category-fb',
        'link'  => $cat_fb_query[2]->link
    ),
    7 => array(
        'id'    => $cat_fb_query[3]->id,
        'title' => '',
        'text'  => $text_fb_3,
        'media' => '',
        'user'  => '',
        'class' => 'category-fb',
        'link'  => $cat_fb_query[3]->link
    ),
    8 =>  array(
        'id'    => $cat_in_query[4]->id,
        'title' => '',
        'text'  => $cat_in_query[4]->description,
        'media' => $cat_in_query[4]->media_url,
        'user'  => '',
        'class' => 'category-in',
        'link'  => $cat_in_query[4]->link
    )
);

$class_size = $dims[0];

foreach ($all_items as $key => $item) {

    if ($key == 0) {
        $class_size = $dims[2];
    }

    if ($key == 1 || $key == 2 || $key == 7 || $key == 8) {
        $class_size = $dims[0];
    }

    if ($key == 3 || $key == 4) {
        $class_size = $dims[3];
    }

    if ($key == 5 || $key == 6) {
        $class_size = $dims[1];
    }

    if (!empty($item['media'])) {
        $style = 'style="background-image:url(' . $item['media'] . ')"';
    } else {
        $style = '';
    }

    if ($item['class'] == 'category-jb' || $item['class'] == 'category-know') {
        $style = 'style="background-image:url(' . wp_get_attachment_image_src(get_post_thumbnail_id($item['id']), $class_size)[0] . ')"';
    }

    if ($item['class'] == 'category-fb') {
        $template = 'facebook';
    }

    if ($item['class'] == 'category-tw') {
        $template = 'twitter';
    }

    if ($item['class'] == 'category-in') {
        $template = 'instagram';
    }

    echo '<a id="conteudo_' . $template . '" href="' . $item['link'] . '" class="se-masonry ' . $item['class'] . ' category-' . get_the_category($item['id'])[0]->slug . ' ' . $class_size . ' ' . 'item-' . $key .'" ' . $style . '>';
    echo '<div class="se-item">';
    echo '<div class="bg-layer"></div>';
    echo '<div class="se-item-inner">';
    echo '<span class="se-icon"></span>';

    if (!empty($item['title'])) {
        echo '<h3 class="se-title">';

        if ($item['class'] == 'category-jb') {
            echo '<span class="job-logo"></span>';
        }
        $childcat = get_the_category($item['id'])[0];

        if (cat_is_ancestor_of(38, $childcat)) {
            $childcat_name = explode(' ', $childcat->cat_name);
            $content = $item->post_content;
            for ($i = 0; $i <= count($childcat_name); $i++) {
                echo '<span class="cat-title part-' . $i . '">' . $childcat_name[$i] . '&nbsp;' . '</span>';
            }
            echo '<span class="last-part">' . $item['title'] . '</span>';
        } else {
            echo '<span class="txt-title">' . $item['title'] . '</span>';
        }
        echo '</h3>';
    }

    if (!empty($item['text']) && $item['class'] != 'category-in' && $item['class'] != 'category-jb') {

        echo '<div class="se-text">';
        if ($item['user']) {
            echo '<span class="se-user">' . $item['user'] . '</span>';
        }
        $text = $item['text'];
        $text = preg_replace('#<a.*?>(.*?)</a>#i', '\1', $text);
        $text = preg_replace('/<h[0-6][^>]*>([\s\S]*?)<\/h[0-6][^>]*>/', '', $text);

        if ($class_size == 'size_220-460' || $class_size == 'size_220-220') {
            $length = 95;
        } else {
            $length = 230;
        }

        if (strlen($text) > $length) {
            $offset = ($length - 3) - strlen($text);
            $text   = substr($text, 0, strrpos($text, ' ', $offset));
            echo $text . ' ...';
        } else {
            echo $text;
        }

        if ($item['class'] == 'category-know') {
            echo '<span class="arrow-right"></span>';
        }
        echo '</div>';
    }
    echo '</div>';
    echo '</div>';
    echo '</a>';
}
