<?php
/**
 * Created by WebStorm.
 * User: anderson
 * Date: 1/21/15
 * Time: 2:30 PM
 */

$categories = array(
    0 => array(
        'id'       => 38,
        'cat_name' => __('Cases', 'roots')
    ),
    /*1 => array(
        'id'       => 17,
        'cat_name' => __('News', 'roots')
    ),*/
    2 => array(
        'id'       => 'net',
        'cat_name' => __('Social networks', 'roots')
    ),
    3 => array(
        'id'       => 'all',
        'cat_name' => __('See all', 'roots')
    )
);

if (is_page(565)) {
    $cat_jb_id = 9;
}

if (is_page(559)) {
    $cat_jb_id = 6;
}

if (is_page(561)) {
    $cat_jb_id = 7;
}

if (is_page(563)) {
    $cat_jb_id = 8;
}

if (is_home()) {
    $cat_jb_id = 38;
}
?>

<header class="sp-modular-header container">
    <div class="col-md-12">
        <h2 class="se-modular-header-title pull-left">
            <?php _e('Related work and content', 'roots'); ?>
        </h2>
        <form id="se-list" name="se-categories" class="pull-right">
        <span class="legend">
            <?php _e('Show', 'roots'); ?>
        </span>
            <bottom data-toggle="dropdown" id="dropdown-case" type="text" class="btn btn-default dropdown-toggle pull-right"><?php _e('See all', 'roots'); ?></bottom>
            <ul class="dropdown-menu">
                <?php foreach ($categories as $category) : ?>
                    <li>
                        <input name="se-category" id="se-category-<?php echo $category['id']; ?>" value="<?php echo $category['id']; ?>" type="radio" <?php echo $category['id'] == 'all' ? 'checked' : ''; ?>>
                        <label for="se-category-<?php echo $category['id']; ?>"><?php echo $category['cat_name']; ?></label>
                    </li>
                <?php endforeach; ?>
            </ul>
            <input name="single-cat-id" id="single-cat-id" type="hidden" value="<?php echo $cat_jb_id; ?>">
        </form>
    </div>
</header>