<?php
/**
 * Created by WebStorm.
 * User: anderson
 * Date: 12/11/14
 * Time: 10:14 AM
 */

global $wpdb;

$id = sanitize_text_field($_POST['v_id']);

$vacancy = $wpdb->get_row('SELECT * FROM ytew5_vacancy WHERE id = ' . $id . ' AND published = 1', ARRAY_A);
$states  = $wpdb->get_results('SELECT * FROM ytew5_states');

?>
<div id="vacancy-wrapper">
    <div id="head-line">
        <h2>
            <?php _e('Work with us', 'roots'); ?>
        </h2>
        <a id="back" href="#"><?php _e('Back', 'roots'); ?></a>
    </div>
    <div id="vacancy">
        <header>
            <div id="vacancy-amount" class="pull-left">
            <span id="heavy">
                <?php echo $vacancy['amount']; ?>
            </span>
            <span id="label">
                <?php echo $vacancy['amount'] > 1 ? __('Vacancies', 'roots') : __('Vacancy', 'roots'); ?>
            </span>
            </div>
            <h3 id="vacancy-title">
                <?php echo $vacancy['vacancy']; ?>
                <span>
                <?php echo $vacancy['location']; ?>
            </span>
            </h3>
            <div class="clearfix"></div>
        </header>
        <div id="vacancy-content">
            <div id="vacancy-txt" class="col-md-6">
                <div id="vacancy-txt-inner">
                    <?php echo wpautop($vacancy['description']); ?>
                </div>
            </div>
            <div id="vacancy-col2" class="col-md-6">
                <h3><?php _e('Interested? Sign up', 'roots'); ?></h3>
                <form id="vacancy-form" class="form-validate" name="vacancy-form" method="post" enctype="multipart/form-data" novalidate>
                    <?php require_once ('_vacancy-form.php'); ?>
                    <div class="input-group">
                        <input name="nv-interest" value="<?php echo $vacancy['id']; ?>" type="hidden"/>
                        <input name="action" id="nv-action" value="have_vacancy" type="hidden">
                        <input type="submit" class="btn vacancy-submit btn-info" value="<?php _e('Send message', 'roots'); ?>">
                    </div>
                </form>
            </div>
        </div>
    </div>
</div>
<?php wp_reset_query(); ?>