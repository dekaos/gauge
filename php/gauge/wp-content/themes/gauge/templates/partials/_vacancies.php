<?php

$post_295 = get_post(295);

?>
<div id="vacancies-wrapper">
    <div id="head-line">
        <h2>
            <?php _e('Work with us', 'roots'); ?>
        </h2>
        <?php echo $post_295->post_content; ?>
    </div>
    <?php

        global $wpdb;

        $vacancies   = $wpdb->get_results('SELECT * FROM ytew5_vacancy WHERE published = 1 ORDER BY created_at ASC');
        $states      = $wpdb->get_results('SELECT * FROM ytew5_states');
        $departments = $wpdb->get_results('SELECT * FROM ytew5_vacancy_departments');
        $h2          = '<h2>' . __('Register in our talent pool :)', 'roots') . '</h2>';
    ?>
    <?php if (count($vacancies) > 0) : ?>

        <?php
        $h2 = '<h2>' . __('Can not find your post on our vacancies?', 'roots') .
            '<br>' . __('Then sign up to our talent pool :)', 'roots') . '</h2>';
        ?>
        <div id="vacancies">
            <h2>
                <?php _e('Check out our vacancies', 'roots'); ?>
            </h2>
            <?php foreach ($vacancies as $vacancy) : ?>
                <div class="vacancy-box <?php echo ($vacancy == end($vacancies)) ? ' last' : ''; ?>">
                    <div class="vacancy-box-inner">
                        <h3 class="vacancy-title">
                            <?php echo $vacancy->vacancy; ?>
                            <span class="vacancy-city">
                            <?php echo $vacancy->location; ?>
                        </span>
                        </h3>
                        <div class="vacancy-amount">
                            <?php echo $vacancy->amount; ?>
                            <span>
                            <?php echo ($vacancy->amount > 1 ? __('Vacancies', 'roots') : __('Vacancy', 'roots')); ?>
                        </span>
                        </div>
                        <a class="vacancy-link" href="#" data-id="<?php echo $vacancy->id; ?>">
                            <span></span>
                        </a>
                    </div>
                </div>
            <?php endforeach; ?>
            <div class="clearfix"></div>
        </div>
    <?php endif; ?>
    <div id="no-vacancy">
        <?php echo $h2; ?>
        <form id="no-vacancy-form" class="form-validate" name="no-vacancy-form" method="post" enctype="multipart/form-data" novalidate>
            <div class="col-md-6">
                <?php require_once ('_vacancy-form.php'); ?>
                <div class="input-group">
                    <input name="action" id="nv-action" value="no_vacancy" type="hidden">
                    <input type="submit" class="btn no-vacancy-submit btn-info" value="<?php _e('Send message', 'roots'); ?>">
                </div>
            </div>
        </form>
    </div>
</div>
<?php wp_reset_query(); ?>