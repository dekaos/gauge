<?php
    global $wpdb;
    $states = $wpdb->get_results('SELECT * FROM ytew5_states');
    $post_141 = get_post(141);
?>
<form id="research-contact" method="post" name="research-contact" novalidate>
    <div class="col-md-12">
        <h2>
            <?php _e('Research', 'roots'); ?>
        </h2>
        <div class="research-post">
            <p>
                <?php echo $post_141->post_content; ?>
            </p>
            <h3>
                <?php _e('Tell us about you', 'roots'); ?>
            </h3>
        </div>
        <div class="group-1">
            <div class="input-group">
                <label for="re-name">
                    <?php _e('Name', 'roots'); ?>
                </label>
                <input class="form-control" id="re-name" name="re-name" maxlength="70" type="text" required/>
            </div>
            <div class="input-group birth-field input-text pull-left">
                <label for="re-birth">
                    <?php _e('Birth date', 'roots'); ?>
                </label>
                <input class="form-control" id="re-birth" name="re-birth" maxlength="70" type="text" required>
            </div>
            <div class="input-group input-radio pull-left">
                <span>
                    <?php _e('Sex', 'roots'); ?>
                </span>
                <div class="clearfix"></div>
                <input id="re-sex-male" name="re-sex" class="radio-button" type="radio" value="m">
                <label for="re-sex-male">
                    <span class="radio-circle"><span></span></span>
                    <?php _e('Male', 'roots'); ?>
                </label>
                <input id="re-sex-female" name="re-sex" class="radio-button" type="radio" value="f">
                <label for="re-sex-female">
                    <span class="radio-circle"><span></span></span>
                    <?php _e('Female', 'roots'); ?>
                </label>
            </div>
            <div class="clearfix"></div>
            <div class="input-group select-state pull-left">
                <label for="re-state">
                    <?php _e('State', 'roots'); ?>
                </label>
                <div class="form-inline">
                    <div class="btn-group">
                        <input type="button" data-toggle="dropdown" id="re-state" name="v-state" class="btn btn-default dropdown-toggle form-control" required>
                        <ul class="dropdown-menu">
                            <?php foreach($states as $state) : ?>
                            <li>
                                <input type="radio" id="state-<?php echo $state->id; ?>" name="re-state" value="<?php echo $state->id; ?>">
                                <label for="state-<?php echo $state->id; ?>"><?php echo $state->uf; ?></label>
                            </li>
                            <?php endforeach; ?>
                        </ul>
                    </div>
                </div>
            </div>
            <div class="input-group select-city pull-left">
                <label for="re-city">
                    <?php _e('City', 'roots'); ?>
                </label>
                <div class="form-inline">
                    <div class="btn-group">
                        <input data-toggle="dropdown" id="re-city" type="text" name="v-city" class="btn btn-default dropdown-toggle" required disabled>
                        <ul id="city-groups" class="dropdown-menu">

                        </ul>
                    </div>
                </div>
            </div>
            <div class="input-group">
                <label for="re-email">
                    <?php _e('E-mail', 'roots'); ?>
                </label>
                <input id="re-email" name="re-email" class="form-control" maxlength="70" required type="email">
            </div>
            <div class="input-group">
                <label for="re-phone">
                    <?php _e('DDD + Phone', 'roots'); ?>
                </label>
                <input id="re-phone" name="re-phone" class="form-control" maxlength="70" required type="text">
            </div>
            <div class="input-group">
                <label for="re-occupation">
                    <?php _e('Occupation', 'roots'); ?>
                </label>
                <input class="form-control" name="re-occupation" id="re-occupation" maxlength="70" required type="text">
            </div>
        </div>
    </div>
    <div class="clearfix"></div>
    <?php $post_143 = get_post(143); ?>
    <div class="col-md-12">
        <div class="research-post">
            <h3>
                <?php echo $post_143->post_title; ?>
            </h3>
            <p>
                <?php echo $post_143->post_content; ?>
            </p>
        </div>
        <div class="group-2 pull-left">
            <div class="input-group">
                <div class="btn-group">
                    <input data-toggle="dropdown" id="re-tv" type="button" name="v-tv" class="btn btn-default dropdown-toggle" required>
                    <ul class="dropdown-menu">
                        <li>
                            <input type="radio" id="tv-0" name="re-tv" value="0">
                            <label for="tv-0">0</label>
                        </li>
                        <li>
                            <input type="radio" id="tv-1" name="re-tv" value="1">
                            <label for="tv-1">1</label>
                        </li>
                        <li>
                            <input type="radio" id="tv-2" name="re-tv" value="2">
                            <label for="tv-2">2</label>
                        </li>
                        <li>
                            <input type="radio" id="tv-3" name="re-tv" value="3">
                            <label for="tv-3">3</label>
                        </li>
                        <li>
                            <input type="radio" id="tv-4" name="re-tv" value="4">
                            <label for="tv-4">4</label>
                        </li>
                        <li>
                            <input type="radio" id="tv-4" name="re-tv" value="4">
                            <label for="tv-4">4</label>
                        </li>
                        <li>
                            <input type="radio" id="tv-more-4" name="re-tv" value="+ de 4">
                            <label for="tv-more-4">+ de 4</label>
                        </li>
                    </ul>
                </div>
                <label for="re-tv">
                    <?php _e('Color TVs', 'roots'); ?>
                </label>
            </div>
            <div class="input-group">
                <div class="btn-group">
                    <input data-toggle="dropdown" id="re-radio" name="v-radio" type="button" class="btn btn-default dropdown-toggle" required>
                    <ul class="dropdown-menu">
                        <li>
                            <input id="ra-0" name="re-radio" value="0" type="radio">
                            <label for="ra-0">0</label>
                        </li>
                        <li>
                            <input id="ra-1" name="re-radio" value="1" type="radio">
                            <label for="ra-1">1</label>
                        </li>
                        <li>
                            <input id="ra-2" name="re-radio" value="2" type="radio">
                            <label for="ra-2">2</label>
                        </li>
                        <li>
                            <input id="ra-3" name="re-radio" value="3" type="radio">
                            <label for="ra-3">3</label>
                        </li>
                        <li>
                            <input id="ra-4" name="re-radio" value="4" type="radio">
                            <label for="ra-4">4</label>
                        </li>
                        <li>
                            <input id="ra-more-4" name="re-radio" value="+ de 4" type="radio">
                            <label for="ra-more-4">+ de 4</label>
                        </li>
                    </ul>
                </div>
                <label for="re-radio">
                    <?php _e('Radios', 'roots'); ?>
                </label>
            </div>
            <div class="input-group">
                <div class="btn-group">
                    <input data-toggle="dropdown" id="re-bathroom" name="v-bathroom" type="button" class="btn btn-default dropdown-toggle" required>
                    <ul class="dropdown-menu">
                        <li>
                            <input id="ba-0" name="re-bathroom" value="0" type="radio">
                            <label for="ba-0">0</label>
                        </li>
                        <li>
                            <input id="ba-1" name="re-bathroom" value="1" type="radio">
                            <label for="ba-1">1</label>
                        </li>
                        <li>
                            <input id="ba-2" name="re-bathroom" value="2" type="radio">
                            <label for="ba-2">2</label>
                        </li>
                        <li>
                            <input id="ba-3" name="re-bathroom" value="3" type="radio">
                            <label for="ba-3">3</label>
                        </li>
                        <li>
                            <input id="ba-4" name="re-bathroom" value="4" type="radio">
                            <label for="ba-4">4</label>
                        </li>
                        <li>
                            <input id="ba-more-4" name="re-bathroom" value="+ de 4" type="radio">
                            <label for="ba-more-4">+ de 4</label>
                        </li>
                    </ul>
                </div>
                <label for="re-bathroom">
                    <?php _e('Bathrooms', 'roots'); ?>
                </label>
            </div>
            <div class="input-group">
                <div class="btn-group">
                    <input data-toggle="dropdown" id="re-car" type="button" name="v-car" class="btn btn-default dropdown-toggle" required>
                    <ul class="dropdown-menu">
                        <li>
                            <input id="ca-0" name="re-car" value="0" type="radio">
                            <label for="ca-0">0</label>
                        </li>
                        <li>
                            <input id="ca-1" name="re-car" value="1" type="radio">
                            <label for="ca-1">1</label>
                        </li>
                        <li>
                            <input id="ca-2" name="re-car" value="2" type="radio">
                            <label for="ca-2">2</label>
                        </li>
                        <li>
                            <input id="ca-3" name="re-car" value="3" type="radio">
                            <label for="ca-3">3</label>
                        </li>
                        <li>
                            <input id="ca-4" name="re-car" value="4" type="radio">
                            <label for="ca-4">4</label>
                        </li>
                        <li>
                            <input id="ca-more-4" name="re-car" value="+ de 4" type="radio">
                            <label for="ca-more-4">+ de 4</label>
                        </li>
                    </ul>
                </div>
                <label for="re-car">
                    <?php _e('Cars', 'roots'); ?>
                </label>
            </div>
            <div class="input-group">
                <div class="btn-group">
                    <input data-toggle="dropdown" id="re-employer" type="button" name="v-employer" class="btn btn-default dropdown-toggle" required>
                    <ul class="dropdown-menu">
                        <li>
                            <input id="em-0" value="0" name="re-employer" type="radio">
                            <label for="em-0">0</label>
                        </li>
                        <li>
                            <input id="em-1" value="1" name="re-employer" type="radio">
                            <label for="em-1">1</label>
                        </li>
                        <li>
                            <input id="em-2" value="2" name="re-employer" type="radio">
                            <label for="em-2">2</label>
                        </li>
                        <li>
                            <input id="em-3" value="3" name="re-employer" type="radio">
                            <label for="em-3">3</label>
                        </li>
                        <li>
                            <input id="em-4" value="4" name="re-employer" type="radio">
                            <label for="em-4">4</label>
                        </li>
                        <li>
                            <input id="em-more-4" value="+ de 4" name="re-employer" type="radio">
                            <label for="em-more-4">+ de 4</label>
                        </li>
                    </ul>
                </div>
                <label for="re-employer">
                    <?php _e('Salaried employees', 'roots'); ?>
                </label>
            </div>
        </div>
        <div class="group-3 pull-left">
            <div class="input-group">
                <div class="btn-group">
                    <input data-toggle="dropdown" id="re-washing-machine" type="button" name="v-washing" class="btn btn-default dropdown-toggle" required>
                    <ul class="dropdown-menu">
                        <li>
                            <input id="wm-0" value="0" name="re-washing-machine" type="radio">
                            <label for="wm-0">0</label>
                        </li>
                        <li>
                            <input id="wm-1" value="1" name="re-washing-machine" type="radio">
                            <label for="wm-1">1</label>
                        </li>
                        <li>
                            <input id="wm-2" value="2" name="re-washing-machine" type="radio">
                            <label for="wm-2">2</label>
                        </li>
                        <li>
                            <input id="wm-3" value="3" name="re-washing-machine" type="radio">
                            <label for="wm-3">3</label>
                        </li>
                        <li>
                            <input id="wm-4" value="4" name="re-washing-machine" type="radio">
                            <label for="wm-4">4</label>
                        </li>
                        <li>
                            <input id="wm-more-4" value="+ de 4" name="re-washing-machine" type="radio">
                            <label for="wm-more-4">+ de 4</label>
                        </li>
                    </ul>
                </div>
                <label for="re-washing-machine">
                    <?php _e('Washing Machines', 'roots'); ?>
                </label>
            </div>
            <div class="input-group">
                <div class="btn-group">
                    <input data-toggle="dropdown" id="re-videos" type="button" name="v-videos" class="btn btn-default dropdown-toggle" required>
                    <ul class="dropdown-menu">
                        <li>
                            <input id="vi-0" value="0" name="re-videos" type="radio">
                            <label for="vi-0">0</label>
                        </li>
                        <li>
                            <input id="vi-1" value="1" name="re-videos" type="radio">
                            <label for="vi-1">1</label>
                        </li>
                        <li>
                            <input id="vi-2" value="2" name="re-videos" type="radio">
                            <label for="vi-2">2</label>
                        </li>
                        <li>
                            <input id="vi-3" value="3" name="re-videos" type="radio">
                            <label for="vi-3">3</label>
                        </li>
                        <li>
                            <input id="vi-4" value="4" name="re-videos" type="radio">
                            <label for="vi-4">4</label>
                        </li>
                        <li>
                            <input id="vi-more-4" value="+ de 4" name="re-videos" type="radio">
                            <label for="vi-more-4">+ de 4</label>
                        </li>
                    </ul>
                </div>
                <label for="re-videos">
                    <?php _e('Videocassettes or DVDs', 'roots'); ?>
                </label>
            </div>
            <div class="input-group">
                <div class="btn-group">
                    <input data-toggle="dropdown" id="re-refrigerator" name="v-refrigerator" type="button" class="btn btn-default dropdown-toggle" required>
                    <ul class="dropdown-menu">
                        <li>
                            <input id="re-0" value="0" name="re-refrigerator" type="radio">
                            <label for="re-0">0</label>
                        </li>
                        <li>
                            <input id="re-1" value="1" name="re-refrigerator" type="radio">
                            <label for="re-1">1</label>
                        </li>
                        <li>
                            <input id="re-2" value="2" name="re-refrigerator" type="radio">
                            <label for="re-2">2</label>
                        </li>
                        <li>
                            <input id="re-3" value="3" name="re-refrigerator" type="radio">
                            <label for="re-3">3</label>
                        </li>
                        <li>
                            <input id="re-4" value="4" name="re-refrigerator" type="radio">
                            <label for="re-4">4</label>
                        </li>
                        <li>
                            <input id="re-more-4" value="+ de 4" name="re-refrigerator" type="radio">
                            <label for="re-more-4">+ de 4</label>
                        </li>
                    </ul>
                </div>
                <label for="re-refrigerator">
                    <?php _e('Refrigerators', 'roots'); ?>
                </label>
            </div>
            <div class="input-group">
                <div class="btn-group">
                    <input data-toggle="dropdown" id="re-freezer" type="button" name="v-freezer" class="btn btn-default dropdown-toggle" required>
                    <ul class="dropdown-menu">
                        <li>
                            <input id="fr-0" value="0" name="re-freezer" type="radio">
                            <label for="fr-0">0</label>
                        </li>
                        <li>
                            <input id="fr-1" value="1" name="re-freezer" type="radio">
                            <label for="fr-1">1</label>
                        </li>
                        <li>
                            <input id="fr-2" value="2" name="re-freezer" type="radio">
                            <label for="fr-2">2</label>
                        </li>
                        <li>
                            <input id="fr-3" value="3" name="re-freezer" type="radio">
                            <label for="fr-3">3</label>
                        </li>
                        <li>
                            <input id="fr-4" value="4" name="re-freezer" type="radio">
                            <label for="fr-4">4</label>
                        </li>
                        <li>
                            <input id="fr-more-4" value="+ de 4" name="re-freezer" type="radio">
                            <label for="fr-more-4">+ de 4</label>
                        </li>
                    </ul>
                </div>
                <label for="re-freezer">
                    <?php _e('Freezers', 'roots'); ?>
                </label>
            </div>
        </div>
        <div class="clearfix"></div>
        <div class="group-4">
            <div class="input-group">
                <label for="re-education">
                    <?php _e('Education of the family head', 'roots'); ?>
                </label>
                <div class="btn-group">
                    <input data-toggle="dropdown" id="re-education" type="button" name="v-education" class="btn btn-default dropdown-toggle" required>
                    <ul class="dropdown-menu">
                        <li>
                            <input id="ed-0" value="Analfabeto" name="re-education" type="radio">
                            <label for="ed-0"><?php _e('Unlettered', 'roots'); ?></label>
                        </li>
                        <li>
                            <input id="ed-1" value="até 3º série 1º grau" name="re-education" type="radio">
                            <label for="ed-1">Até 3º série 1º grau</label>
                        </li>
                        <li>
                            <input id="ed-2" value="até 4º série 1º grau" name="re-education" type="radio">
                            <label for="ed-2">Até 4º série 1º grau</label>
                        </li>
                        <li>
                            <input id="ed-3" value="1º grau completo" name="re-education" type="radio">
                            <label for="ed-3">1º grau completo</label>
                        </li>
                        <li>
                            <input id="ed-4" value="2º grau completo" name="re-education" type="radio">
                            <label for="ed-4">2º grau completo</label>
                        </li>
                        <li>
                            <input id="ed-more-4" value="Superior completo" name="re-education" type="radio">
                            <label for="ed-more-4">Superior completo</label>
                        </li>
                    </ul>
                </div>
            </div>
        </div>
        <div class="group-5">
            <div class="input-group">
                <input name="action" value="research-contact" type="hidden"/>
                <input class="form-control research-submit btn btn-info" value="<?php _e('Send message', 'roots'); ?>" type="submit"/>
            </div>
        </div>
    </div>
</form>