<?php
/**
 * Created by WebStorm.
 * User: anderson
 * Date: 2/9/15
 * Time: 9:22 AM
 */


$args_jb = array(
    'posts_per_page'   => 9,
    'offset'           => 0,
    'category__in'     => array(38),
    'orderby'          => 'post_date',
    'order'            => 'DESC',
    'post_type'        => 'post',
    'post_status'      => 'publish',
    'suppress_filters' => true
);

$items_jb = get_posts($args_jb);

$class_size = $dims[0];

foreach ($items_jb as $key => $item) {

    if ($key == 0) {
        $class_size = $dims[2];
    }

    if ($key == 1 || $key == 2 || $key == 7 || $key == 8) {
        $class_size = $dims[0];
    }

    if ($key == 3 || $key == 4) {
        $class_size = $dims[3];
    }

    if ($key == 5 || $key == 6) {
        $class_size = $dims[1];
    }

    $item_post           = get_post($item->ID);
    $template_name       = str_replace('templates/', '', get_post_meta($item->ID, '_wp_post_template'));
    $template_name       = str_replace('.php', '', $template_name);
    $template_striped    = str_replace('-', '_', $template_name);
    $template_striped[0] = 'template_' . $template_striped[0] . '_';
    $post_name           = str_replace('-', '_', $item_post->post_name);

    $style = 'style="background-image:url(' . wp_get_attachment_image_src(get_post_thumbnail_id($item->ID), $class_size)[0] . ')"';

    echo '<a id="conteudo_' . $template_striped[0] . $post_name . '" href="' . get_the_permalink($item->ID) . '" class="se-masonry category-jb category-' . get_the_category($item->ID)[0]->slug . ' ' . $class_size . ' ' . 'item-' . $key .'" ' . $style . '>';
    echo '<div class="se-item">';
    echo '<div class="bg-layer"></div>';
    echo '<div class="se-item-inner">';
    echo '<span class="se-icon"></span>';
    echo '<h3 class="se-title">';
    echo '<span class="job-logo"></span>';
    $childcat = get_the_category($item->ID)[0];

    $childcat_name = explode(' ', $childcat->cat_name);
    $content = $item->post_content;

    for ($i = 0; $i <= count($childcat_name); $i++) {
        echo '<span class="cat-title part-' . $i . '">' . $childcat_name[$i] . '&nbsp;' . '</span>';
    }

    $se_item_title = $item->post_title;
    if (strlen($se_item_title) > 40) {
        $se_item_title = mb_substr($se_item_title, 0, 40) . ' ...';
    }

    echo '<span class="last-part">' . $se_item_title . '</span>';
    echo '</h3>';
    echo '</div>';
    echo '</div>';
    echo '</a>';
}
