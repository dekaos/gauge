<?php
/**
 * Created by WebStorm.
 * User: anderson
 * Date: 12/31/14
 * Time: 4:40 PM
 */

$dims = array(
    'size_220-220',
    'size_220-460',
    'size_460-460',
    'size_460-220'
);

$counter = 0;
$size    = $class = $dims[0];
if (is_page_template('page-knowledge.php') || isset($_GET['page_tmp']) && $_GET['page_tmp'] == 'knowledge') {
    $page_name = 'knowledge';
} else if (is_page_template('page-work.php') || isset($_GET['page_tmp']) && $_GET['page_tmp'] == 'work') {
    $page_name = 'work';
} else {
    $page_name = '';
}

?>

<?php query_posts($args); ?>

<?php while(have_posts()) : ?>

    <?php the_post(); ?>

    <?php

    if ($parent_cat == '38') {
        if ($counter == 0)
            $size = $class = $dims[2];

        if ($counter == 1
            || $counter == 2
            || $counter == 7
            || $counter == 11
            || $counter == 12)
            $size = $class = $dims[0];

        if ($counter == 4 || $counter == 5)
            $size = $class = $dims[1];

        if ($counter == 6)
            $size = $class = $dims[3];

        if ($counter == 3)
            $size = $class = $dims[3];

        if ( $counter == 9)
            $size = $class = $dims[3];

        if ( $counter == 10)
            $size = $class = $dims[2];
    } else {
        if ($counter == 0)
            $size = $class = $dims[2];

        if ($counter == 1
            || $counter == 2
            || $counter == 7)
            $size = $class = $dims[0];

        if ($counter == 5)
            $size = $class = $dims[1];

        if ($counter == 6)
            $size = $class = $dims[3];

        if ($counter == 3)
            $size = $class = $dims[1];

        if ($counter == 4 || $counter == 9)
            $size = $class = $dims[0];
    }
    ?>
    <?php $img = wp_get_attachment_image_src(get_post_thumbnail_id($post->ID), $size); ?>
    <?php $template_name    = str_replace('templates/', '', get_post_meta($post->ID, '_wp_post_template')); ?>
    <?php $template_name    = str_replace('.php', '', $template_name); ?>
    <?php $template_striped = str_replace('-', '_', $template_name); ?>
    <?php $post_name        = str_replace('-', '_', $post->post_name); ?>
    <article class="post-item-wrapper <?php echo (get_post_class()[7] == 'category-trabalhos' ? get_post_class()[6] : get_post_class()[7]) . ' ' . $class; echo ($counter == 5 && $page_name == 'knowledge' ? ' quadratic' : $counter == 4 && $page_name == 'work' ? ' quadratic' : ''); ?>" style="background: url(<?php echo $img[0]; ?>) no-repeat center center">
        <div class="item-bg-layer"></div>
        <a id="conteudo_template_<?php echo $template_striped[0] . '_' . $post_name; ?>" class="post-link pull-left" href="<?php echo get_the_permalink($post->ID); ?>">
            <div class="post-item-content">
                <div class="post-item-inner">
                    <h3 class="post-item-title">
                        <span class="job-logo"></span>
                        <?php

                        $childcat = get_the_category()[0];

                        if (cat_is_ancestor_of(38, $childcat)) :

                            $childcat_name = explode(' ', $childcat->cat_name);
                            $content = get_the_content($post->ID);

                            for ($i = 0; $i <= count($childcat_name); $i++) {
                                echo '<span class="cat-title part-' . $i . '">' . $childcat_name[$i] . '&nbsp;' . '</span>';
                            }
                            $item_title = get_the_title();
                            if ($class == 'size_460-220' || $class == 'size_220-460' || $class == 'size_220-220' && strlen($item_title) > 34) {
                                $item_title = mb_substr($item_title, 0, 34) . ' ...';
                            }
                            echo '<span class="cat-title last-part">' . $item_title . '</span>';
                        else :
                            echo the_title();
                        ?>
                        <span class="arrow-right"></span>
                        <?php endif; ?>
                    </h3>
                    <?php

                    if ($content) {
                        echo '<div class="excerpt">';
                        echo '<div class="excerpt-inner">';
                        $max_length = 220;
                        if ($size == 'size_220-220' || $size == 'size_220-460') {
                            $max_length = 60;
                        } else {
                            $max_length = 200;
                        }

                        if (strlen($content) > $max_length)
                        {
                            $offset = ($max_length - 3) - strlen($content);
                            $content = substr($content, 0, strrpos($content, ' ', $offset));
                            echo $content . ' ...';
                        } else {
                            echo $content;
                        }
                        echo '</div>';
                        echo '<span class="arrow-right"></span>';
                        echo '</div>';
                    }
                    ?>
                </div>
            </div>
            <div class="clearfix"></div>
        </a>
    </article>
    <?php $counter++; ?>
<?php endwhile; ?>
<?php wp_reset_query(); ?>