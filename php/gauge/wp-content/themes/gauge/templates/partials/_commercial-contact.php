<div class="col-md-6">
    <form id="commercial-form" method="post" novalidate>
        <h2>
            <?php _e('Commercial', 'roots'); ?>
        </h2>
        <h3>
            <?php _e('Tell us about you', 'roots'); ?>
        </h3>
        <div class="input-group">
            <label for="cm-form-name">
                <?php _e('Name', 'roots'); ?>
            </label>
            <input class="form-control" id="cm-form-name" name="cm-form-name" type="text" maxlength="70" required/>
        </div>
        <div class="input-group">
            <label for="cm-form-email">
                <?php _e('E-mail', 'roots'); ?>
            </label>
            <input class="form-control" id="cm-form-email" name="cm-form-email" type="email" maxlength="70" required/>
        </div>
        <div class="input-group">
            <label for="cm-form-phone">
                <?php _e('DDD + Phone', 'roots'); ?>
            </label>
            <input class="form-control" id="cm-form-phone" name="cm-form-phone" type="text" maxlength="70" required/>
        </div>
        <h3>
            <?php _e('Tell us about your project', 'roots'); ?>
        </h3>
        <div class="input-group">
            <label for="cm-form-message">
                <?php _e('Message', 'roots'); ?>
            </label>
            <textarea class="form-control" required name="cm-form-message" id="cm-form-message" maxlength="5000" cols="30" rows="10"></textarea>
        </div>
        <div class="input-group">
            <input name="action" value="commercial-contact" type="hidden"/>
            <input class="form-control comercial-submit btn btn-info" name="contato_comercial_enviar_sucesso" value="<?php _e('Send message', 'roots'); ?>" type="submit">
        </div>
    </form>
</div>
<div class="col-md-6">
    <?php // from 'contato' plugin ?>
    <?php if(function_exists(get_office_phone_and_email())) {
        get_office_phone_and_email();
    } ?>
</div>