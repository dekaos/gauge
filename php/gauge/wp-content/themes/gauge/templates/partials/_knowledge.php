<?php
/**
 * Created by WebStorm.
 * User: anderson
 * Date: 12/2/14
 * Time: 3:35 PM
 */

global $post;

if (isset($_GET['cat']) && $_GET['cat'] != 'all') {
    $category = sanitize_text_field($_GET['cat']);
} else {
    $category = array('17');
}

if (!empty($_GET['item_offset'])) {
    $offset = sanitize_text_field($_GET['item_offset']);
} else {
    $offset = 0;
}

$parent_cat = '17';

$args = array(
    'category__in' => $category,
    'offset'       => $offset,
    'type'         => 'post',
    'post_status'  => 'published',
    'order'        => 'DESC',
    'paged'        => ( get_query_var('paged') ? get_query_var('paged') : 1 )
);

require_once(get_template_directory() . '/templates/partials/_modular-content.php');

