<?php

$args = array(
    'posts_per_page' => '-1',
    'category'       => '14',
    'orderby'        => 'title',
    'order'          => 'ASC',
    'post__not_in'   => array(91),
    'post_status'    => 'publish',
);

$posts = get_posts($args);

$correct_order = array(
    0 => array(
        'ID'      => $posts[4]->ID,
        'title'   => $posts[4]->post_title,
        'excerpt' => $posts[4]->post_excerpt,
        'content' => $posts[4]->post_content
    ),
    1 => array(
        'ID'      => $posts[5]->ID,
        'title'   => $posts[5]->post_title,
        'excerpt' => $posts[5]->post_excerpt,
        'content' => $posts[5]->post_content
    ),
    2 => array(
        'ID'      => $posts[0]->ID,
        'title'   => $posts[0]->post_title,
        'excerpt' => $posts[0]->post_excerpt,
        'content' => $posts[0]->post_content
    ),
    3 => array(
        'ID'      => $posts[1]->ID,
        'title'   => $posts[1]->post_title,
        'excerpt' => $posts[1]->post_excerpt,
        'content' => $posts[1]->post_content
    ),
    4 => array(
        'ID'      => $posts[7]->ID,
        'title'   => $posts[7]->post_title,
        'excerpt' => $posts[7]->post_excerpt,
        'content' => $posts[7]->post_content
    ),
    5 => array(
        'ID'      => $posts[3]->ID,
        'title'   => $posts[3]->post_title,
        'excerpt' => $posts[3]->post_excerpt,
        'content' => $posts[3]->post_content
    ),
    6 => array(
        'ID'      => $posts[2]->ID,
        'title'   => $posts[2]->post_title,
        'excerpt' => $posts[2]->post_excerpt,
        'content' => $posts[2]->post_content
    ),
    7 => array(
        'ID'      => $posts[6]->ID,
        'title'   => $posts[6]->post_title,
        'excerpt' => $posts[6]->post_excerpt,
        'content' => $posts[6]->post_content
    )
);

$specialty_id = $_POST['specialty_id'];
$current_id   = $_POST['current_id'];
$specialty    = get_post($specialty_id);

$degs = array (
    0 => '90',
    1 => '45',
    2 => '0',
    3 => '-45',
    4 => '-90',
    5 => '-135',
    6 => '-180',
    7 => '-225'
);

$reverse = array (
    0 => '-90',
    1 => '-45',
    2 => '0',
    3 => '45',
    4 => '90',
    5 => '135',
    6 => '180',
    7 => '225'
);

if (isset($_POST['current_screen']) && $_POST['current_screen'] == 'mobile') {
    $mobile = '-mobile';
} else {
    $mobile = '';
}

?>

<div class="lg-content-wrapper<?php echo $mobile; ?>">
    <div class="lg-content<?php echo $mobile; ?>">
        <span class="lg-back<?php echo $mobile; ?>">
            <?php _e('View specialties', 'roots'); ?>
        </span>
        <span class="lg-close"></span>
        <?php if (!empty($mobile)) : ?>
            <div class="lg-mobile">
                <div class="m-title">
                    <h2 class="m">
                        <?php echo $specialty->post_title; ?>
                    </h2>
                </div>
                <div class="m-content">
                    <?php echo $specialty->post_excerpt; ?>
                </div>
            </div>
        <?php endif ?>
        <div class="lg-content-outer">
            <div class="lg-content-inner">
                <?php if (empty($mobile)) : ?>
                <div class="left-box-outer">
                    <div class="left-box-wrapper">
                        <div class="left-content">
                            <!--<div class="left-post-content">
                                <h2 class="left-content-title">
                                    <?php //echo $specialty->post_title; ?>
                                    <span></span>
                                </h2>
                                <span class="left-excerpt">
                                    <?php //echo $specialty->post_excerpt; ?>
                                </span>
                            </div>-->
                        </div>
                        <div class="left-box circle">
                            <?php foreach ($correct_order as $key => $item) : ?>
                                <span class="icon-circle post-<?php echo $item['ID']; ?><?php echo $item['ID'] == $specialty_id ? ' active' : ''; ?>">
                                <span class="icon-bg-large"></span>
                                <span data-reverse="<?php echo $reverse[$key]; ?>" data-deg="<?php echo $degs[$key]; ?>" class="get-sp icon-bg" data-id="<?php echo $item['ID']; ?>"></span>
                                    <span class="tooltip">
                                        <span class="tooltip-inner">
                                            <?php echo $item['title']; ?>
                                        </span>
                                    </span>
                                </span>
                            <?php endforeach; ?>
                        </div>
                        <span class="point-top"></span>
                    </div>
                    <span class="m-left"></span>
                </div>
                <?php endif; ?>
                <div class="middle-box-wrapper">
                    <div class="midle-box pull-left">
                        <div class="middle-post-content">
                            <?php if (empty($mobile)) : ?>
                            <h2 id="<?php echo str_replace('-', '_', $specialty->post_name); ?>">
                                <span class="lg-post-title-h2">
                                     <?php echo $specialty->post_title; ?>
                                </span>
                                <span class="cat-name">
                                     <?php echo get_cat_name(55) ?>
                                </span>
                            </h2>
                            <?php endif; ?>
                            <div class="sp-links">
                                <?php
                                    $elm    = new DOMDocument();
                                    $elm->loadHTML(get_post($specialty)->post_content);
                                    $items   = $elm->getElementsByTagName('a');
                                    $counter = 0;

                                    echo '<ul>';

                                    foreach ($items as $item) {
                                        $data_id = custom_url_to_postid($item->getAttribute('href'));
                                        if ($current_id == $data_id) {
                                            $class = 'active';
                                        } else {
                                            $class = '';
                                        }
                                        echo '<li data-id="' . $data_id . '" class="links-list ' . $class .  '"><span class="m-left"></span>';
                                        echo '<a href="' . $item->getAttribute('href') . '">' . mb_convert_encoding($item->nodeValue, 'iso-8859-1') . '</a>';
                                        echo '</li>';
                                    }

                                    echo '</ul>';
                                ?>
                            </div>
                        </div>
                    </div>
                </div>
                <div class="right-box-wrapper">
                    <div class="right-box  pull-left">
                        <div class="right-box-content">

                        </div>
                    </div>
                </div>
            </div>
        </div>
        <div class="cleafifx"></div>
    </div>
</div>

<?php wp_reset_query(); ?>