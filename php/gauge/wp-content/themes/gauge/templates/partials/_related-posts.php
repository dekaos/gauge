<?php
//wp_reset_query();
    $catID  = get_the_category($post->ID);
    $parent = get_category($catID[0]->category_parent);

    if ($parent->slug == 'trabalhos') {
        $max_posts = 4;
        $jobs      = true;
    } else {
        $max_posts = 3;
        $jobs      = false;
    }
    $cat     = $catID[0]->cat_ID;
    $query   = array('category__in' => $cat, 'posts_per_page' => $max_posts, 'post__not_in' => array($post->ID));
    $items   = new WP_Query($query);
    $results = count($items->posts);
?>
<?php if ($results > 0) : ?>
<div class="related-posts container">
    <div class="related-posts-inner">
        <h2 class="pull-left">
            <?php _e('See also', 'roots'); ?>
        </h2>
        <?php

        if ($jobs == true) : ?>
            <a class="return-to-jobs pull-right" href="<?php echo get_permalink(359); ?>">
                <?php _e('See all jobs', 'roots'); ?>
            </a>
        <?php endif; ?>

        <div class="clearfix"></div>
        <?php
            require_once(get_template_directory() . '/templates/partials/_items.php');
        if ($jobs == true) : ?>
            <a class="return-to-jobs m-320 pull-right" href="<?php echo get_permalink(359); ?>">
                <?php _e('See all jobs', 'roots'); ?>
            </a>
        <?php endif; ?>
    </div>
</div>
<?php endif; ?>