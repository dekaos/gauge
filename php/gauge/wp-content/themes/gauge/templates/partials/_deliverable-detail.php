<?php
/**
 * Created by WebStorm.
 * User: anderson
 * Date: 1/8/15
 * Time: 12:48 PM
 */

$postid = custom_url_to_postid($_POST['post_link']);
$post   = get_post($postid);

?>

<div class="deliverable-detail">
    <div class="deliverable-detail-inner">
        <h2 class="detail-title">
            <?php echo $post->post_title; ?>
        </h2>
        <div class="detail-content">
            <?php echo $post->post_content; ?>
        </div>
    </div>
</div>