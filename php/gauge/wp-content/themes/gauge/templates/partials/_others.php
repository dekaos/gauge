<?php
/**
 * Created by PhpStorm.
 * User: anderson
 * Date: 11/23/14
 * Time: 3:48 PM
 */
?>
<form method="post" id="others-contacts" name="others-contacts" novalidate>
    <div class="col-md-6">
        <h3>
            <?php _e('Tell us about you', 'roots'); ?>
        </h3>
        <div class="input-group">
            <label for="ot-name">
                <?php _e('Name', 'roots'); ?>
            </label>
            <input class="form-control" id="ot-name" name="ot-name" type="text" required>
        </div>
        <div class="input-group">
            <label for="ot-email">
                <?php _e('E-mail', 'roots'); ?>
            </label>
            <input class="form-control" id="ot-email" name="ot-email" type="email" required>
        </div>
        <h3>
            <?php _e('Which contact reason?', 'roots'); ?>
        </h3>
        <div class="input-group">
            <label for="ot-subject">
                <?php _e('Subject', 'roots'); ?>
            </label>
            <input class="form-control" id="ot-subject" name="ot-subject" type="text" required>
        </div>
        <div class="input-group">
            <label for="ot-message">
                <?php _e('Message', 'roots'); ?>
            </label>
            <textarea class="form-control" name="ot-message" id="ot-message" cols="30" rows="10" required></textarea>
        </div>
        <div class="input-group">
            <input value="others-contacts" name="action" type="hidden">
            <input class="form-control others-submit btn btn-info" name="contato_outros_enviar_sucesso" value="<?php _e('Send message', 'roots'); ?>" type="submit">
        </div>
    </div>
</form>