<div class="sidebar-right pull-left">
    <div class="sidebar-inner pull-right">
        <div class="author">
            <div class="author-avatar">
                <?php echo get_avatar( get_the_author_meta( 'ID' ), 60); ?>
            </div>
            <div class="author-info">
                <div class="name">
                    <?php echo get_the_author(); ?>
                </div>
                <div class="cargo">
                    <?php echo get_the_author_meta('user_cargo'); ?>
                </div>
            </div>
        </div>
        <div class="category-info">
            <?php

                $category_name = get_the_category($post->ID);

                if ($category_name[0]->cat_ID == '17') {
                    echo $category_name[1]->name;
                } else {
                    echo $category_name[0]->name;
                }

            ?>
        </div>
        <div class="date-info">
            <div class="date-label">
                <?php _e('Date:', 'roots'); ?>
            </div>
            <div class="date-post">
                <?php the_date(); ?>
            </div>
        </div>
        <div class="tags-info">
            <div class="tags-label">
                Tags:
            </div>

            <?php

                $post_tags = get_the_tags();

            if ($post_tags) : ?>
                <div class="tags-name">
                    <?php foreach (get_the_tags() as $tag) : ?>
                        <span class="tag-name">
                            <?php echo $tag->name; ?>
                        </span>
                    <?php endforeach; ?>
                </div>
            <?php else : ?>
                <br>
                <span class="no-tag">
                <?php _e('No tags found', 'roots'); ?>
            </span>
            <?php endif; ?>
        </div>
        <div class="social-share">
            <?php $single_post_name = str_replace('-', '_', $post->post_name); ?>
            <a id="compartilhar_<?php echo $single_post_name; ?>" share-id="<?php echo $single_post_name; ?>" class="share-bottom btn btn-info"><?php _e('Share', 'roots'); ?></a>
        </div>
        <div class="search-box">
            <form role="search" method="get" class="search-form-single form-inline" action="<?php echo get_permalink(147); ?>">
                <div class="input-group">
                    <input type="search" value="" name="search" class="search-field form-control" placeholder="O que você procura?" maxlength="70">
                <span class="input-group-btn">
                    <button type="submit" class="search-submit btn">Busca</button>
                </span>
                </div>
            </form>
        </div>
        <div class="categories-box">
            <h3>
                <?php _e('Our categories', 'roots'); ?>
            </h3>
            <div class="categories">
                <?php

                $cats_name = array(
                    'type'                     => 'post',
                    'child_of'                 => 17,
                    'parent'                   => '',
                    'orderby'                  => 'name',
                    'order'                    => 'ASC',
                    'hide_empty'               => 1,
                    'hierarchical'             => 1,
                    'exclude'                  => '',
                    'include'                  => '',
                    'number'                   => '',
                    'taxonomy'                 => 'category',
                    'pad_counts'               => false

                );

                $categories = get_categories( $cats_name );

                foreach ($categories as $category) : ?>
                    <a id="nossas_categorias_<?php echo str_replace('-', '_', $category->slug); ?>" href="<?php echo get_permalink(147); ?>?cat=<?php echo $category->cat_ID; ?>">
                        <?php echo $category->name; ?>
                    </a>
                <?php endforeach; ?>
            </div>
        </div>
    </div>
</div>