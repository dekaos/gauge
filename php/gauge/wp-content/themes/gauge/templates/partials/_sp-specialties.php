<?php global $post; ?>

<?php $length = array_filter(get_post_meta( $post->ID, 'sp-specialties' )[0]); ?>
<?php if (is_single()) {
  $extra_class = 'sp-applied-single';
} else {
    $extra_class = 'sp-applied-page';
}

?>
<?php if (count($length) > 0) : ?>
    <?php $sp_specialties = get_post_meta( $post->ID, 'sp-specialties' )[0]; ?>
    <div class="sp-specialties">
        <div class="container">
            <div class="sp-specialties-inner">
                <h3 class="sp-applied-title">
                    <?php if (is_single()) {
                        _e('Applied specialties', 'roots');
                    } else {
                        _e('Related specialties', 'roots');
                        echo '<span data-id="' . $sp_specialties[0]['sp-post'] . '" class="see-all pull-right">' . __('See all specialties', 'roots') . '</span>';
                    }
                    ?>
                </h3>
                <?php
                $ce = 0;
                foreach ($sp_specialties as $sp_specialtie) : ?>

                    <?php $data_id = $sp_specialtie['sp-post']; ?>
                    <?php $class   = $sp_specialtie['sp-class']; ?>
                    <?php $title   = get_post($sp_specialtie['sp-post'])->post_title; ?>
                    <?php $spec_id = str_replace('-', '_', get_post($sp_specialtie['sp-post'])->post_name); ?>

                    <div class="sp-applied-wrapper pull-left">
                        <a id="espec_<?php echo is_page() ? 'macro_': ''; ?><?php echo $spec_id; ?>" class="sp-applied <?php echo $extra_class . ' ' . $class; ?>" data-id="<?php echo $data_id; ?>">
                            <span class="sp-icon"></span>
                            <span class="sp-title">
                                <?php echo $title; ?>
                            </span>
                        </a>
                        <?php if (is_page()) : ?>
                        <div class="main-deliverables">
                            <h4>
                                <?php _e('Main deliverables', 'roots'); ?>
                            </h4>
                            <div class="deliverables-links">
                            <?php
                                $el    = new DOMDocument();
                                $el->loadHTML(get_post($data_id)->post_content);
                                $items   = $el->getElementsByTagName('a');
                                $counter = 0;

                                echo '<div class="clearfix"></div><ul>';

                                foreach ($items as $item) {
                                    if ($counter < 5) {

                                        $string  = mb_convert_encoding($item->nodeValue, 'iso-8859-1');
                                        $accents = '/&([A-Za-z]{1,2})(grave|acute|circ|cedil|uml|lig);/';

                                        $string_encoded = htmlentities($string,ENT_NOQUOTES,'UTF-8');

                                        $string  = preg_replace($accents,'$1', $string_encoded);
                                        $item_id = strtolower(str_replace(' ', '_', $string));
                                        $post_id = custom_url_to_postid($item->getAttribute('href'));
                                        echo '<li parent-id="' . $data_id . '" data-id="' . $post_id . '" class="links-list">';
                                        echo '<a id="espec_' . $spec_id . '_2_' . $item_id . '" href="' . $item->getAttribute('href') . '">' . mb_convert_encoding($item->nodeValue, 'iso-8859-1') . '</a>';
                                        echo '</li>';
                                    }

                                    $counter++;
                                }

                                echo '</ul>';
                            ?>
                            </div>
                            <button data-id="<?php echo $data_id; ?>" class="m-info-service btn btn-info"><?php _e('Know the service', 'roots'); ?></button>
                        </div>
                        <?php endif; ?>
                    </div>
                    <?php if ($ce != 0 && $ce % 4 == 0) : ?>
                        <div class="clearfix"></div>
                    <?php endif; ?>
                <?php $ce++; endforeach ?>
                <div class="clearfix"></div>
            </div>
        </div>
    </div>
<?php endif; ?>
