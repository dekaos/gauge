    <div class="input-group">
        <label for="nv-name"><?php _e('Name', 'roots'); ?></label>
        <input id="nv-name" name="nv-name" class="form-control" type="text" required>
    </div>
    <div class="input-group">
        <label for="nv-email">E-mail</label>
        <input id="nv-email" name="nv-email" class="form-control" type="text" required>
    </div>
    <div class="clearfix"></div>
    <div class="input-group select-state pull-left">
        <label for="nv-state">
            <?php _e('State', 'roots'); ?>
        </label>
        <div class="form-inline">
            <div class="btn-group">
                <input data-toggle="dropdown" id="nv-state" type="button" name="n-state" class="btn btn-default dropdown-toggle form-group" required>
                <ul class="dropdown-menu">
                    <?php foreach($states as $state) : ?>
                        <li>
                            <input type="radio" id="nv-state-<?php echo $state->id; ?>" name="nv-state" value="<?php echo $state->id; ?>">
                            <label for="nv-state-<?php echo $state->id; ?>"><?php echo $state->uf; ?></label>
                        </li>
                    <?php endforeach; ?>
                </ul>
            </div>
        </div>
    </div>
    <div class="input-group select-city pull-left">
        <label for="nv-city">
            <?php _e('City', 'roots'); ?>
        </label>
        <div class="form-inline">
            <div class="btn-group">
                <input data-toggle="dropdown" id="nv-city" type="text" name="n-city" class="btn btn-default dropdown-toggle" required autocomplete="off" disabled>
                <ul id="city-groups-two" class="dropdown-menu">

                </ul>
            </div>
        </div>
    </div>
    <div class="input-group">
        <label for="nv-phone"><?php _e('DDD + Phone', 'roots'); ?></label>
        <input id="nv-phone" class="form-control" name="nv-phone" type="text" required>
    </div>
    <?php if (isset($departments)) : ?>
    <div class="input-group">
        <label for="nv-interest">
            <?php _e('Area of interest','roots'); ?>
        </label>
        <div class="form-inline">
            <div class="btn-group interest-area">
                <input data-toggle="dropdown" id="nv-interest" type="text" name="n-interest" class="btn btn-default dropdown-toggle" required>
                <ul class="dropdown-menu">
                    <?php foreach($departments as $department) : ?>
                        <li>
                            <input type="radio" id="nv-interest-<?php echo $department->id; ?>" name="nv-interest" value="<?php echo $department->id; ?>">
                            <label for="nv-interest-<?php echo $department->id; ?>"><?php echo $department->title; ?></label>
                        </li>
                    <?php endforeach; ?>
                </ul>
            </div>
        </div>
    </div>
    <?php endif; ?>
    <div class="input-group linkedin">
        <label for="nv-likedin">
            <?php _e('LinkedIn profile', 'roots'); ?>
        </label>
        <input class="form-control" id="nv-linkedin" name="nv-linkedin" type="text" required>
    </div>
    <div class="input-group file-upload">
        <label for="nv-upload">
            <?php _e('Insert a file', 'roots'); ?>
        </label>
        <input name="nv-upload" class="nv-upload" id="nv-upload" type="file" accept="application/pdf" required>
        <span>
            <?php _e('Maximum size: 5mb / Format: pdf', 'roots'); ?>
        </span>
    </div>
    <?php wp_reset_query(); ?>