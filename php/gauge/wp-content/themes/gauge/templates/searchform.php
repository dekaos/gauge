<div class="col-md-12">
  <form role="search" method="get" class="search-form form-inline" action="<?php echo home_url('/'); ?>">
    <div class="input-group">
      <input type="search" value="<?php if (is_search()) { echo get_search_query(); } ?>" name="s" class="search-field form-control" placeholder="<?php _e('What you looking for?', 'roots'); ?>" maxlength="70">
      <label class="hide"><?php _e('Search for:', 'roots'); ?></label>
    <span class="input-group-btn">
      <button type="submit" class="search-submit btn"><?php _e('Search', 'roots'); ?></button>
    </span>
    </div>
  </form>
</div>
