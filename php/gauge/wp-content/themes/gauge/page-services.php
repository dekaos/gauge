<?php
/**
 * Created by WebStorm.
 * User: anderson
 * Template Name: Serviços
 * Date: 1/15/15
 * Time: 2:04 PM
 */
?>

<div id="services-wrapper">
    <div id="serices-header-wrapper" class="yellow-bg">
        <header class="entry-meta">
            <div class="container">
                <h1>
                    <?php _e('What we do to deliver results', 'roots'); ?>
                </h1>
            </div>
        </header>
    </div>
    <div id="service-main">
        <?php
            $page_559     = get_post(559);
            $page_559_img = wp_get_attachment_image_src(get_post_thumbnail_id(559), 'single-post-thumbnail');
            $page_561     = get_post(561);
            $page_561_img = wp_get_attachment_image_src(get_post_thumbnail_id(561), 'single-post-thumbnail');
            $page_563     = get_post(563);
            $page_563_img = wp_get_attachment_image_src(get_post_thumbnail_id(563), 'single-post-thumbnail');
            $page_565     = get_post(565);
            $page_565_img = wp_get_attachment_image_src(get_post_thumbnail_id(565), 'single-post-thumbnail');
        ?>
        <div class="article-outer page-559" <?php echo !empty($page_559_img) && !wp_is_mobile() ? 'style="background: url(' . $page_559_img[0] . ') no-repeat center center"' : ''; ?>>
            <div class="container">
                <header class="entry-header col-md-4">
                    <div class="entry-header-inner">
                        <h2 class="page-title" data-id="#section-<?php echo $page_559->ID; ?>">
                            <div class="table-style">
                                <span class="page-logo"></span>
                                <span class="title-txt">
                                    <?php echo $page_559->post_title; ?>
                                </span>
                                <span class="indicator plus"></span>
                            </div>
                        </h2>
                    </div>
                </header>
                <article id="section-<?php echo $page_559->ID; ?>" class="entry-content col-md-8">
                    <div class="primary-content">
                        <div class="entry-content-inner">
                            <?php
                                //excerpt_130($page_559->post_excerpt);
                                echo wpautop(get_post_meta(559, 'second-excerpt')[0]);
                            ?>
                        </div>
                        <div class="entry-arrow">
                            <span class="right-arrow"></span>
                        </div>
                    </div>
                    <div class="secondary-content">
                        <div class="entry-content-inner">
                            <?php echo wpautop($page_559->post_content); ?>
                        </div>
                        <div class="entry-link">
                            <a id="servicos_conheca_<?php echo str_replace('-', '_', $page_559->post_name); ?>" href="<?php echo get_the_permalink(559); ?>">
                                <?php _e('Meet', 'roots'); ?>
                            </a>
                        </div>
                    </div>
                </article>
            </div>
            <div class="bg-layer"></div>
        </div>
        <div class="article-outer page-561" <?php echo !empty($page_561_img) && !wp_is_mobile() ? 'style="background: url(' . $page_561_img[0] . ') no-repeat center center"' : ''; ?>>
            <div class="container">
                <header class="entry-header col-md-4">
                    <div class="entry-header-inner">
                        <h2 class="page-title" data-id="#section-<?php echo $page_561->ID; ?>">
                            <div class="table-style">
                                <span class="page-logo"></span>
                                <span class="title-txt">
                                    <?php echo $page_561->post_title; ?>
                                </span>
                                <span class="indicator plus"></span>
                            </div>
                        </h2>
                    </div>
                </header>
                <article id="section-<?php echo $page_561->ID; ?>"  class="entry-content col-md-8">
                    <div class="primary-content">
                        <div class="entry-content-inner">
                            <?php echo wpautop(get_post_meta(561, 'second-excerpt')[0]); ?>
                        </div>
                        <div class="entry-arrow">
                            <span class="right-arrow"></span>
                        </div>
                    </div>
                    <div class="secondary-content">
                        <div class="entry-content-inner">
                            <?php echo wpautop($page_561->post_content); ?>
                        </div>
                        <div class="entry-link">
                            <a id="servicos_conheca_<?php echo str_replace('-', '_', $page_561->post_name); ?>" href="<?php echo get_the_permalink(561); ?>">
                                <?php _e('Meet', 'roots'); ?>
                            </a>
                        </div>
                    </div>
                </article>
            </div>
            <div class="bg-layer"></div>
        </div>
        <div class="article-outer page-563" <?php echo !empty($page_563_img) && !wp_is_mobile() ? 'style="background: url(' . $page_563_img[0] . ') no-repeat center center"' : ''; ?>>
            <div class="container">
                <header class="entry-header col-md-4">
                    <div class="entry-header-inner">
                        <h2 class="page-title" data-id="#section-<?php echo $page_563->ID; ?>">
                            <div class="table-style">
                                <span class="page-logo"></span>
                                <span class="title-txt">
                                    <?php echo $page_563->post_title; ?>
                                </span>
                                <span class="indicator plus"></span>
                            </div>
                        </h2>
                    </div>
                </header>
                <article id="section-<?php echo $page_563->ID; ?>" class="entry-content col-md-8">
                    <div class="primary-content">
                        <div class="entry-content-inner">
                            <?php echo wpautop(get_post_meta(563, 'second-excerpt')[0]); ?>
                        </div>
                        <div class="entry-arrow">
                            <span class="right-arrow"></span>
                        </div>
                    </div>
                    <div class="secondary-content">
                        <div class="entry-content-inner">
                            <?php echo wpautop($page_563->post_content); ?>
                        </div>
                        <div class="entry-link">
                            <a id="servicos_conheca_<?php echo str_replace('-', '_', $page_563->post_name); ?>" href="<?php echo get_the_permalink(563); ?>">
                                <?php _e('Meet', 'roots'); ?>
                            </a>
                        </div>
                    </div>
                </article>
            </div>
            <div class="bg-layer"></div>
        </div>
        <div class="article-outer page-565" <?php echo !empty($page_565_img) && !wp_is_mobile() ? 'style="background: url(' . $page_565_img[0] . ') no-repeat center center"' : ''; ?>>
            <div class="container">
                <header class="entry-header col-md-4">
                    <div class="entry-header-inner">
                        <h2 class="page-title" data-id="#section-<?php echo $page_565->ID; ?>">
                            <div class="table-style">
                                <span class="page-logo"></span>
                                <span class="title-txt">
                                    <?php echo $page_565->post_title; ?>
                                </span>
                                <span class="indicator plus"></span>
                            </div>
                        </h2>
                    </div>
                </header>
                <article id="section-<?php echo $page_565->ID; ?>" class="entry-content col-md-8">
                    <div class="primary-content">
                        <div class="entry-content-inner">
                            <?php echo wpautop(get_post_meta(565, 'second-excerpt')[0]); ?>
                        </div>
                        <div class="entry-arrow">
                            <span class="right-arrow"></span>
                        </div>
                    </div>
                    <div class="secondary-content">
                        <div class="entry-content-inner">
                            <?php echo wpautop($page_565->post_content); ?>
                        </div>
                        <div class="entry-link">
                            <a id="servicos_conheca_<?php echo str_replace('-', '_', $page_565->post_name); ?>" href="<?php echo get_the_permalink(565); ?>">
                                <?php _e('Meet', 'roots'); ?>
                            </a>
                        </div>
                    </div>
                </article>
            </div>
            <div class="bg-layer"></div>
        </div>
    </div>
</div>