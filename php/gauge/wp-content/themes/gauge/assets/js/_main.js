/* ========================================================================
 * DOM-based Routing
 * Based on http://goo.gl/EUTi53 by Paul Irish
 *
 * Only fires on body classes that match. If a body class contains a dash,
 * replace the dash with an underscore when adding it to the object below.
 *
 * .noConflict()
 * The routing is enclosed within an anonymous function so that you can
 * always reference jQuery with $, even when in .noConflict() mode.
 *
 * Google CDN, Latest jQuery
 * To use the default WordPress version of jQuery, go to lib/config.php and
 * remove or comment out: add_theme_support('jquery-cdn');
 * ======================================================================== */

(function($) {

// Use this variable to set up the common and page specific functions. If you
// rename this variable, you will also need to rename the namespace below.
	var Roots = {
		// All pages
		common: {
			init: function() {
				var aCategoryTitle;
				var	aOffset;
				var	aCategory;
				var	aClearHtml;
				var	aBehavior;
				var	aElems;
				var	currentId;
				var	fMessage;
				var	fClose;
				var	fTip;
				var	fForm;
				var	oldUrl;
				var	fResponse;
				var	mContainer;
				var	vOptions;
				var	dataMedia;
				var	vId;
				var	cPrefix;
				var	iElem;
				var	validOptions;
				var	vForm;
				var	uElem;
				var	parentId;
				var	control;
				var	fullsize;
				var	postLink;
				var	fullContent;
				var	getDetail;
				var	specialtyId;
				var	getSp;
				var	seContainer;
				var	mobileVersion;
				var	deg;
				var	screen;
				var kcw;
				var kgt;
				var	reverse;
				var	sElems;
				var closeLg;
				var checKMb;
				var sigla;
				var expanded;
				var thisOffsetTop;
				var thisOffsetLeft;
				var thisWidth;
				var thisHeight;

				$('.langs a').each(function (index) {
					sigla = $('.langs a').eq(index).text().slice(0, 2);
					if (sigla == 'en' || sigla == 'us') {
						sigla = 'en';
					}
					$('.langs a').eq(index).removeAttr('title');
					$('.langs a').eq(index).text(sigla);
				});

				var checkR = function () {
					if ($(window).width() > 991) {
						$('#our-process-wrapper').removeClass('is-visible');
						$('#open-image-wrapper, .our-services').removeClass('is-not-visible')
					}
				};

				checkR();

				$(window).resize(function () {
					$('.analyses-wrapper').attr('style', 'height:' + $('.analyses').height() + 'px');
					checkR();
				});

				checKMb = function () {
					if ($(window).width() < 992) {
						mobileVersion = true;
					} else {
						mobileVersion = false;
					}
				};

				checKMb();

				closeLg = function () {
					$('.lightbox-specialty').fadeOut('fast');

					$('body').css({
						'overflow-y': 'auto'
					})
				};

				$('.navbar-toggle').on('click', function () {
					//$('.navbar-toggle').removeClass('opened');
					$(this).toggleClass('opened');
				});

				$('body').on('click', '.lg-close', function () {
					closeLg();
				});

				$(window).resize(function () {
					closeLg();
					checKMb();
				});

				var accentsTidy = function(s){
					var r = s.toLowerCase();
					r = r.replace(new RegExp('\\s', 'g'),'_');
					r = r.replace(new RegExp('[àáâãäå]', 'g'),'a');
					r = r.replace(new RegExp('æ', 'g'),'ae');
					r = r.replace(new RegExp('ç', 'g'),'c');
					r = r.replace(new RegExp('[èéêë]', 'g'),'e');
					r = r.replace(new RegExp('[ìíîï]', 'g'),'i');
					r = r.replace(new RegExp('ñ', 'g'),'n');
					r = r.replace(new RegExp('[òóôõö]', 'g'),'o');
					r = r.replace(new RegExp('œ', 'g'),'oe');
					r = r.replace(new RegExp('[ùúûü]', 'g'),'u');
					r = r.replace(new RegExp('[ýÿ]', 'g'),'y');
					r = r.replace(new RegExp('\\W', 'g'),'');
					return r;
				};

				if ($('div').hasClass('details-links')) {
					$('.details-links li a').each(function (index) {
						$(this).attr('id', 'espec_' + $(this).closest('.content-list').closest('.specialty-title').attr('id') + '_2_' + accentsTidy($(this).text()));
					});
				}

				if($('body').find('.no-results-box').length > 0) {
					mContainer = $('.no-results-box');
				} else {
					mContainer = $('.masonry');
				}

				/*if ($(window).width() > 766) {
					$('.top-menu .custom-selected.dropdown').hover(function () {
						$(this).find('.dropdown-menu').slideDown('fast');
						console.log($(window).width());
					});

					$('.top-menu .dropdown-menu').on('mouseleave', function () {
						//if (!$('.custom-selected').is('.active')) {
							setTimeout(function () {
								$('.top-menu .custom-selected .dropdown-menu').slideUp('fast');
							}, 500);
						//}
					});
				}*/

				$('.top-menu .dropdown-menu').on('mouseleave', function (e) {
					e.stopPropagation();
				});

				seContainer = $('.se-modular-items');
				$('form').trigger('reset');

				function getForm () {
					vForm = $(document.body).find('.form-validate');
					return vForm;
				}

				dataMedia = $('.featured-image').attr('data-media');
				if (dataMedia) {
					$('.featured-image').css({
						'background-image': 'url(' + dataMedia + ')'
					});
				}

				$('.share-bottom').on('click', function () {
					var shareId = $(this).attr('share-id');
					$('.share-wrapper').fadeIn();
					$('body').css('overflow', 'hidden');
					$('.at-svc-facebook').attr('id', 'compartilhar_facebook_' + shareId);
					$('.at-svc-twitter').attr('id', 'compartilhar_twitter_' + shareId);
					$('.at-svc-linkedin').attr('id', 'compartilhar_linkedin_' + shareId);
					$('.at-svc-google_plusone_share').attr('id', 'compartilhar_google_plusone_' + shareId);
					$('.at-svc-whatsapp').attr('id', 'compartilhar_whatsapp' + shareId);
					return toClipBoard();
				});

				function toClipBoard () {
					$('.copy-bottom').zclip({
						path: '/ZeroClipboard.swf',
						copy: $('#copy-url').val(),
						afterCopy:function(){
						}
					});
				}

				$('.analyses-wrapper').attr('style', 'height:' + $('.analyses').height() + 'px');

				$('input[name="se-category"]').on('change', function () {
					aCategory = $(this).val();
					seContainer.html('').css({'opacity': '0'});
					$('.se-modular-inner').append('<span class="loading"></span>');
					$.ajax({
						url: ajaxurl,
						type: 'post',
						cache: true,
						data: {
							action: 'se-modular',
							se_cat: aCategory,
							single_cat_id: $('#single-cat-id').val()
						},
						success: function (data) {
							$('.loading').remove();

							seContainer.html(data);
							seContainer.masonry('reloadItems');

							if ($(window).width() < 768 ) {
								seMasonry(140, 10);
							} else {
								seMasonry(220, 20);
							}
						}
					});
				});

				var seMasonry = function (colunWidth, gutter) {
					seContainer.imagesLoaded(function () {
						seContainer.masonry({
							columnWidth: colunWidth,
							gutter: gutter,
							isFitWidth: true,
							itemSelector: '.se-masonry',
							isAnimated: true
						})
					});
					setTimeout(function () {
						seContainer.animate({
							'opacity': 1
						});
					}, 100);
				};

				$('.share-box .close-bottom').on('click', function () {
					$('.share-wrapper').fadeOut();
					$('body').css('overflow', 'auto');
				});

				$('#work-with-us, #research').on('change', 'input[name="re-state"], input[name="nv-state"]', function () {
					if ($(this).attr('name') === 're-state') {
						iElem = $('#re-city');
						uElem = $('#city-groups');
						cPrefix = 're';
					} else {
						iElem = $('#nv-city');
						uElem = $('#city-groups-two');
						cPrefix = 'nv'
					}

					$('#re-state, #nv-state').text($(this).next('label').text());

					$.ajax({
						url: ajaxurl,
						type: 'post',
						data: {
							action: 'get-citys',
							state_id: $(this).val()
						},
						success: function (data) {
							uElem.find('li').remove();

							iElem.val('');

							if (data) {
								iElem.removeAttr('disabled');
								return populateCitys(data, cPrefix, uElem);
							}
						}
					});
				});

				function populateCitys (data, cPrefix, uElem) {

					var city   = $.parseJSON(data);
					var values = '';

					for (var i in city) {
						values += '<li><input type="radio" name="' + cPrefix + '-city" id="'  + cPrefix + '-city-' + city[i].city_id + '" value="' + city[i].city_id + '"><label for="' + cPrefix + '-city-' + city[i].city_id + '">' + city[i].city_name + '</label></li>';
					}

					uElem.html(values);
				}

				$('#re-city').on('keyup', function (e) {
					$.ajax({
						type: 'POST',
						url: ajaxurl,
						data: {
							action: 'get-citys',
							state_id: $('input[name="re-state"]:checked').val(),
							action_type: 'search_by_string',
							string: $(this).val()
						},
						success: function (data) {
							uElem   = $('#city-groups');
							cPrefix = 're';

							if (data) {

								return populateCitys(data, cPrefix, uElem);

							} else {
								uElem.html('');
							}
						}
					});
				});

				$(document).on('keyup', '#nv-city', function () {

					$.ajax({
						type: 'POST',
						url: ajaxurl,
						data: {
							action: 'get-citys',
							state_id: $(document).find('input[name="nv-state"]:checked').val(),
							action_type: 'search_by_string',
							string: $(this).val()
						},
						success: function (data) {
							uElem   = $('#city-groups-two');
							cPrefix = 'nv';

							if (data) {

								uElem.css({
									'display': 'block !important'
								});

								return populateCitys(data, cPrefix, uElem);
							} else {
								uElem.html('');
							}
						}
					});

				});

				validOptions = {
					rules: {
						'nv-phone': {
							minlength: 14
						},
						'nv-upload': {
							accept: 'application/pdf',
							filesize: 5242880
						},
						'nv-email': {
							required: {
								depends:function(){
									$(this).val($.trim($(this).val()));
									return true;
								}
							},
							customemail: true
						}
					}
				};

				vOptions = {
					url: ajaxurl,
					beforeSend: function () {
						$('body').find('.vacancy-submit, .no-vacancy-submit').after('<span class="loading"></span>');
					},
					success: function (data) {
						$('.loading').remove();
						fResponse = $.parseJSON(data);

						if (fResponse[0].type_of === 'success') {
							fMessage = fResponse[0].message;
							fTip     = fResponse[0].tip;
							fClose   = fResponse[0].close;
							fForm    = $('#' + fResponse[0].form_name);

							if (fResponse[0].form_name === 'no-vacancy-form') {
								dataLayer.push({
									'event': 'Envio_Formularios', 'form': 'contato_trabalhe_conosco_enviar_sucesso'
								});

								dataLayer.push({
									'event': 'VirtualPageview',
									'virtualPageURL': '/contatosucesso/trabalhe-conosco',
									'virtualPageTitle': 'Contato – Trabalhe Conosco'
								});
							}

							if (fResponse[0].form_name === 'vacancy-form') {
								dataLayer.push({
									'event': 'Envio_Formularios', 'form': 'contato_trabalhe_conosco_vaga_enviar_sucesso'
								});

								dataLayer.push({
									'event': 'VirtualPageview',
									'virtualPageURL': '/contatosucesso/trabalhe-conosco-vaga',
									'virtualPageTitle': 'Contato – Trabalhe Conosco Vaga'
								});
							}

							return successMessage(fMessage, fTip, fClose, fForm);

						} else {
							if (fResponse[0].type_of === 'process_error') {
								alert(fResponse[0].message);
							}
						}
					}
				};

				$('#work-with-us').on('click', '.vacancy-box', function (e) {

					$('#work-with-us').append('<span class="loading"></span>');

					$('#work-with-us-inner').css({
						'opacity': 0
					});

					vId = $(this).find('.vacancy-link').attr('data-id');

					$.ajax({
						url: ajaxurl,
						type: 'POST',
						data: {
							action: 'vacancy_detail',
							v_id: vId
						},
						success: function (data) {

							$('#work-with-us .loading').remove();

							$('#work-with-us-inner').html(data);

							setTimeout(function () {
								$('#work-with-us-inner').animate({
									'opacity': 1
								}, 400);
							}, 400);
						},
						complete: function () {
							vForm = getForm();
							vForm.validate(validOptions);
							vForm.ajaxForm(vOptions);
						}
					});
					e.preventDefault();
				});

				$('#work-with-us').on('click', '#back', function (e) {

					$('#work-with-us').append('<span class="loading"></span>');

					$('#work-with-us-inner').css({
						'opacity': 0
					});

					$.ajax({
						url: ajaxurl,
						type: 'POST',
						data: {
							action: 'vacancies'
						},
						success: function (data) {

							$('#work-with-us .loading').remove();

							$('#work-with-us-inner').html(data);

							setTimeout(function () {
								$('#work-with-us-inner').animate({
									'opacity': 1
								}, 400);
							}, 400);
						},
						complete: function () {
							vForm = getForm();
							vForm.validate(validOptions);
							vForm.ajaxForm(vOptions);
						}
					});
					e.preventDefault();
				});

				$.validator.addMethod('customemail',
					function(value) {
						return /^([a-zA-Z0-9_.+-])+\@(([a-zA-Z0-9-])+\.)+([a-zA-Z0-9]{2,4})+$/.test(value);
					},
					errorEmail
				);

				$.validator.addMethod('filesize', function(value, element, param) {
					return this.optional(element) || (element.files[0].size <= param)
				});

				vForm = getForm();
				vForm.validate(validOptions);
				vForm.ajaxForm(vOptions);

				$('#work-with-us').on('change', '#nv-upload', function () {
					if($(this).valid()) {
						$('#work-with-us').find('label[for="nv-upload"]').append('<span class="pdf-icon"></span>');
					} else {
						$('.pdf-icon').remove();
					}
				});

				$('.search-form').on('submit', function (e) {
					e.preventDefault();

					changeUrl();

					if ($('.search-field').val().length > 0) {
						$('#more-posts').fadeOut('fast');
						$('#know-main-inner, .search-results-box').html('').removeAttr('style');
						$('#know-main').append('<span class="loading"></span>');
						$.ajax({
							type: 'GET',
							url: ajaxurl,
							data: {
								action: 'ajax_search',
								search: $('.search-field').val(),
								cat: $('input[name="category"]').filter(':checked').val()
							},
							success: function (data) {
								$('#know-main .loading').remove();

								$('#know-main-inner').append($(data));

								var nContainer = $('#know-main').find('.no-results-box');

								sElems = nContainer.find('.no-results-box .post-item-wrapper');

								nContainer.masonry({
									columnWidth: 220,
									gutter: 21,
									itemSelector: '.post-item-wrapper'
								}).fadeIn();

								setTimeout(function () {
									if (sElems.length > 0) {

										nContainer.append(sElems).masonry('prepended', sElems, true);
									}

									$('#know-main').find('.search-results-box').fadeIn('slow');
									$('.search-form').trigger('reset');
								}, 400);

								$('.item-bg-layer').animate({
									'opacity': 1
								}, 100);
							}
						});
					}
				});

				var mRcheck = function () {
					if ($(window).width() >= 320) {
						kcw = 300;
						kgt = 10;
					}
					if ($(window).width() > 479) {
						kcw = 460;
						kgt = 10;
					}
					if ($(window).width() > 766) {
						kcw = 220;
						kgt = 20;
					}
				};

				mRcheck();

				$(window).resize(function () {
					mRcheck();
					setTimeout(function () {
						mContainer.masonry('reloadItems').masonry({
							columnWidth: kcw,
							gutter: kgt});
					});
				});

				var masonryGrid = function (mContainer) {
					if ($('#know-main').find('.item-link').length === 0) {
						mContainer.masonry({
							columnWidth: kcw,
							gutter: kgt,
							isFitWidth: true,
							itemSelector: '.post-item-wrapper'
						}).fadeIn('slow');
						$('.item-bg-layer').animate({
							'opacity': 1
						}, 100);
					}
				};

				masonryGrid(mContainer);

				function postMasonry (colunWidth, gutter) {
					$('.post-mansory').imagesLoaded(function () {
						$('.post-masonry').masonry({
							columnWidth: colunWidth,
							gutter: gutter,
							itemSelector: '.post-masonry-image'
						}).fadeIn('slow');
					});
				}

				if ($(window).width() < 768 ) {
					postMasonry(140, 10);
					seMasonry(140, 10);
				} else {
					postMasonry(220, 20);
					seMasonry(220, 20);
				}

				//seMasonry(220, 20);

				$('input[name="category"]').on('change', function () {
					$('#know-main .search-results-box, .no-results-box').remove();

					changeUrl();

					mContainer = $('.masonry');

					if ($(this).attr('id') === 'category-all' && $(this).filter(':checked')) {
						aCategoryTitle = $('.top-menu li.active a').text();
					} else {
						aCategoryTitle = $(this).next('label').text();
					}

					$('h1.dynamic span').fadeOut(100);
					setTimeout(function() {
						$('h1.dynamic span').text(aCategoryTitle).fadeIn('fast');
					}, 200);

					aCategory = $(this).val();

					return postsByCategory(aCategory, mContainer, null);

				});

				var changeUrl = function () {
					oldUrl = window.location.href;
					oldUrl = oldUrl.substring(0 ,oldUrl.indexOf('?'));

					window.history.pushState('', '', oldUrl);
				};

				var postsByCategory = function (aCategory, mContainer, aOffset) {
					mRcheck();
					setTimeout(function () {
						masonryGrid(mContainer);
					});

					$('.masonry').css({
						'margin-top': '0'
					});

					if (!aOffset) {
						mContainer.append('<span class="loading"></span>');
						aClearHtml = true;
						aBehavior  = 'prepended';
						$('.post-item-wrapper').fadeOut('fast');
					} else {
						aClearHtml = false;
						aBehavior  = 'appended';
					}

					$.ajax({
						type: 'GET',
						url: ajaxurl,
						cache: false,
						data: {
							action: 'by_category',
							cat: aCategory,
							item_offset: aOffset,
							page_name: pageName,
							page_tmp: pageTemplate
						},
						success: function (data) {

							if ($(data).filter('.post-item-wrapper').length >= 10 ) {
								$('.charge').removeClass('hide');
							} else {
								$('.charge').addClass('hide');
							}

							aElems = $(data);

							if (aElems.length > 0) {
								if (aClearHtml) {
									mContainer.html('');
								}
								mContainer.masonry('reloadItems');
								mContainer.append(aElems).masonry(aBehavior, aElems, true);
								$('#more-posts').fadeIn();
							}

							$('#more-posts').removeClass('charging');
							$('.loading').remove();

							$('.item-bg-layer').animate({
								'opacity': 1
							}, 100)
						}
					})
				};

				$('#more-posts').on('click', function (e) {
					e.preventDefault();

					$(this).addClass('charging');

					aCategory = $('input[name="category"]').filter(':checked').val();
					aOffset   = $('.masonry').find('.post-item-wrapper').length;

					return postsByCategory(aCategory, mContainer, aOffset);
				});

				$('.image-zoom-wrapper').fadeOut();

				$('.open-image').on('click', function (e) {
					$('.image-zoom-wrapper').fadeIn();
					setTimeout(function () {
						$('.img-target').smartZoom({'containerClass':'zoomableContainer'});
						$('#zoomInButton, #zoomOutButton').bind("click", zoomButtonClickHandler);
					}, 500);
					e.preventDefault();
				});

				$('.zoom-close').on('click', function () {
					$('.image-zoom-wrapper').fadeOut();
					$('#zoomOutButton').trigger('click');
				});

				function zoomButtonClickHandler(e){
					var scaleToAdd = 0.8;
					if(e.target.id == 'zoomOutButton')
						scaleToAdd = -scaleToAdd;
					$('.img-target').smartZoom('zoom', scaleToAdd);
				}

				$('.input-radio label').on('click', function () {
					$('.input-radio label').removeClass('active');
					$(this).addClass('active');
				});

				$('.top-nav .dropdown-toggle').click(function(){
					$(this).next('.dropdown-menu').slideToggle(200);
				});

				/*var hiddenOnScroll = function () {
					$('.language-selection').stop().fadeOut('fast');
					$(this).off('scroll')[0].setTimeout(function(){
						$('.language-selection').stop().fadeIn('fast');
						$(this).on('scroll', hiddenOnScroll);
					}, 500);
				};
				$(window).on('scroll', hiddenOnScroll);*/

				$('#know-more').on('click', function (e) {
					$('html, body').animate({
						scrollTop: $('#about').offset().top - 40 + 'px'
					}, 600);
					e.preventDefault();
				});

				$('.page-more').on('click', function (e) {
					$('html, body').animate({
						scrollTop: $('.single-page-inner, .post-outer').offset().top - 70 + 'px'
					}, 600);
					e.preventDefault();
				});

				$('#toggle-menu').click(function (e) {
					$(this).toggleClass('not-visible');
					$('.top-menu').stop().slideToggle('fast');
					e.preventDefault();
				});

				function close_accordion_section() {
					$('.article-outer .page-title, .article-outer .specialty-title, .sp-applied').removeClass('active');
					$('.article-outer .entry-content, .article-outer .specialty-txt, .main-deliverables').slideUp().removeClass('open');
					$('.indicator').addClass('plus');
				}

				$('.page-title, .specialty-title').click(function() {
					if( $(window).width() < 992 ) {

						var currentAttrValue = $(this).attr('data-id');

						if($(this).is('.active')) {

							close_accordion_section();

						}else {

							close_accordion_section();

							$(this).addClass('active');

							$('.article-outer ' + currentAttrValue).slideDown().addClass('open');
							$(this).find('.indicator').removeClass('plus');
						}
						return false;
					}
				});

				$('.to-video').on('click', function (e) {
					e.stopPropagation();

					$(this).css({
						'display': 'none'
					});

					$('.stop-video').css({
						'display': 'block'
					});

					$('body').css({
						'overflow-y': 'hidden'
					});

					$('.to-txt').addClass('close');

					$('.about-featured-wrapper iframe').css({
						'display': 'block',
						'width': $(window).width() + 'px',
						'height': '100%',
						'z-index': '2000'
					});

					$('.about-content-type').css({
						'z-index': '2001'
					});

					setTimeout(function () {
						$('.about-featured-wrapper iframe').animate({
							'opacity': '1'
						});
						$('.about-content-type').animate({
							'margin-top': '-50px'
						});
						player.playVideo();
					}, 300);

					e.preventDefault();
				});

				$(document).on('click', '.to-txt.close, .stop-video', function (e) {
					e.preventDefault();
					$('.stop-video').css({
						'display': 'none'
					});
					$('.to-video').css({
						'display': 'block'
					});
					$('.about-featured-wrapper iframe').animate({
						'opacity': '0'
					});

					$(this).removeClass('close');

					$('.about-content-type').animate({
						'margin-top': '30px'
					});

					setTimeout(function () {
						$('.about-featured-wrapper iframe').css({
							'display': 'none'
						});
					}, 300);

					$('.about-content-type').css({
						'z-index': '10'
					});

					$('body').css({
						'overflow-y': 'auto'
					});

					player.pauseVideo();
				});

				$.validator.messages.required = errorMsg;
				$.validator.messages.email    = errorEmail;

				$('#commercial-form').validate({
					rules: {
						'cm-form-phone': {
							minlength: 14
						},
						'cm-form-email': {
							required: {
								depends: function () {
									$(this).val($.trim($(this).val()));
									return true;
								}
							},
							customemail: true
						}
					},
					submitHandler: function (form) {
						$('.comercial-submit').after('<span class="loading"></span>');

						$.ajax({
							url: ajaxurl,
							type: 'post',
							data: $(form).serialize(),
							success: function (data) {
								$('.comercial-submit + span').remove();
								fResponse = $.parseJSON(data);
								for (var i in fResponse) {
									if (fResponse[i].type_of === 'error') {
										$('#' + fResponse[i].field_id).addClass('error').after('<label class="error">' + fResponse[i].message + '</label>');
									}
								}

								if (fResponse[0].type_of === 'success') {
									fMessage = fResponse[0].message;
									fTip     = fResponse[0].tip;
									fClose   = fResponse[0].close;
									fForm    = $(form);

									dataLayer.push({
										'event': 'Envio_Formularios', 'form': 'contato_comercial_enviar_sucesso'
									});

									dataLayer.push({
										'event':'VirtualPageview',
										'virtualPageURL':'/contatosucesso/comercial',
										'virtualPageTitle' : 'Contato – Comercial'
									});

									return successMessage(fMessage, fTip, fClose, fForm);

								} else {
									if (fResponse[0].type_of === 'process_error') {
										alert(fResponse[0].message);
									}
								}
							}
						});
					}
				});

				$('#research-contact').validate({
					rules: {
						're-sex': "required",
						're-phone': {
							minlength: 14
						},
						're-email': {
							required: {
								depends: function () {
									$(this).val($.trim($(this).val()));
									return true;
								}
							},
							customemail: true
						}
					},
					submitHandler: function (form) {

						$('.research-submit').after('<span class="loading"></span>');

						$.ajax({
							url: ajaxurl,
							type: 'post',
							data: $(form).serialize(),
							success: function (data) {
								$('.research-submit + span').remove();

								fResponse = $.parseJSON(data);

								if (fResponse[0].type_of === 'success') {
									fMessage = fResponse[0].message;
									fTip     = fResponse[0].tip;
									fClose   = fResponse[0].close;
									fForm    = $(form);

									dataLayer.push({
										'event': 'Envio_Formularios', 'form': 'contato_pesquisa_enviar_sucesso'
									});

									dataLayer.push({
										'event':'VirtualPageview',
										'virtualPageURL':'/contatosucesso/pesquisa',
										'virtualPageTitle' : 'Contato – Pesquisa'
									});

									return successMessage(fMessage, fTip, fClose, fForm);

								} else {
									if (fResponse[0].type_of === 'process_error') {
										alert(fResponse[0].message);
									}
								}
							}
						});
					}
				});

				$('#others-contacts').validate({
					rules: {
						'ot-email': {
							required: {
								depends: function () {
									$(this).val($.trim($(this).val()));
									return true;
								}
							},
							customemail: true
						}
					},
					submitHandler: function (form) {

						$('.others-submit').after('<span class="loading"></span>');

						$.ajax({
							url: ajaxurl,
							type: 'post',
							data: $(form).serialize(),
							success: function (data) {
								$('.others-submit + span').remove();
								fResponse = $.parseJSON(data);

								if (fResponse[0].type_of === 'success') {
									fMessage = fResponse[0].message;
									fTip     = fResponse[0].tip;
									fClose   = fResponse[0].close;
									fForm    = $(form);

									dataLayer.push({
										'event': 'Envio_Formularios', 'form': 'contato_outros_enviar_sucesso'
									});

									dataLayer.push({
										'event':'VirtualPageview',
										'virtualPageURL':'/contatosucesso/outros',
										'virtualPageTitle' : 'Contato – Outros'
									});

									return successMessage(fMessage, fTip, fClose, fForm);

								} else {
									if (fResponse[0].type_of === 'process_error') {
										alert(fResponse[0].message);
									}
								}
							}
						})
					}
				});

				function successMessage (fMessage, fTip, fClose, fForm) {
					$('body').append('<div class="success-box">' +
					'<div class="message-box">' +
					'<span class="close-bottom"></span>' +
					'<span class="check-box"></span>' +
					'<h2>' + fMessage +
					'<span class="tip-message">' + fTip + '</span>' +
					'</h2>' +
					'<span class="close-txt">' + fClose + '</span>' +
					'</div></div>').fadeIn('slow');
					$('body').css({
						'overflow': 'hidden'
					});

					$('input').removeClass('valid');
					$('#nv-state, #re-state').val('').text('');
					$('input[type="radio"]').removeAttr('checked');
					fForm.trigger('reset');

					$('.pdf-icon').remove();

					$('.input-radio label').removeClass('active');

					$('.close-txt, .close-bottom').on('click', function () {
						$('body .success-box').fadeOut();
						$('body').css({
							'overflow-y': 'auto'
						});
					});
				}

				$('#re-birth').mask('99/99/9999', {clearIfNotMatch: true});
				$('#cm-form-phone, #re-phone, #nv-phone').mask('(00) 0000-00009', {clearIfNotMatch: true});

				function mobileSwiper () {
					if ($(window).width() < 767) {
						var logos = new Swiper('#client-list', {
							mode: 'horizontal',
							loop: false,
							resizeReInit: false,
							paginationClickable: true,
							autoResize: false,
							calculateHeight: true,
							slidesPerViewFit: true,
							pagination: '.mobile-pagination',
							createPagination: true
						});
					}
				}

				mobileSwiper();

				$(window).resize(function () {
					seContainer.masonry('bindResize');
					if ($(window).width() < 768 ) {
						seMasonry(140, 10);
					} else {
						seMasonry(220, 20);
					}
					mobileSwiper();
				});

				$('.sp-applied-single, .see-all').bind('click',  function (e) {

					e.preventDefault();

					createLg();

					specialtyId = $(this).attr('data-id');
					fullContent = true;

					return getSp(specialtyId, fullContent);
				});

				$('.sp-applied-page').on('click', function (e) {
					e.preventDefault();

					if (mobileVersion === false) {
							createLg(mobileVersion);

							specialtyId = $(this).attr('data-id');
							fullContent = true;

						return getSp(specialtyId, fullContent);
					} else {

						if($(this).is('.active')) {

							close_accordion_section();

						} else {

							close_accordion_section();

							$(this).addClass('active');

							$('.sp-applied.active').next('.main-deliverables').addClass('open').slideDown();
						}
					}
				});

				$('.m-info-service').on('click', function () {

					specialtyId   = $(this).attr('data-id');
					fullContent   = true;
					mobileVersion = true;

					createLg(mobileVersion);

					return getSp(specialtyId, fullContent);
				});

				$('.details-links li').on('click', function (e) {

					$('.details-links li').removeClass('active');
					$(this).addClass('active');

					createLg(mobileVersion);

					e.preventDefault();

					parentId    = $(this).closest('.specialty-txt').attr('post-id');
					postLink    = $(this).find('a').attr('href');
					fullContent = true;

					$.ajax({
						url: ajaxurl,
						type: 'post',
						data: {
							action: 'get-post-id',
							url_to_id: postLink
						},
						success: function (data) {
							currentId = data;
							getSp(parentId, fullContent, postLink, currentId);
						}
					});

				});

				$('.links-list').on('click', function (e) {

					$('.links-list, .specialty-txt a').removeClass('active');
					$(this).addClass('active');

					createLg();

					e.preventDefault();

					parentId    = $(this).attr('parent-id');
					currentId   = $(this).attr('data-id');
					fullContent = true;
					postLink    = $(this).find('a').attr('href');

					getSp(parentId, fullContent, postLink, currentId);
				});

				function createLg() {
					$('.lightbox-specialty').remove();

					$('body').css({
						'overflow-y': 'hidden'
					});

					$('body').append('<div class="lightbox-specialty">' +
						'<span class="loading">' +
						'<div class="spinner">' +
						'<div class="double-bounce1"></div>' +
						'<div class="double-bounce2"></div>' +
						'</div>' +
						'</span>' +
						'</div>');
				}

				$(document).on('click', '.get-sp', function (e) {
					$('.icon-circle').removeClass('active');

					thisOffsetTop  = $('.get-sp').offset().top;
					thisOffsetLeft = $('.get-sp').offset().left;
					thisWidth      = $('.get-sp').width();
					thisHeight     = $('.get-sp').height();
					$(this).closest('.icon-circle').addClass('active');

					$('.left-post-content, .middle-post-content').fadeOut('fast');

					specialtyId = $(this).attr('data-id');

					fullContent = false;

					getSp(specialtyId, fullContent, null);

				});

				$('body').on('click', '.sp-links li', function (e) {

					e.preventDefault();

					if ($(this).is('.active')) {
						return false;
					} else {
						$('.sp-links li').removeClass('active');

						$(this).addClass('active');

						postLink = $(this).find('a').attr('href');

						$('.lg-content-inner').removeClass('to-right').addClass('to-left');

						getDetail(postLink);
					}
				});
				getSp = function (specialtyId, fullContent, postLink, currentId) {

					if (mobileVersion === true) {
						screen = 'mobile';
					} else {
						$('.middle-box-wrapper').css({'opacity': '0'});
						screen = '';
					}

					$.ajax({
						url: ajaxurl,
						type: 'POST',
						data: {
							action      : 'lightbox-specialty',
							specialty_id: specialtyId,
							current_id  : currentId,
							current_screen: screen
						},
						success: function (data) {
							if (fullContent === true) {
								$('.lightbox-specialty').append(data);
							} else {
								$('.left-post-content').html($(data).find('.left-post-content').html());
								$('.middle-post-content').html($(data).find('.middle-post-content').html());
								$('.left-post-content, .middle-post-content').fadeIn('fast');
							}

							$('.lg-content').css({
								'top': '50%',
								'margin-top': '-' + ($('.lg-content').height() / 2) + 'px',
								'left': '50%',
								'margin-left': '-' + ($('.lg-content').width() / 2) + 'px'
							});

							$('.tooltip').each(function () {
								$(this).css({
									'top': '-' + ($(this).height() / 1.5) + 'px',
									'left': '50%',
									'margin-left': '-' + ($(this).width() / 2) + 'px'
								});
							});

							$('.lightbox-specialty .loading').fadeOut('slow');

							deg     = $('.icon-circle.active .get-sp').attr('data-deg');
							reverse = $('.icon-circle.active .get-sp').attr('data-reverse');

							setTimeout(function () {
								$('.lg-content').animate({
									'opacity': '1'
								});
							}, 100);

							$('.middle-box-wrapper li a').each(function () {
								$(this).attr('id', 'spec_' + $('.middle-post-content h2').attr('id') + '_2_' + accentsTidy($(this).text()));
							});

							if (postLink) {

								$('.lg-content-inner').removeClass('to-right').addClass('to-left');

								$('.lg-back').fadeIn('fast');

								getDetail(postLink);
							}

							rotate(deg, reverse);
						}
					});
				};

				$('.icon-circle .get-sp').on('click', function () {
					//deg     = $(this).attr('data-deg');
					reverse = $(this).attr('data-reverse');
					rotate (deg, reverse);
				});

				var rotate = function (deg, reverse) {
					var time = '1.5s';

					$('.circle').css({
						'-webkit-transition': 'all ' + time,
						'-moz-transition': 'all ' + time,
						'-o-transition': 'all ' + time,
						'-moz-transform': 'rotate(' + deg + 'deg)',
						'-webkit-transform': 'rotate(' + deg + 'deg)',
						'-o-transform': 'rotate(' + deg + 'deg)'
					});

					$('.icon-circle').css({
						'-webkit-transition': 'all ' + time,
						'-moz-transition': 'all ' + time,
						'-o-transition': 'all ' + time,
						'-moz-transform': 'rotate(' + reverse + 'deg)',
						'-webkit-transform': 'rotate(' + reverse + 'deg)',
						'-o-transform': 'rotate(' + reverse + 'deg)'
					});

					setTimeout(function (){
						$('.middle-box-wrapper').animate({'opacity': '1'});
					}, 1500);

					return false;
				};

				getDetail = function (postLink) {

					$('.right-box-content').html('');

					if (mobileVersion === true) {
						screen = 'mobile';
					} else {
						screen = '';
					}

					$.ajax({
						url: ajaxurl,
						type: 'POST',
						data: {
							action: 'deliverable-detail',
							post_link: postLink,
							current_screen: screen
						},
						success: function (data) {

							$('.right-box-content').html(data);

							$('.lg-back').fadeIn('fast');

							/*$('.left-box-outer').css({
								'border-color': 'transparent'
							});

							$('.left-box-outer .m-left').css({
								'opacity': 0
							});*/

							$('.icon-bg-large').css({
								'z-index': '10'
							});

							$('.lg-back').on('click', function () {
								$('.lg-content-inner').removeClass('to-left').addClass('to-right');

								$('.icon-bg-large').css({
									'z-index': '0'
								});

								$(this).fadeOut('fast');

								$('.deliverable-detail').html('');

								$('.sp-links ul').css({
									'border-right': '1px solid transparent'
								});

								$('.sp-links .m-left').css({
									'opacity': '0'
								});

								/*$('.left-box-outer').css({
									'padding-right': '40px',
									'border-color': '#c2c5c8'
								});

								$('.left-box-outer .m-left').css({
									'opacity': '1'
								});*/

								$('.links-list').removeClass('active');

							});

							$('.sp-links ul').css({
								'border-right': '1px solid #c2c5c8'
							});

							$('.sp-links .m-left').css({
								'opacity': '1'
							});

							/*$('.left-box-outer').css({
								'padding-right': '0'
							});*/

							$('.deliverable-detail').css('opacity', '1');

							$('.middle-box ul').css({
								'border-right': '1px solid #c2c5c8'
							});

							$('.lg-content-wrapper-mobile .deliverable-detail').animate({
								'left': '0',
								'right': '0'
							}, 300);

							$('.lg-back-mobile').css({'display': 'block'});

							$('.lg-back-mobile').on('click', function () {
								$('.lg-content-wrapper-mobile .deliverable-detail').animate({
									'left': '-100%',
									'right': 'auto'
								}, 300);

								$(this).css({'display': 'none'});
							});
						}
					});
					return false;
				};

				/*$(document).on('click', '.se-masonry', function () {
					window.location = $(this).attr('data-href');
				});*/

				control  = true;
				fullsize = true;

				$('.fullsize').on('click', function () {
					if (fullsize === true) {

						fullsize = false;

						$.ajax({
							url: ajaxurl,
							type: 'post',
							data: {
								action: 'fullscreen_gallery',
								post_id: postId
							},
							success: function (data) {
								$('body').css({
									'overflow': 'hidden'
								});

								$('.single-post-wrapper').append(data);

								setTimeout(function () {
									var fullSizeGallery = new Swiper('.full-size-screen .post-gallery', {
										mode:'horizontal',
										loop: false,
										speed: 1200,
										paginationClickable: true,
										grabCursor: true,
										roundLengths: true,
										autoResize: true,
										resizeReInit: true,
										slidesPerView: 1,
										calculateHeight: true,
										pagination: '.pagination'
									});

									$('.content-prev').on('click', function (e) {
										e.preventDefault();
										fullSizeGallery.swipePrev();
									});

									$('.content-next').on('click', function (e) {
										e.preventDefault();
										fullSizeGallery.swipeNext();
									});

									$('.normal-size').on('click', function () {
										fullsize = true;
										$('.full-size-screen').remove();

										$('body').css({
											'overflow': 'auto'
										});
									});

									$('.full-size-screen .post-gallery').animate({
										'opacity': 1
									});
								}, 100);
							}
						});
					}
				});

				$(window).resize(function(){
					//postGallery.resizeFix(true)
				});

				var postGallery = new Swiper('.post-gallery', {
						mode: 'horizontal',
						loop: false,
						speed: 600,
						paginationClickable: true,
						grabCursor: true,
						roundLengths: true,
						autoResize: true,
						resizeReInit: true,
						slidesPerView: 1,
						calculateHeight: true,
						onInit: function () {
							var totalThumbs = $('ul.thumbnails li').length + 1;
							$('ul.thumbnails').css({
								'width': (totalThumbs * 125) + totalThumbs * 10 + 'px'
							});
							$('.thumbnails li').each(function (i) {
								if (i + 1 === postGallery.activeIndex) {
									$('.thumbnails li').removeClass('active');
									$(this).addClass('active');
								}
							});
						},
						onSlideChangeStart: function () {
							$('.thumbnails li').each(function (i) {
								if (i === postGallery.activeIndex) {
									$('.thumbnails li').removeClass('active');
									$(this).addClass('active');
								}
							});

							if (postGallery.activeIndex === ($('.thumbnails li').length - 1)) {
								control = false;
							} else if (postGallery.activeIndex === 0) {

								$('.thumbnails').animate({
									'margin-left': '0'
								});

								control = true;
							}
						},
						onSlideNext: function () {
							if ($('.thumbnails li.active').position().left + 135 >= $('.normal-size-screen').width() && control === true) {
								$('.thumbnails').animate({
									'margin-left': '-=135px'
								});
							}
						},
						onSlidePrev: function () {
							if ($('.thumbnails li.active').position().left <= $('.normal-size-screen').width() && control === false) {
								$('.thumbnails').animate({
									'margin-left': '+=135px'
								});
							}
						}
					}
				);
				$('.thumbnails').on('click', 'li', function(e){
					e.preventDefault();
					postGallery.swipeTo($(this).index(), 500);
				});

				$('.content-prev').on('click', function (e) {
					e.preventDefault();
					postGallery.swipePrev();
				});

				$('.content-next').on('click', function (e) {
					e.preventDefault();
					postGallery.swipeNext();
				});
			}
		},

		// Home page
		home: {
			init: function() {

				var timeDuration = 10000;

				var progress = function (el) {
					el.circleProgress({
						value: 1,
						size: 24,
						fill: { color: '#11b2e1' },
						thickness: 5,
						emptyFill: 'transparent',
						animation: {
							duration: timeDuration
						}
					});
				};

				var homeCarousel = function () {
					$('.swiper-container').swiper({
						mode:'horizontal',
						loop: true,
						speed: 600,
						autoplay: 10000,
						pagination: '.pagination',
						paginationClickable: true,
						paginationActiveClass: 'active',
						autoResize: true,
						resizeReInit: true,
						effect: 'coverflow',
						calculateHeight: true,
						onSlideChangeStart: function () {
							progress($('.pagination .active'));
						}
					});
				};

				homeCarousel();

				progress($('.pagination .active'));

				setTimeout(function () {
					$('#carousel').animate({
						'opacity': '1'
					}, 100);
				}, 500);
			}
		}
	};

// The routing fires all common scripts, followed by the page specific scripts.
// Add additional events for more control over timing e.g. a finalize event
	var UTIL = {
		fire: function(func, funcname, args) {
			var namespace = Roots;
			funcname = (funcname === undefined) ? 'init' : funcname;
			if (func !== '' && namespace[func] && typeof namespace[func][funcname] === 'function') {
				namespace[func][funcname](args);
			}
		},
		loadEvents: function() {
			UTIL.fire('common');

			$.each(document.body.className.replace(/-/g, '_').split(/\s+/),function(i,classnm) {
				UTIL.fire(classnm);
			});
		}
	};


	$(document).ready(UTIL.loadEvents);

})(jQuery); // Fully reference jQuery after this point.
