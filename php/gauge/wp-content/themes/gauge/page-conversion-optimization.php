<?php
/**
 * Created by WebStorm.
 * User: anderson
 * Template Name: Otimização de Conversão
 * Date: 1/16/15
 * Time: 12:17 PM
 */

$page_559     = get_post(559);
$page_559_img = wp_get_attachment_image_src( get_post_thumbnail_id( 559 ), 'single-post-thumbnail' );

?>

<div class="page-featured-wrapper services-inner" <?php echo !empty($page_559_img) ? 'style="background-image: url(' . $page_559_img[0] . ')"' : ''; ?>>
    <div class="page-featured">
        <div class="container">
            <div class="entry-content-outer">
                <header class="entry-header">
                    <h1 class="page-title">
                        <?php
                            $page_title = $page_559->post_title;
                            $pos = strpos($page_title, ' ');
                            $part1 = substr($page_title, 0, $pos);
                            $part2 = substr($page_title, $pos + 1);
                            echo $part1 . '<br>' . $part2;
                        ?>
                    </h1>
                </header>
                <article class="entry-content">
                    <?php echo wpautop($page_559->post_excerpt); ?>
                </article>
            </div>
            <a class="page-more" href="#">
                <span id="know-more-inner"></span>
            </a>
        </div>
    </div>
</div>
<div class="single-page-inner">
    <?php get_template_part('templates/partials/_sp-specialties'); ?>
    <div id="se-modular-wrapper">
        <?php get_template_part('templates/partials/_se-modular-header'); ?>
        <div class="se-modular container">
            <div class="se-modular-inner">
                <div class="se-modular-items">
                    <?php get_template_part('templates/partials/_se-modular'); ?>
                </div>
            </div>
        </div>
    </div>
</div>
