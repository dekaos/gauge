<?php
/**
 * Clean up gallery_shortcode()
 *
 * Re-create the [gallery] shortcode and use thumbnails styling from Bootstrap
 * The number of columns must be a factor of 12.
 *
 * @link http://getbootstrap.com/components/#thumbnails
 */

function roots_gallery($attr) {

  if (isset($_POST['action']) && $_POST['action'] == 'fullscreen_gallery') {
    $post          = get_post($_POST['post_id']);
    $css_class     = 'full-size-screen';
    $size          = 'full';
    $gallery_title = '<h2 class="gallery-title">' . get_the_title($_POST['post_id']) . '</h2>';
    $pagination    = '<div class="pagination"></div>';
  } else {
    $post          = get_post();
    $css_class     = 'normal-size-screen';
    $size          = 'size_665-465';
    $gallery_title = '';
    $pagination    = '';
  }

  static $instance = 0;

  $instance++;

  $attr['orderby'] = 'post__in';

  if (!empty($attr['ids'])) {
    $attr['include'] = $attr['ids'];
  } else {
    $pattern = get_shortcode_regex();

    preg_match('/'.$pattern.'/s', $post->post_content, $matches);

    if (is_array($matches) && $matches[2] == 'gallery') {

      $shortcode = $matches[3];
      $ids       = explode('ids=', $shortcode);
      $ids[1]    = str_replace("\"", '', $ids[1]);

      $attr['include'] = $ids[1];
    }
  }

  $output = apply_filters('post_gallery', '', $attr);

  if ($output != '') {
    return $output;
  }

  if (isset($attr['orderby'])) {
    $attr['orderby'] = sanitize_sql_orderby($attr['orderby']);
    if (!$attr['orderby']) {
      unset($attr['orderby']);
    }
  }

  extract(shortcode_atts(array(
    'order'      => 'ASC',
    'orderby'    => 'menu_order ID',
    'id'         => $post->ID,
    'itemtag'    => '',
    'icontag'    => '',
    'captiontag' => '',
    'columns'    => '',
    'size'       => $size,
    'include'    => '',
    'exclude'    => '',
    'link'       => ''
  ), $attr));

  $id = intval($id);
  if (!empty($include)) {
    $_attachments = get_posts(array('include' => $include, 'post_status' => 'inherit', 'post_type' => 'attachment', 'post_mime_type' => 'image', 'order' => $order, 'orderby' => $orderby));

    $attachments = array();
    foreach ($_attachments as $key => $val) {
      $attachments[$val->ID] = $_attachments[$key];
    }
  }

  if (empty($attachments)) {
    return '';
  }

  if (is_feed()) {
    $output = "\n";
    foreach ($attachments as $att_id => $attachment) {
      $output .= wp_get_attachment_link($att_id, $size, true) . "\n";
    }
    return $output;
  }

  $get_cat = array_filter(
      get_the_category(),

      function ($a) {
        return array_key_exists('term_id', $a) && $a->term_id == 38;
      }
  );
  if (!$get_cat) {
    //$unique = (get_query_var('page')) ? $instance . '-p' . get_query_var('page'): $instance;
    $output .= '<div id="gallery" class="' . $css_class . '">';
    $output .= $gallery_title;
    $output .= '<div class="normal-size close">x</div>';
    $output .= '<a href="#" class="content-prev"></a>';
    $output .= '<a href="#" class="content-next"></a>';
    $output .= '<div class="post-gallery swiper-container">';
    $output .= '<span class="fullsize"></span>';
    $output .= '<div class="swiper-wrapper">';

    foreach ($attachments as $id => $attachment) {
      $img_src = wp_get_attachment_image_src($id, $size);
      $output .= '<div class="swiper-slide">';
      $output .= '<div class="inner">';
      $output .= '<img src="' . $img_src[0] .'" width="100%">';
      $output .= '</div>';
      $output .= '</div>';
    }

    $output .= '</div></div></div>';
    $output .= $pagination;

    if ($css_class == 'normal-size-screen') {
      $output .= '<div class="thumbnails-wrapper">';
      $output .= '<ul class="thumbnails">';

      $counter = 0;

      foreach ($attachments as $thumb_id => $attachment) {
        $img_thumb = wp_get_attachment_image_src($thumb_id, 'size_130-90');

        $output .= $attachment == reset($attachments) ? '<li class="active">' : '<li>';
        $output .= '<img src="' . $img_thumb[0] . '" class="thumbnail-indicator">';
        $output .= '<span></span>';
        $output .= '</li>';
        $counter++;
      }

      $output .= '</ul>';
      $output .= '<div class="clearfix"></div>';
      $output .= '</div>';
    }

    return $output;
  } else {

    $template_id = get_post_meta( $post->ID, '_wp_post_template', true);

    $dims = array(
        'size_220-220',
        'size_220-460',
        'size_460-220',
        'size_460-460'
    );

    $counter = 0;
    $size    = $class = $dims[0];
    $output  = '<div class="post-masonry">';
    foreach ($attachments as $id => $attachment) {

      if ($template_id == 'templates/trabalhos-case-4.php') {

        if ($counter == 0) {
          $size = $class = $dims[3];
        }

        if ($counter == 1 ||
            $counter == 2 ||
            $counter == 3 ||
            $counter == 4) {
          $size = $class = $dims[0];
        }

        if ($counter >= 5) {
          $counter = 0;
        }

      } else {

        if ($counter == 0 ||
            $counter == 3 ||
            $counter == 6) {
          $size = $class = $dims[2];
        }

        if ($counter == 1 ||
            $counter == 2 ||
            $counter == 5 ||
            $counter == 7) {
          $size = $class = $dims[0];
        }

        if ($counter == 4) {
          $size = $class = $dims[1];
        }

        if ($counter >= 8) {
          $counter = 0;
        }
      }

      $img_src = wp_get_attachment_image_src($id, $size);
      $output .= '<div class="post-masonry-image ' . $class . ' ' . 'post-' . $counter . '" style="background: url(' . $img_src[0] . ') no-repeat center center">';
      //$output .= '<img src="' . $img_src[0] .'" width="' . $width . '" height="' . $height . '">';
      $output .= '</div>';

      $counter++;
    }

    $output .= '<div class="clearfix"></div>';
    $output .= '</div>';

    return $output;
  }
}

if (current_theme_supports('bootstrap-gallery')) {
  remove_shortcode('gallery');
  add_shortcode('gallery', 'roots_gallery');
  add_filter('use_default_gallery_style', '__return_null');
}

/**
 * Add class="thumbnail img-thumbnail" to attachment items
 */
function roots_attachment_link_class($html) {
  $postid = get_the_ID();
  $html = str_replace('<a', '<a class="large-image links"', $html);
  return $html;
}
add_filter('wp_get_attachment_link', 'roots_attachment_link_class', 10, 1);