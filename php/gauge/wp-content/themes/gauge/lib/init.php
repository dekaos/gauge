<?php
/**
 * Roots initial setup and constants
 */
function roots_setup() {
  // Make theme available for translation
  load_theme_textdomain('roots', get_template_directory() . '/lang');

  // Register wp_nav_menu() menus (http://codex.wordpress.org/Function_Reference/register_nav_menus)
  register_nav_menus(array(
    'primary_navigation' => __('Primary Navigation', 'roots'),
  ));

  // Add post thumbnails (http://codex.wordpress.org/Post_Thumbnails)
  add_theme_support('post-thumbnails');
  add_image_size( 'size_220-220', 220, 220, true );
  add_image_size( 'size_460-460', 460, 460, true );
  add_image_size( 'size_220-460', 220, 460, true );
  add_image_size( 'size_460-220', 460, 220, true );
  add_image_size( 'size_665-465', 665, 465, true );
  add_image_size( 'size_130-90', 130, 90, true );
  set_post_thumbnail_size(220, 220, false);
  // add_image_size('category-thumb', 300, 9999); // 300px wide (and unlimited height)

  // Add post formats (http://codex.wordpress.org/Post_Formats)
  // add_theme_support('post-formats', array('aside', 'gallery', 'link', 'image', 'quote', 'status', 'video', 'audio', 'chat'));

  // Tell the TinyMCE editor to use a custom stylesheet
  add_editor_style('/assets/css/editor-style.css');
}
add_action('after_setup_theme', 'roots_setup');

function variables() {
  if (is_page_template('page-knowledge.php')) {
    $page_name = 'knowledge';
  } else if (is_page_template('page-work.php')) {
    $page_name = 'work';
  } else {
    $page_name = '';
  }
  ?>
  <script>
    var ajaxurl      = '<?php echo admin_url('admin-ajax.php') ?>';
    var errorMsg     = '<?php _e('This field is mandatory', 'roots'); ?>';
    var errorEmail   = '<?php _e('Invalid e-mail', 'roots'); ?>';
    var morePosts    = '<?php _e('Load more posts', 'roots'); ?>';
    var noMorePosts  = '<?php _e('No more posts', 'roots'); ?>';
    var pageName     = '<?php echo basename(get_permalink()); ?>';
    <?php if ($page_name) : ?>
      var pageTemplate = '<?php echo $page_name; ?>';
    <?php endif; ?>
    <?php if (is_single()) : global $post; ?>
    var postId = '<?php echo $post->ID; ?>';
    <?php endif; ?>
  </script>
<?php
}

add_action('wp_head','variables');

if (!is_admin()) {
  function SearchFilter($query) {
    if ($query->is_search) {

      $query->set('cat','17, 18, 19, 20, 21, 22, 23');
    }
    return $query;
  }

  add_filter('pre_get_posts','SearchFilter');
}