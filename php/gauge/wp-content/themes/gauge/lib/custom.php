<?php
/**
 * Custom functions
 */
show_admin_bar(false);

add_action('wp_ajax_nopriv_ajax_search', 'ajax_search');
add_action('wp_ajax_ajax_search', 'ajax_search');

add_action('wp_ajax_nopriv_by_category', 'ajax_posts_by_category');
add_action('wp_ajax_by_category', 'ajax_posts_by_category');

add_action('wp_ajax_nopriv_vacancy_detail', 'vacancy_detail');
add_action('wp_ajax_vacancy_detail', 'vacancy_detail');

add_action('wp_ajax_nopriv_vacancies', 'vacancies');
add_action('wp_ajax_vacancies', 'vacancies');

add_action('wp_ajax_nopriv_se-modular', 'se_modular');
add_action('wp_ajax_se-modular', 'se_modular');

add_action('wp_ajax_nopriv_fullscreen_gallery', 'fullscreen_gallery');
add_action('wp_ajax_fullscreen_gallery', 'fullscreen_gallery');

add_action('wp_ajax_nopriv_lightbox-specialty', 'lightbox_specialty');
add_action('wp_ajax_lightbox-specialty', 'lightbox_specialty');

add_action('wp_ajax_nopriv_deliverable-detail', 'deliverable_detail');
add_action('wp_ajax_deliverable-detail', 'deliverable_detail');

add_action('wp_ajax_nopriv_get-post-id', 'get_post_id');
add_action('wp_ajax_get-post-id', 'get_post_id');

add_action('show_user_profile', 'user_extra_fields');
add_action('edit_user_profile', 'user_extra_fields');
add_action('personal_options_update', 'save_user_extra_fields');
add_action('edit_user_profile_update', 'save_user_extra_fields');
add_action('user_new_form', 'user_extra_fields');
add_action('user_register', 'save_user_extra_fields');

add_action('add_meta_boxes', 'cd_meta_box_add');
add_action('save_post', 'cd_meta_box_save');

add_action('add_meta_boxes', 'add_video_url');
add_action('save_post', 'video_url_save');

add_action('add_meta_boxes', 'add_second_excerpt');
add_action('save_post', 'second_excerpt_save');

function fullscreen_gallery() {

    require_once(get_template_directory() . '/lib/gallery.php');
    echo roots_gallery(null);
    exit;
}

function ajax_search() {
    require_once(get_template_directory() . '/search.php');
    exit;
}

function ajax_posts_by_category () {
    switch ($_GET['page_name']) {
        case 'conhecimento' :
            require_once(get_template_directory() . '/templates/partials/_knowledge.php');
            break;
        case 'trabalhos' :
            require_once(get_template_directory() . '/templates/partials/_work.php');
            break;
        default:
            '';
    }
    exit;
}

function vacancy_detail () {
    require_once(get_template_directory() . '/templates/partials/_vacancy.php');
    exit;
}

function vacancies () {
    require_once(get_template_directory() . '/templates/partials/_vacancies.php');
    exit;
}

function user_extra_fields( $user )
{
    ?>
    <h3>Informações Extras</h3>

    <table class="form-table">
        <tr>
            <th><label for="user_cargo">Cargo</label></th>
            <td><input type="text" id="user_cargo" name="user_cargo" value="<?php echo esc_attr(get_the_author_meta( 'user_cargo', $user->ID )); ?>" class="regular-text"></td>
        </tr>
    </table>
<?php
}

function save_user_extra_fields ( $user_id ) {
    update_user_meta($user_id, 'user_cargo', sanitize_text_field($_POST['user_cargo']));
}

if (!function_exists( 'post_child_category' ) ) {
    function post_child_category( $id = null )
    {
        if ( $id = null )
            return false;

        $categories = get_the_category( $id );
        if ( count($categories) > 0 )
        {
            return $categories[count($categories)-1];
        } else {
            return false;
        }
    }
}

function cd_meta_box_add() {
    $category = get_the_category();
    $parent   = get_category($category[0]->category_parent);
    //if ($parent->slug == 'trabalhos') {
        add_meta_box( 'my-meta-box-id', 'Especialidades Aplicadas', 'cd_meta_box_cb', 'post', 'normal', 'high' );
        add_meta_box( 'my-meta-box-id', 'Especialidades Aplicadas', 'cd_meta_box_cb', 'page', 'normal', 'high' );
    //}
}

function get_array_value($value) {
    global $post;

    $values = get_post_meta( $post->ID, 'sp-specialties')[0];

    foreach ($values as $v) {
        if ($v['sp-class'] == $value) {
            return true;
        } else {
            return false;
        }
    }
}

function in_array_r($needle, $haystack, $strict = false) {
    foreach ($haystack as $item) {
        if (($strict ? $item === $needle : $item == $needle) || (is_array($item) && in_array_r($needle, $item, $strict))) {
            return true;
        }
    }

    return false;
}

function cd_meta_box_cb () {

    $sp_1 = 'Analytics & performance';
    $sp_2 = 'Business strategy';
    $sp_3 = 'Buzz & monitoring';
    $sp_4 = 'Data visualization';
    $sp_5 = 'Insights & pesquisa';
    $sp_6 = 'Digital presence strategy';
    $sp_7 = 'User experience';
    $sp_8 = 'Development and technology';

    global $post;

    $values = get_post_meta( $post->ID, 'sp-specialties')[0];
?>
    <div>
        <input id="sp-analytics" name="sp-analytics" type="checkbox" value="1" <?php echo in_array_r('sp-analytics', $values) ? 'checked' : ''; ?>>
        <label for="sp-analytics">
            <?php _e($sp_1, 'roots'); ?>
        </label>
    </div>
    <div>
        <input id="sp-business" name="sp-business" type="checkbox" value="2" <?php echo in_array_r('sp-business', $values) ? 'checked' : ''; ?>>
        <label for="sp-business">
            <?php _e($sp_2, 'roots'); ?>
        </label>
    </div>
    <div>
        <input id="sp-buzz" name="sp-buzz" type="checkbox" value="3" <?php echo in_array_r('sp-buzz', $values) ? 'checked' : ''; ?>>
        <label for="sp-buzz">
            <?php _e($sp_3, 'roots'); ?>
        </label>
    </div>
    <div>
        <input id="sp-data" name="sp-data" type="checkbox" value="4" <?php echo in_array_r('sp-data', $values) ? 'checked' : ''; ?>>
        <label for="sp-data">
            <?php _e($sp_4, 'roots'); ?>
        </label>
    </div>
    <div>
        <input id="sp-insights" name="sp-insights" type="checkbox" value="5" <?php echo in_array_r('sp-insights', $values) ? 'checked' : ''; ?>>
        <label for="sp-insights">
            <?php _e($sp_5, 'roots'); ?>
        </label>
    </div>
    <div>
        <input id="sp-digital" name="sp-digital" type="checkbox" value="6" <?php echo in_array_r('sp-digital', $values) ? 'checked' : ''; ?>>
        <label for="sp-digital">
            <?php _e($sp_6, 'roots'); ?>
        </label>
    </div>
    <div>
        <input id="sp-user" name="sp-user" type="checkbox" value="7" <?php echo in_array_r('sp-user', $values) ? 'checked' : ''; ?>>
        <label for="sp-user">
            <?php _e($sp_7, 'roots'); ?>
        </label>
    </div>
    <div>
        <input id="sp-development" name="sp-development" type="checkbox" value="8" <?php echo in_array_r('sp-development', $values) ? 'checked' : ''; ?>>
        <label for="sp-development">
            <?php _e($sp_8, 'roots'); ?>
        </label>
    </div>

<?php }

function cd_meta_box_save( $post_id ) {

    if( defined( 'DOING_AUTOSAVE' ) && DOING_AUTOSAVE ) return $post_id;

    $sp_analytics   = sanitize_text_field($_POST['sp-analytics']);
    $sp_business    = sanitize_text_field($_POST['sp-business']);
    $sp_buzz        = sanitize_text_field($_POST['sp-buzz']);
    $sp_data        = sanitize_text_field($_POST['sp-data']);
    $sp_insights    = sanitize_text_field($_POST['sp-insights']);
    $sp_digital     = sanitize_text_field($_POST['sp-digital']);
    $sp_user        = sanitize_text_field($_POST['sp-user']);
    $sp_development = sanitize_text_field($_POST['sp-development']);

    $meta_data = array();

    if ($sp_analytics) {
        $sp_analytics = array(
            'sp-class' => 'sp-analytics',
            'sp-post'  => 94
        );
        array_push($meta_data, $sp_analytics);
    }

    if ($sp_business) {
        $sp_business = array(
            'sp-class' => 'sp-business',
            'sp-post'  => 102
        );
        array_push($meta_data, $sp_business);
    }

    if ($sp_buzz) {
        $sp_buzz = array(
            'sp-class' => 'sp-buzz',
            'sp-post'  => 104
        );
        array_push($meta_data, $sp_buzz);
    }

    if ($sp_data) {
        $sp_data = array(
            'sp-class' => 'sp-data',
            'sp-post'  => 115
        );
        array_push($meta_data, $sp_data);
    }

    if ($sp_insights) {
        $sp_insights = array(
            'sp-class' => 'sp-insights',
            'sp-post'  => 117
        );
        array_push($meta_data, $sp_insights);
    }

    if ($sp_digital) {
        $sp_digital = array(
            'sp-class' => 'sp-digital',
            'sp-post'  => 100
        );
        array_push($meta_data, $sp_digital);
    }

    if ($sp_user) {
        $sp_user = array(
            'sp-class' => 'sp-user',
            'sp-post'  => 109
        );
        array_push($meta_data, $sp_user);
    }

    if ($sp_development) {
        $sp_development = array(
            'sp-class' => 'sp-development',
            'sp-post'  => 111
        );
        array_push($meta_data, $sp_development);
    }
    /*$meta_data = array(
        'sp-analytics'   => $sp_analytics,
        'sp-business'    => $sp_business,
        'sp-buzz'        => $sp_buzz,
        'sp-data'        => $sp_data,
        'sp-insights'    => $sp_insights,
        'sp-digital'     => $sp_digital,
        'sp-user'        => $sp_user,
        'sp-development' => $sp_development
    );*/

    if( !current_user_can( 'edit_post' ) ) return;

    update_post_meta( $post_id, 'sp-specialties', $meta_data);
}

function add_second_excerpt () {
    add_meta_box( 'add-second-excerpt', 'Resumo do serviço', 'second_excerpt', 'page', 'normal', 'high' );
}

function second_excerpt () {
    global $post;
    $value = get_post_meta( $post->ID, 'second-excerpt')[0];
    echo '<textarea name="second-excerpt" value="' . $value . '" style="width: 98%">' . $value .'</textarea>';
}

function second_excerpt_save ($post_id) {
    if( !current_user_can( 'edit_post' ) ) return;

    if( defined( 'DOING_AUTOSAVE' ) && DOING_AUTOSAVE ) return $post_id;

    $second_excerpt = sanitize_text_field($_POST['second-excerpt']);

    update_post_meta( $post_id, 'second-excerpt', $second_excerpt);
}

function lightbox_specialty () {
    require_once(get_template_directory() . '/templates/partials/_lightbox-specialty.php');
    exit;
}

function deliverable_detail () {
    require_once(get_template_directory() . '/templates/partials/_deliverable-detail.php');
    exit;
}

function se_modular () {
    require_once(get_template_directory() . '/templates/partials/_se-modular.php');
    exit;
}

function get_post_id () {
    $url = sanitize_text_field($_POST['url_to_id']);
    echo custom_url_to_postid($url);
    exit;
}

remove_action( 'wp_head', 'print_emoji_detection_script', 7 );
remove_action( 'wp_print_styles', 'print_emoji_styles' );

function custom_url_to_postid( $url ) {

    $post_id = url_to_postid( $url );

    if ( $post_id == 0 ) {
        $cpts = get_post_types( array(
            'public'   => true,
            '_builtin' => false
        ),  'objects', 'and' );

        $url_parts = explode( '/', trim( $url, '/' ) );
        $url_parts = array_splice( $url_parts, 3 );
        $path = implode( '/', $url_parts );

        foreach ( $cpts as $cpt_name => $cpt ) {
            $cpt_slug = $cpt->rewrite['slug'];
            if ( strlen( $path ) > strlen( $cpt_slug ) && substr( $path, 0, strlen( $cpt_slug ) ) == $cpt_slug ) {
                $slug = substr( $path, strlen( $cpt_slug ) );
                $query = new WP_Query( array(
                    'post_type'         => $cpt_name,
                    'name'              => $slug,
                    'posts_per_page'    => 1
                ));
                if ( is_object( $query->post ) )
                    $post_id = $query->post->ID;
            }
        }
    }
    return $post_id;
}

function excerpt_130 ($excerpt) {
    if (strlen($excerpt) > 130) {
        $offset = (130 - 3) - strlen($excerpt);
        $excerpt = substr($excerpt, 0, strrpos($excerpt, ' ', $offset));
        echo wpautop($excerpt . ' ...');
    } else {
        echo wpautop($excerpt);
    }
}

function add_video_url () {
    add_meta_box( 'post-video-url', 'Video URL', 'video_url_meta_box', 'post', 'side', 'low' );
}

function video_url_meta_box () {
    global $post;

    echo '<p><input id="post-video-url" name="post-video-url" style="width: 98%" value="' . get_post_meta( $post->ID, 'post-video-url')[0] . '"></p>';
    $video_url = get_post_meta( $post->ID, 'post-video-url')[0];

    $video_url = explode('v=', $video_url);
    if ($video_url[1]) {
        echo '<p><iframe width="400" height="300" frameborder="0" allowfullscreen src="//www.youtube.com/embed/'. $video_url[1] . '"></iframe></p>';
    }
}

function video_url_save ($post_id) {
    if( defined( 'DOING_AUTOSAVE' ) && DOING_AUTOSAVE ) return $post_id;

    $video_url = sanitize_text_field($_POST['post-video-url']);

    if( !current_user_can( 'edit_post' ) ) return;

    update_post_meta( $post_id, 'post-video-url', $video_url);

}

add_filter("pre_update_option_category_base","cc_remove_blog_from_tags_categories");
add_filter("pre_update_option_tag_base","cc_remove_blog_from_tags_categories");
add_filter("pre_update_option_permalink_structure","cc_remove_blog_slug");

/* just check if the current structure begins with /blog/ remove that and return the stripped structure */
function cc_remove_blog_slug($tag_cat_permalink)
{

    if(!preg_match("/^\/blog\//",$tag_cat_permalink))
        return $tag_cat_permalink;

    $new_permalink=preg_replace ("/^\/blog\//","/",$tag_cat_permalink );
    return $new_permalink;


}