<?php
/**
 * Created by WebStorm.
 * User: anderson
 * Date: 1/20/15
 * Time: 10:44 PM
 */
?>

<div class="single-post-template">
    <div class="container">
        <?php while (have_posts()) : the_post(); ?>
        <div class="category-<?php echo get_the_category()[0]->cat_ID; ?>">
            <header class="entry-header">
                <h1>
                    <?php the_title(); ?>
                </h1>
                <?php if (has_post_thumbnail()) : ?>
                    <div class="post-image">
                        <?php the_post_thumbnail('full'); ?>
                    </div>
                <?php endif; ?>
            </header>
            <article>
                <div class="entry-content">
                    <?php the_content(); ?>
                </div>
            </article>
        </div>
        <?php endwhile; ?>
    </div>
    <div class="clearfix"></div>
</div>