<?php

/** 
 * As configurações básicas do WordPress.
 *
 * Esse arquivo contém as seguintes configurações: configurações de MySQL, Prefixo de Tabelas,
 * Chaves secretas, Idioma do WordPress, e ABSPATH. Você pode encontrar mais informações
 * visitando {@link http://codex.wordpress.org/Editing_wp-config.php Editing
 * wp-config.php} Codex page. Você pode obter as configurações de MySQL de seu servidor de hospedagem.
 *
 * Esse arquivo é usado pelo script ed criação wp-config.php durante a
 * instalação. Você não precisa usar o site, você pode apenas salvar esse arquivo
 * como "wp-config.php" e preencher os valores.
 *
 * @package WordPress
 */

// ** Configurações do MySQL - Você pode pegar essas informações com o serviço de hospedagem ** //
/** O nome do banco de dados do WordPress */
define('WP_CACHE', true); //Added by WP-Cache Manager
define( 'WPCACHEHOME', '/srv/http/gauge/gauge/dev/php/gauge/wp-content/plugins/wp-super-cache/' ); //Added by WP-Cache Manager
if ($_SERVER['SERVER_ADDR'] == 'localhost' || $_SERVER['SERVER_ADDR'] == '192.168.0.120') {
    define('DB_NAME', 'gauge');
    /** Usuário do banco de dados MySQL */
    define('DB_USER', 'root');

    /** Senha do banco de dados MySQL */
    define('DB_PASSWORD', '');

    /** nome do host do MySQL */
    define('DB_HOST', 'localhost');
} else {
    define('DB_NAME', getenv('OPENSHIFT_APP_NAME'));

    /** Usuário do banco de dados MySQL */
    define('DB_USER', getenv('OPENSHIFT_MYSQL_DB_USERNAME'));

    /** Senha do banco de dados MySQL */
    define('DB_PASSWORD', getenv('OPENSHIFT_MYSQL_DB_PASSWORD'));

    /** nome do host do MySQL */
    define('DB_HOST', getenv('OPENSHIFT_MYSQL_DB_HOST'));
}

define('FS_METHOD','direct');

/** Conjunto de caracteres do banco de dados a ser usado na criação das tabelas. */
define('DB_CHARSET', 'utf8');

/** O tipo de collate do banco de dados. Não altere isso se tiver dúvidas. */
define('DB_COLLATE', '');

/**#@+
 * Chaves únicas de autenticação e salts.
 *
 * Altere cada chave para um frase única!
 * Você pode gerá-las usando o {@link https://api.wordpress.org/secret-key/1.1/salt/ WordPress.org secret-key service}
 * Você pode alterá-las a qualquer momento para desvalidar quaisquer cookies existentes. Isto irá forçar todos os usuários a fazerem login novamente.
 *
 * @since 2.6.0
 */
define('AUTH_KEY',         'T$~8OsC=|.wY=H:^OtP&{Xm`i!]xna5U`I-a>~ya7lW.(-s<aw+j;gmus!$@@#@#ooLDSLFO*)CtbAR');
define('SECURE_AUTH_KEY',  'hj@kLrX%W,MIIJ4*b5;b%YK[sOE0M:X<sadfsadfpQyrhycep<KU 1HXWr:tG9h/%mh{iSaK');
define('LOGGED_IN_KEY',    ',TLW@^2*KNLtf |}EK@m.OkDB=3ZD!.2034902382897987EJWOISFD((*&1|Ox||{{gv*SD~xOL+;Eue:6!<zl_s}*+');
define('NONCE_KEY',        '-ZHl9e+DN YxD4:--N>D/NbSGl%ZF,Pzp(@i2|kBlg+)4Y|a}Jvx]{(}$o&1W+#@*(#u@FKJLJLJDFOljsf3842kjfsd+T');
define('AUTH_SALT',        'JD.!9a^u&5zFo.h4eE+|DH^Rn(0d)`K6;Sne+@sdfsdafsadfsdaf/g6diY(Y,##)1(Xtm$<2Q;@xxF');
define('SECURE_AUTH_SALT', ';FC$16um 5[xGeLHH3654372jfjl_)*Uy6t%$r4#u8xdafqwer23432?Xk-e(zyi4eYJMB vkLIPQ%pW~`%P5XoIXlAE)&j[-t*n ');
define('LOGGED_IN_SALT',   'g1gP7clJ@BLc/]s vQrsdafs(lGb7oo4Gy@NLsadfsadfsdaf:[)6*EiSsh+(c2*QVfg2K?(){c~1QNb');
define('NONCE_SALT',       '/r-N1xRiMC@xy32467826yriuewqyrw`B&Z|-9oW>Gt{NS+uxP|R#xE<*d<Z{J&hp`Yi%)Y$3p5XA7-w_l');

/**#@-*/


/**
 * Prefixo da tabela do banco de dados do WordPress.
 *
 * Você pode ter várias instalações em um único banco de dados se você der para cada um um único
 * prefixo. Somente números, letras e sublinhados!
 */
$table_prefix  = 'ytew5_';

/**
 * Para desenvolvedores: Modo debugging WordPress.
 *
 * altere isto para true para ativar a exibição de avisos durante o desenvolvimento.
 * é altamente recomendável que os desenvolvedores de plugins e temas usem o WP_DEBUG
 * em seus ambientes de desenvolvimento.
 */
define('WP_DEBUG', false);

/* Isto é tudo, pode parar de editar! :) */
/** Caminho absoluto para o diretório WordPress. */
if ( !defined('ABSPATH') )
	define('ABSPATH', dirname(__FILE__) . '/');

define('MULTISITE', true);
define('SUBDOMAIN_INSTALL', false);
define( 'PATH_CURRENT_SITE', '/gauge/gauge/dev/php/gauge/' );
define( 'SITE_ID_CURRENT_SITE', 1 );
define( 'BLOG_ID_CURRENT_SITE', 1 );
	
/** Configura as variáveis do WordPress e arquivos inclusos. */
require_once(ABSPATH . 'wp-settings.php');